import jpk.repository.user.UserRepository;
import jpk.service.user.UserValidationService;
import jpk.service.user.parameter.PasswordService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@SpringBootTest(classes = {UserValidationService.class, UserRepository.class, PasswordService.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class UserValidationServiceTest {

    @MockBean
    private UserRepository userRepositoryMock;

    @MockBean
    private PasswordService passwordServiceMock;

    private UserValidationService userValidationService;

    @Before
    public void beforeAll() {
        this.userValidationService = new UserValidationService(userRepositoryMock, passwordServiceMock);
    }

    @Test
    public void validateNIPTest() {
        assertTrue(userValidationService.validateNIP(1234567890L));
        assertFalse(userValidationService.validateNIP(12345678901L));
        assertFalse(userValidationService.validateNIP(123456789L));
    }

    @Test
    public void validateEmailTest() {
        assertTrue(userValidationService.validateEmail("mail@gmail.com"));
        assertFalse(userValidationService.validateEmail("mail_gmail.com"));
        assertFalse(userValidationService.validateEmail("mail@gmailcom"));
        assertFalse(userValidationService.validateEmail("mail_gmail_com"));
    }

    @Test
    public void validateZipCodeTest() {
        assertTrue(userValidationService.validateZipCode("06-400"));
        assertFalse(userValidationService.validateZipCode("00900"));
        assertFalse(userValidationService.validateZipCode("12a123"));
        assertFalse(userValidationService.validateZipCode("ad-123"));
        assertFalse(userValidationService.validateZipCode("1-2344"));
        assertFalse(userValidationService.validateZipCode("123-11"));
        assertFalse(userValidationService.validateZipCode("12-1123"));
    }
}
