import jpk.controller.reckoning.mapper.MonthlyReckoningMapper;
import jpk.repository.reckoning.MonthlyReckoningRepository;
import jpk.service.reckoning.MonthlyReckoningService;
import jpk.service.record.PurchaseRecordService;
import jpk.service.record.SaleRecordService;
import jpk.service.user.TaxpayerService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest(classes = {MonthlyReckoningService.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class MonthlyReckoningServiceTest {

    @MockBean
    private PurchaseRecordService purchaseRecordService;

    @MockBean
    private SaleRecordService saleRecordService;

    @MockBean
    private MonthlyReckoningMapper monthlyReckoningMapper;

    @MockBean
    private MonthlyReckoningRepository monthlyReckoningRepository;

    @MockBean
    private TaxpayerService taxpayerService;

    private MonthlyReckoningService monthlyReckoningService;

    @Before
    public void beforeAll() {
        this.monthlyReckoningService = new MonthlyReckoningService(purchaseRecordService, saleRecordService, taxpayerService,
                monthlyReckoningMapper, monthlyReckoningRepository);
    }

    @Test
    public void createMonthlyReckoningNameFromDateTest() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        String dateAsString = "12.09.2018";
        Date date = sdf.parse(dateAsString);
        assertThat(monthlyReckoningService.createMonthlyReckoningNameFromDate(new java.sql.Date(date.getTime()))).isEqualTo("09/2018");
    }
}
