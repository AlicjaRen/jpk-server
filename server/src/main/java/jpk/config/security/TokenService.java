package jpk.config.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jpk.model.user.User;
import jpk.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
public class TokenService {

    private static final long EXPIRATION_TIME = 6L;
    private static final String SECRET = SecretGenerator.getNewSecret();
    private static final String ALGORITHM = "HS512";
    private static final String TOKEN_TYPE = "JWT";
    private static final String HEADER_STRING = "AUTH-TOKEN";

    @Autowired
    private UserService userService;

    public UserAuthentication getUserAuthentication(HttpServletRequest httpServletRequest) {
        String token = httpServletRequest.getHeader(HEADER_STRING);
        if (token != null) {
            User user = getUserFromToken(token);
            if (user != null)
                return new UserAuthentication(user);
        }
        return null;
    }

    private User getUserFromToken(String token) {
        String login = Jwts.parser()
                .setSigningKey(SECRET.getBytes())
                .parseClaimsJws(token)
                .getBody()
                .get("login", String.class);
        return userService.getUserByLogin(login);
    }

    public String generateToken(User user) {
        Date now = new Date();
        Date expiration = new Date(now.getTime() + TimeUnit.HOURS.toMillis(EXPIRATION_TIME));

        return Jwts.builder()
                .setHeaderParam("typ", TOKEN_TYPE)
                .setHeaderParam("alg", ALGORITHM)
                .claim("login", user.getLogin())
                .setIssuedAt(now)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
                .compact();
    }

}

