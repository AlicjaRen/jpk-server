package jpk.config.security.permission.user;

import jpk.exception.exceptions.UserNotFoundException;
import jpk.model.user.User;
import jpk.model.user.enums.UserRole;
import jpk.repository.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component("adminPermission")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AdminPermission {

    UserRepository userRepository;

    public boolean canUseAdmin(Authentication authentication) {
        User user = userRepository.findByLogin(authentication.getName()).orElseThrow(UserNotFoundException::new);
        return UserRole.ADMIN.equals(user.getUserRole());
    }
}
