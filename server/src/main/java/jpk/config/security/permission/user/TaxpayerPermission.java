package jpk.config.security.permission.user;

import jpk.exception.exceptions.UserNotFoundException;
import jpk.model.user.User;
import jpk.model.user.enums.UserRole;
import jpk.repository.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component("taxpayerPermission")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TaxpayerPermission {

    private UserRepository userRepository;

    public boolean canReadOrUpdateTaxpayer(Authentication authentication, Long taxpayerId) {
        User requester = userRepository.findByLogin(authentication.getName())
                .orElseThrow(UserNotFoundException::new);
        return UserRole.ADMIN.equals(requester.getUserRole()) || taxpayerId.equals(requester.getUserId());
    }

    public boolean canDeleteOrReadAllTaxpayers(Authentication authentication) {
        User requester = userRepository.findByLogin(authentication.getName())
                .orElseThrow(UserNotFoundException::new);
        return UserRole.ADMIN.equals(requester.getUserRole());
    }
}
