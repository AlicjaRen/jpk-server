package jpk.config.security.permission.user;

import jpk.exception.exceptions.UserNotFoundException;
import jpk.model.user.User;
import jpk.model.user.enums.UserRole;
import jpk.repository.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component("userPermission")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserPermission {

    private UserRepository userRepository;

    public boolean canResetPassword(Authentication authentication) {
        User requester = userRepository.findByLogin(authentication.getName())
                .orElseThrow(UserNotFoundException::new);
        return UserRole.ADMIN.equals(requester.getUserRole());
    }
}
