package jpk.config.security.permission.invoice;

import jpk.exception.exceptions.PurchaseInvoiceNotFound;
import jpk.exception.exceptions.SaleInvoiceNotFound;
import jpk.model.invoice.PurchaseInvoice;
import jpk.model.invoice.SaleInvoice;
import jpk.model.user.Taxpayer;
import jpk.repository.invoice.PurchaseInvoiceRepository;
import jpk.repository.invoice.SaleInvoiceRepository;
import jpk.service.user.TaxpayerService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component("invoicePermission")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class InvoicePermission {

    private TaxpayerService taxpayerService;
    private SaleInvoiceRepository saleInvoiceRepository;
    private PurchaseInvoiceRepository purchaseInvoiceRepository;

    public boolean canUpdateOrDeleteSaleInvoice(Authentication authentication, long invoiceId) {
        Taxpayer taxpayer = taxpayerService.findTaxpayerFromUsername(authentication.getName());
        SaleInvoice saleInvoice = saleInvoiceRepository.findBySaleInvoiceId(invoiceId)
                .orElseThrow(SaleInvoiceNotFound::new);
        return taxpayer.getMonthlyReckonings()
                .stream()
                .anyMatch(monthlyReckoning -> monthlyReckoning.getSaleRecord().getSaleInvoices().contains(saleInvoice));
    }

    public boolean canUpdateOrDeletePurchaseInvoice(Authentication authentication, long invoiceId) {
        Taxpayer taxpayer = taxpayerService.findTaxpayerFromUsername(authentication.getName());
        PurchaseInvoice purchaseInvoice = purchaseInvoiceRepository.findByPurchaseInvoiceId(invoiceId)
                .orElseThrow(PurchaseInvoiceNotFound::new);
        return taxpayer.getMonthlyReckonings()
                .stream()
                .anyMatch(monthlyReckoning -> monthlyReckoning.getPurchaseRecord().getPurchaseInvoices().contains(purchaseInvoice));
    }
}
