package jpk.config.security.permission.audit.file;

import jpk.exception.exceptions.AuditFileNotFoundException;
import jpk.exception.exceptions.UserNotFoundException;
import jpk.model.audit.file.AuditFileCorrection;
import jpk.model.reckoining.MonthlyReckoning;
import jpk.model.user.Taxpayer;
import jpk.model.user.User;
import jpk.model.user.enums.UserRole;
import jpk.repository.audit.file.AuditFileCorrectionRepository;
import jpk.repository.user.TaxpayerRepository;
import jpk.repository.user.UserRepository;
import jpk.service.user.TaxpayerService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component("auditFilePermission")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AuditFilePermission {

    private UserRepository userRepository;
    private TaxpayerRepository taxpayerRepository;
    private AuditFileCorrectionRepository auditFileCorrectionRepository;

    private TaxpayerService taxpayerService;

    public boolean canCreateAuditFileOrAuditFileCorrection(Authentication authentication, long reckoningId) {
        User user = userRepository.findByLogin(authentication.getName())
                .orElseThrow(UserNotFoundException::new);
        return UserRole.ADMIN.equals(user.getUserRole()) || taxpayerHasReckoning(user.getUserId(), reckoningId);
    }

    public boolean canReadUpdateOrDeleteAuditFile(Authentication authentication, long fileId) {
        Taxpayer taxpayer = taxpayerService.findTaxpayerFromUsername(authentication.getName());
        return taxpayer.getMonthlyReckonings().stream()
                .map(MonthlyReckoning::getAuditFile)
                .anyMatch(auditFile -> auditFile != null && auditFile.getAuditFileId().equals(fileId));
    }

    public boolean canReadUpdateOrDeleteAuditFileCorrection(Authentication authentication, long fileId) {
        AuditFileCorrection auditFileCorrection = auditFileCorrectionRepository.findByAuditFileCorrectionId(fileId)
                .orElseThrow(AuditFileNotFoundException::new);
        Taxpayer taxpayer = taxpayerService.findTaxpayerFromUsername(authentication.getName());
        return taxpayer.getMonthlyReckonings().stream()
                .map(MonthlyReckoning::getAuditFileCorrections)
                .anyMatch(auditFileCorrections -> auditFileCorrections.contains(auditFileCorrection));
    }

    private boolean taxpayerHasReckoning(Long userId, long reckoningId) {
        Taxpayer taxpayer = taxpayerRepository.findByTaxpayerId(userId)
                .orElseThrow(UserNotFoundException::new);
        return taxpayer.getMonthlyReckonings().stream()
                .map(MonthlyReckoning::getMonthlyReckoningId)
                .anyMatch(id -> id.equals(reckoningId));
    }
}
