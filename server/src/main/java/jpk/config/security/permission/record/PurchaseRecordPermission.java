package jpk.config.security.permission.record;

import jpk.model.user.Taxpayer;
import jpk.service.user.TaxpayerService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component("purchaseRecordPermission")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PurchaseRecordPermission {

    private TaxpayerService taxpayerService;

    public boolean canReadPurchaseRecord(Authentication authentication, long recordId) {
        Taxpayer taxpayer = taxpayerService.findTaxpayerFromUsername(authentication.getName());
        return taxpayer.getMonthlyReckonings()
                .stream()
                .anyMatch(reckoning -> reckoning.getPurchaseRecord().getPurchaseRecordId().equals(recordId));
    }
}
