package jpk.service.reckoning;

import jpk.controller.reckoning.mapper.MonthlyReckoningMapper;
import jpk.controller.reckoning.response.MonthlyReckoningsResponse;
import jpk.controller.reckoning.response.ReckoningResponse;
import jpk.model.reckoining.MonthlyReckoning;
import jpk.model.record.PurchaseRecord;
import jpk.model.record.SaleRecord;
import jpk.model.user.Taxpayer;
import jpk.repository.reckoning.MonthlyReckoningRepository;
import jpk.service.record.PurchaseRecordService;
import jpk.service.record.SaleRecordService;
import jpk.service.user.TaxpayerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class MonthlyReckoningService {

    private PurchaseRecordService purchaseRecordService;
    private SaleRecordService saleRecordService;
    private TaxpayerService taxpayerService;

    private MonthlyReckoningMapper monthlyReckoningMapper;

    private MonthlyReckoningRepository monthlyReckoningRepository;

    public String createMonthlyReckoningNameFromDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String dateAsString = sdf.format(date);
        String[] splitDate = dateAsString.split("-");
        return splitDate[1] + "/" + splitDate[2];
    }

    public MonthlyReckoning createMonthlyReckoning(String reckoningName, Taxpayer taxpayer) {
        PurchaseRecord purchaseRecord = purchaseRecordService.createPurchaseRecord();
        SaleRecord saleRecord = saleRecordService.createSaleRecord();
        MonthlyReckoning monthlyReckoning = MonthlyReckoning.builder()
                .name(reckoningName)
                .purchaseRecord(purchaseRecord)
                .saleRecord(saleRecord)
                .taxpayer(taxpayer)
                .auditFileCorrections(new HashSet<>())
                .build();
        purchaseRecord.setMonthlyReckoning(monthlyReckoning);
        saleRecord.setMonthlyReckoning(monthlyReckoning);

        return monthlyReckoningRepository.saveAndFlush(monthlyReckoning);
    }

    public MonthlyReckoningsResponse getMonthlyReckoningNamesForTaxpayer(String username) {

        Taxpayer taxpayer = taxpayerService.findTaxpayerFromUsername(username);

        List<MonthlyReckoning> monthlyReckonings = monthlyReckoningRepository.findAll()
                .stream()
                .filter(reckoning -> reckoning.getTaxpayer().equals(taxpayer))
                .collect(Collectors.toList());
        List<ReckoningResponse> reckonings = createReckoningNames(monthlyReckonings, taxpayer);

        ReckoningResponse lastReckoningWithoutJPK = findLastReckoningNameWithoutJPK(monthlyReckonings);
        return MonthlyReckoningsResponse
                .builder()
                .currentSuggestedReckoning(lastReckoningWithoutJPK)
                .reckonings(reckonings)
                .build();
    }

    private List<ReckoningResponse> createReckoningNames(List<MonthlyReckoning> monthlyReckonings, Taxpayer taxpayer) {

        List<ReckoningResponse> reckoningResponse = monthlyReckonings.stream()
                .map(monthlyReckoning -> monthlyReckoningMapper.mapMonthlyReckoningToReckoningNameResponse(monthlyReckoning))
                .collect(Collectors.toList());
        Pair<ReckoningResponse, ReckoningResponse> previousMonthReckonings = findTwoLastMonthReckonings();

        createReckoningIfNotExists(reckoningResponse, previousMonthReckonings.getLeft(), taxpayer);
        createReckoningIfNotExists(reckoningResponse, previousMonthReckonings.getRight(), taxpayer);

        return reckoningResponse;
    }

    private Pair<ReckoningResponse, ReckoningResponse> findTwoLastMonthReckonings() {
        Calendar calendar = Calendar.getInstance();
        Date currentDate = new Date(calendar.getTime().getTime());

        calendar.add(Calendar.MONTH, -1);
        Date lastMonthDate = new Date(calendar.getTime().getTime());

        String currentMonth = createMonthlyReckoningNameFromDate(currentDate);
        String lastMonth = createMonthlyReckoningNameFromDate(lastMonthDate);

        ReckoningResponse currentMonthReckoning = ReckoningResponse.builder()
                .name(currentMonth)
                .build();
        ReckoningResponse lastMonthReckoning = ReckoningResponse.builder()
                .name(lastMonth)
                .build();

        return Pair.of(currentMonthReckoning, lastMonthReckoning);
    }

    private void createReckoningIfNotExists(List<ReckoningResponse> reckoningResponses, ReckoningResponse reckoningName, Taxpayer taxpayer) {
        if (reckoningResponses.stream()
                .noneMatch(reckoningResponse -> reckoningResponse.getName().equals(reckoningName.getName()))) {
            MonthlyReckoning monthlyReckoning = createMonthlyReckoning(reckoningName.getName(), taxpayer);
            reckoningResponses.add(monthlyReckoningMapper.mapMonthlyReckoningToReckoningNameResponse(monthlyReckoning));
        }
    }

    private ReckoningResponse findLastReckoningNameWithoutJPK(List<MonthlyReckoning> reckonings) {
        Pair<ReckoningResponse, ReckoningResponse> twoLastMonthReckonings = findTwoLastMonthReckonings();
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        if (day <= 25) {
            ReckoningResponse lastMonthReckoning = twoLastMonthReckonings.getRight();
            return reckonings.stream()
                    .filter(reckoning -> reckoning.getName().equals(lastMonthReckoning.getName()))
                    .findFirst()
                    .map(reckoning -> monthlyReckoningMapper.mapMonthlyReckoningToReckoningNameResponse(reckoning))
                    .orElse(lastMonthReckoning);
        } else {
            ReckoningResponse currentMonthReckoning = twoLastMonthReckonings.getLeft();
            return reckonings.stream()
                    .filter(reckoning -> reckoning.getName().equals(currentMonthReckoning.getName()))
                    .findFirst()
                    .map(reckoning -> monthlyReckoningMapper.mapMonthlyReckoningToReckoningNameResponse(reckoning))
                    .orElse(currentMonthReckoning);

        }
    }
}
