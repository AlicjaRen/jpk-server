package jpk.service.record;

import jpk.controller.record.mapper.SaleRecordMapper;
import jpk.controller.record.response.SaleRecordResponse;
import jpk.exception.exceptions.RecordNotFoundException;
import jpk.model.invoice.SaleInvoice;
import jpk.model.record.SaleRecord;
import jpk.repository.record.SaleRecordRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SaleRecordService {

    private SaleRecordMapper saleRecordMapper;

    private SaleRecordRepository saleRecordRepository;

    private RecordService recordService;

    public SaleRecord createSaleRecord() {
        SaleRecord saleRecord = SaleRecord.builder()
                .saleInvoices(new HashSet<>())
                .sumNetHigh(0.0)
                .sumNetMedium(0.0)
                .sumNetLow(0.0)
                .sumVatHigh(0.0)
                .sumVatMedium(0.0)
                .sumVatLow(0.0)
                .sumGross(0.0)
                .build();
        return saleRecordRepository.saveAndFlush(saleRecord);
    }

    public void saveNewInvoiceInRecord(SaleRecord saleRecord, SaleInvoice saleInvoice) {
        saleRecord.getSaleInvoices().add(saleInvoice);
        recordService.updateSumInRecord(saleRecord, saleInvoice);
        saleInvoice.setSaleRecord(saleRecord);
        saleRecordRepository.saveAndFlush(saleRecord);
    }

    public SaleRecordResponse getSaleRecord(long recordId) {
        SaleRecord saleRecord = saleRecordRepository.findBySaleRecordId(recordId)
                .orElseThrow(RecordNotFoundException::new);
        return saleRecordMapper.mapSaleRecordToSaleRecordResponse(saleRecord);
    }

    public void updateInvoiceInRecord(SaleRecord saleRecord, SaleInvoice updatedSaleInvoice, SaleInvoice oldInvoice) {
        removeInvoiceFromRecord(oldInvoice);
        saveNewInvoiceInRecord(saleRecord, updatedSaleInvoice);
    }

    public void removeInvoiceFromRecord(SaleInvoice oldInvoice) {
        SaleRecord saleRecord = oldInvoice.getSaleRecord();
        saleRecord.getSaleInvoices().remove(oldInvoice);
        recordService.subtractRemovedInvoiceSumInRecord(saleRecord, oldInvoice);
        saleRecordRepository.saveAndFlush(saleRecord);
    }
}
