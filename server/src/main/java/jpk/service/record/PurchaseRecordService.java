package jpk.service.record;

import jpk.controller.record.mapper.PurchaseRecordMapper;
import jpk.controller.record.response.PurchaseRecordResponse;
import jpk.exception.exceptions.RecordNotFoundException;
import jpk.model.invoice.PurchaseInvoice;
import jpk.model.record.PurchaseRecord;
import jpk.repository.record.PurchaseRecordRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PurchaseRecordService {

    private PurchaseRecordMapper purchaseRecordMapper;

    private PurchaseRecordRepository purchaseRecordRepository;

    private RecordService recordService;

    public PurchaseRecord createPurchaseRecord() {
        PurchaseRecord purchaseRecord = PurchaseRecord.builder()
                .purchaseInvoices(new HashSet<>())
                .sumNetHigh(0.0)
                .sumNetMedium(0.0)
                .sumNetLow(0.0)
                .sumVatHigh(0.0)
                .sumVatMedium(0.0)
                .sumVatLow(0.0)
                .sumGross(0.0)
                .build();
        return purchaseRecordRepository.saveAndFlush(purchaseRecord);
    }

    public void saveNewInvoiceInRecord(PurchaseRecord purchaseRecord, PurchaseInvoice purchaseInvoice) {
        purchaseRecord.getPurchaseInvoices().add(purchaseInvoice);
        recordService.updateSumInRecord(purchaseRecord, purchaseInvoice);
        purchaseInvoice.setPurchaseRecord(purchaseRecord);
        purchaseRecordRepository.saveAndFlush(purchaseRecord);
    }

    public PurchaseRecordResponse getPurchaseRecord(long recordId) {
        PurchaseRecord purchaseRecord = purchaseRecordRepository.findByPurchaseRecordId(recordId)
                .orElseThrow(RecordNotFoundException::new);
        return purchaseRecordMapper.mapPurchaseRecordToResponse(purchaseRecord);
    }

    public void updateInvoiceInRecord(PurchaseRecord purchaseRecord, PurchaseInvoice updatedPurchaseInvoice, PurchaseInvoice oldInvoice) {
        removeInvoiceFromRecord(oldInvoice);
        saveNewInvoiceInRecord(purchaseRecord, updatedPurchaseInvoice);
    }

    public void removeInvoiceFromRecord(PurchaseInvoice oldInvoice) {
        PurchaseRecord purchaseRecord = oldInvoice.getPurchaseRecord();
        purchaseRecord.getPurchaseInvoices().remove(oldInvoice);
        recordService.subtractRemovedInvoiceSumInRecord(purchaseRecord, oldInvoice);
        purchaseRecordRepository.saveAndFlush(purchaseRecord);
    }
}
