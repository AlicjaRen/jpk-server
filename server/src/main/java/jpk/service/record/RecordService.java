package jpk.service.record;

import jpk.model.invoice.Invoice;
import jpk.model.record.Record;
import org.springframework.stereotype.Service;

@Service
public class RecordService {

    public <T extends Record, G extends Invoice> void updateSumInRecord(T record, G invoice) {

        // add sums from invoice to sums in record
        record.setSumNetHigh((double) Math.round((record.getSumNetHigh() + invoice.getNetHigh()) * 100) / 100);
        record.setSumNetMedium((double) Math.round((record.getSumNetMedium() + invoice.getNetMedium()) * 100) / 100);
        record.setSumNetLow((double) Math.round((record.getSumNetLow() + invoice.getNetLow()) * 100) / 100);

        record.setSumVatHigh((double) Math.round((record.getSumVatHigh() + invoice.getVatHigh()) * 100) / 100);
        record.setSumVatMedium((double) Math.round((record.getSumVatMedium() + invoice.getVatMedium()) * 100) / 100);
        record.setSumVatLow((double) Math.round((record.getSumVatLow() + invoice.getVatLow()) * 100) / 100);

        record.setSumGross((double) Math.round((record.getSumGross() + invoice.getGross()) * 100) / 100);
    }

    public <T extends Record, G extends Invoice> void subtractRemovedInvoiceSumInRecord(T record, G invoice) {

        // subtract sums from old invoice from sums in record
        record.setSumNetHigh((double) Math.round((record.getSumNetHigh() - invoice.getNetHigh()) * 100) / 100);
        record.setSumNetMedium((double) Math.round((record.getSumNetMedium() - invoice.getNetMedium()) * 100) / 100);
        record.setSumNetLow((double) Math.round((record.getSumNetLow() - invoice.getNetLow()) * 100) / 100);

        record.setSumVatHigh((double) Math.round((record.getSumVatHigh() - invoice.getVatHigh()) * 100) / 100);
        record.setSumVatMedium((double) Math.round((record.getSumVatMedium() - invoice.getVatMedium()) * 100) / 100);
        record.setSumVatLow((double) Math.round((record.getSumVatLow() - invoice.getVatLow()) * 100) / 100);

        record.setSumGross((double) Math.round((record.getSumGross() - invoice.getGross()) * 100) / 100);
    }
}
