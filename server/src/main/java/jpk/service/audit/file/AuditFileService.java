package jpk.service.audit.file;

import jpk.controller.audit.file.mapper.AuditFileMapper;
import jpk.controller.audit.file.response.AuditFileResponse;
import jpk.exception.exceptions.AuditFileAlreadyExistException;
import jpk.exception.exceptions.AuditFileNotFoundException;
import jpk.exception.exceptions.RecordNotFoundException;
import jpk.message.MessageFailed;
import jpk.message.MessageSuccess;
import jpk.model.audit.file.AuditFile;
import jpk.model.audit.file.AuditFileCorrection;
import jpk.model.audit.file.xml.*;
import jpk.model.invoice.PurchaseInvoice;
import jpk.model.invoice.SaleInvoice;
import jpk.model.reckoining.MonthlyReckoning;
import jpk.model.user.Taxpayer;
import jpk.repository.audit.file.AuditFileRepository;
import jpk.repository.reckoning.MonthlyReckoningRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AuditFileService {

    private AuditFileMapper auditFileMapper;

    private AuditFileRepository auditFileRepository;
    private MonthlyReckoningRepository monthlyReckoningRepository;

    public AuditFileResponse createAuditFile(long reckoningId) {
        MonthlyReckoning monthlyReckoning = monthlyReckoningRepository.findByMonthlyReckoningId(reckoningId)
                .orElseThrow(RecordNotFoundException::new);

        if (monthlyReckoning.getAuditFile() != null) {
            throw new AuditFileAlreadyExistException();
        }

        StandardAuditFileForTax auditFileForTax = createStandardAuditFileForTax(monthlyReckoning, false);
        String fileName = createNameForAuditFile(monthlyReckoning, auditFileForTax);

        try {
            String xmlResult = createStringAuditFileContent(auditFileForTax);
            log.info(xmlResult);

            AuditFile auditFile = auditFileMapper.mapNewFileToAuditFile(xmlResult.getBytes(), fileName, monthlyReckoning);
            auditFile = auditFileRepository.saveAndFlush(auditFile);
            return auditFileMapper.mapAuditFileToResponse(auditFile, monthlyReckoning, MessageSuccess.AUDIT_FILE_CREATED.text(), HttpStatus.CREATED);
        } catch (JAXBException e) {
            log.error("Error by creating audit xml file", e);
            return auditFileMapper.createFailAuditFileResponse(fileName, monthlyReckoning, MessageFailed.ERROR_BY_AUDIT_FILE_CREATING.text(), HttpStatus.BAD_GATEWAY);
        }
    }

    public ResponseEntity<Resource> getAuditFile(long fileId) {
        AuditFile auditFile = auditFileRepository.findByAuditFileId(fileId)
                .orElseThrow(AuditFileNotFoundException::new);
        return createResponseWithFile(auditFile.getData(), auditFile.getFileName());
    }

    public ResponseEntity<Resource> createResponseWithFile(byte[] data, String fileName) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"");
        httpHeaders.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_XML)
                .headers(httpHeaders)
                .body(new ByteArrayResource(data));
    }

    public AuditFileResponse updateAuditFile(long fileId) {
        AuditFile oldAuditFile = auditFileRepository.findByAuditFileId(fileId)
                .orElseThrow(AuditFileNotFoundException::new);

        MonthlyReckoning monthlyReckoning = oldAuditFile.getMonthlyReckoning();

        StandardAuditFileForTax auditFileForTax = createStandardAuditFileForTax(monthlyReckoning, false);

        try {
            String xmlResult = createStringAuditFileContent(auditFileForTax);
            log.info(xmlResult);

            AuditFile updatedAuditFile = auditFileMapper.mapNewFileToAuditFile(xmlResult.getBytes(), oldAuditFile.getFileName(), monthlyReckoning);
            updatedAuditFile.setAuditFileId(oldAuditFile.getAuditFileId());
            updatedAuditFile = auditFileRepository.saveAndFlush(updatedAuditFile);
            return auditFileMapper.mapAuditFileToResponse(updatedAuditFile, monthlyReckoning, MessageSuccess.AUDIT_FILE_UPDATED.text(), HttpStatus.OK);
        } catch (JAXBException e) {
            log.error("Error by creating audit xml file", e);
            return auditFileMapper.createFailAuditFileResponse(oldAuditFile.getFileName(), monthlyReckoning, MessageFailed.ERROR_BY_AUDIT_FILE_CREATING.text(), HttpStatus.BAD_GATEWAY);
        }
    }

    public void deleteAuditFile(long fileId) {
        AuditFile auditFile = auditFileRepository.findByAuditFileId(fileId)
                .orElseThrow(AuditFileNotFoundException::new);
        auditFile.getMonthlyReckoning().setAuditFile(null);
        auditFileRepository.delete(auditFile);
    }

    public String createStringAuditFileContent(StandardAuditFileForTax auditFileForTax) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(StandardAuditFileForTax.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        StringWriter writer = new StringWriter();

        marshaller.marshal(auditFileForTax, writer);

        return writer.toString();
    }

    public String createNameForAuditFile(MonthlyReckoning monthlyReckoning, StandardAuditFileForTax auditFileForTax) {
        String[] reckoningData = monthlyReckoning.getName().split("/");
        return reckoningData[0] + "_" + reckoningData[1] + "_v" + auditFileForTax.getHeader().getAimOfSubmitting() + ".xml";
    }

    public StandardAuditFileForTax createStandardAuditFileForTax(MonthlyReckoning monthlyReckoning, boolean isCorrection) {
        Header header = createHeader(monthlyReckoning, isCorrection);
        Person person = createPerson(monthlyReckoning);

        List<SaleRow> saleRows = createSaleRows(monthlyReckoning);
        SaleControl saleControl = createSaleControl(monthlyReckoning);

        List<PurchaseRow> purchaseRows = createPurchaseRows(monthlyReckoning);
        PurchaseControl purchaseControl = createPurchaseControl(monthlyReckoning);

        return StandardAuditFileForTax.builder()
                .header(header)
                .person(person)
                .saleRows(saleRows)
                .saleControl(saleControl)
                .purchaseRows(purchaseRows)
                .purchaseControl(purchaseControl)
                .build();
    }

    private PurchaseControl createPurchaseControl(MonthlyReckoning monthlyReckoning) {
        double inputTax = 0;
        int numberOfPurchaseRows = 0;
        for (PurchaseInvoice purchaseInvoice : monthlyReckoning.getPurchaseRecord().getPurchaseInvoices()) {
            if (purchaseInvoice.getDeducted()) {
                inputTax += purchaseInvoice.getVatLow() + purchaseInvoice.getVatMedium() + purchaseInvoice.getVatHigh();
                numberOfPurchaseRows++;
            }
        }
        return PurchaseControl.builder()
                .numberOfPurchaseRows(numberOfPurchaseRows != 0 ? numberOfPurchaseRows : null)
                .inputTax(inputTax != 0 ? inputTax : null)
                .build();
    }

    private List<PurchaseRow> createPurchaseRows(MonthlyReckoning monthlyReckoning) {
        List<PurchaseRow> purchaseRows = new ArrayList<>(monthlyReckoning.getPurchaseRecord().getPurchaseInvoices().size());
        monthlyReckoning.getPurchaseRecord().getPurchaseInvoices().stream()
                .filter(PurchaseInvoice::getDeducted)
                .forEach(purchaseInvoice -> {
                    double sumNet = purchaseInvoice.getNetLow() + purchaseInvoice.getNetMedium() + purchaseInvoice.getNetHigh();
                    double sumInputTax = purchaseInvoice.getVatLow() + purchaseInvoice.getVatMedium() + purchaseInvoice.getVatHigh();
                    PurchaseRow purchaseRow = PurchaseRow.builder()
                            .purchaseIndex(purchaseRows.size() + 1)
                            .supplierNIP(purchaseInvoice.getSeller().getNip().toString())
                            .supplierName(purchaseInvoice.getSeller().getName())
                            .supplierAddress(purchaseInvoice.getSeller().getAddress().toString())
                            .invoiceNumber(purchaseInvoice.getNumber())
                            .dateOfPurchase(purchaseInvoice.getDateOfIssue().toString())
                            .dateOfReceive(purchaseInvoice.getDateOfIssue().toString())
                            .sumNetFixedAsset((sumNet != 0 && purchaseInvoice.getFixedAsset()) ? sumNet : null)
                            .sumInputTaxFromFixedAsset((sumNet != 0 && purchaseInvoice.getFixedAsset()) ? sumInputTax : null)
                            .sumNetOther((sumNet != 0 && !purchaseInvoice.getFixedAsset()) ? sumNet : null)
                            .sumInputTaxFromOther((sumInputTax != 0 && !purchaseInvoice.getFixedAsset()) ? sumInputTax : null)
                            .build();
                    purchaseRows.add(purchaseRow);
                });
        return purchaseRows;
    }

    private SaleControl createSaleControl(MonthlyReckoning monthlyReckoning) {
        double taxDue = 0;
        for (SaleInvoice saleInvoice : monthlyReckoning.getSaleRecord().getSaleInvoices()) {
            taxDue += saleInvoice.getVatHigh() + saleInvoice.getVatMedium() + saleInvoice.getVatLow();
        }
        int numberOfSaleRows = monthlyReckoning.getSaleRecord().getSaleInvoices().size();
        return SaleControl.builder()
                .numberOfSaleRows(numberOfSaleRows != 0 ? numberOfSaleRows : null)
                .taxDue(taxDue != 0 ? taxDue : null)
                .build();
    }

    private List<SaleRow> createSaleRows(MonthlyReckoning monthlyReckoning) {
        List<SaleRow> saleRows = new ArrayList<>(monthlyReckoning.getSaleRecord().getSaleInvoices().size());
        monthlyReckoning.getSaleRecord().getSaleInvoices().forEach(saleInvoice -> {
            SaleRow saleRow = SaleRow.builder()
                    .saleIndex(saleRows.size() + 1)
                    .counterpartyNIP(saleInvoice.getPurchaser().getNip().toString())
                    .counterpartyName(saleInvoice.getPurchaser().getName())
                    .counterpartyAddress(saleInvoice.getPurchaser().getAddress().toString())
                    .invoiceNumber(saleInvoice.getNumber())
                    .dateOfIssue(saleInvoice.getDateOfIssue().toString())
                    .dateOfSale(saleInvoice.getDateOfIssue().toString())
                    .sumNetLow(saleInvoice.getNetLow() != 0 ? saleInvoice.getNetLow() : null)
                    .sumVatLow(saleInvoice.getVatLow() != 0 ? saleInvoice.getVatLow() : null)
                    .sumNetMedium(saleInvoice.getNetMedium() != 0 ? saleInvoice.getNetMedium() : null)
                    .sumVatMedium(saleInvoice.getVatMedium() != 0 ? saleInvoice.getVatMedium() : null)
                    .sumNetHigh(saleInvoice.getNetHigh() != 0 ? saleInvoice.getNetHigh() : null)
                    .sumVatHigh(saleInvoice.getVatHigh() != 0 ? saleInvoice.getVatHigh() : null)
                    .build();
            saleRows.add(saleRow);
        });
        return saleRows;
    }

    private Person createPerson(MonthlyReckoning monthlyReckoning) {
        Taxpayer taxpayer = monthlyReckoning.getTaxpayer();
        return Person.builder()
                .NIP(taxpayer.getNIP().toString())
                .name(taxpayer.getName())
                .email(taxpayer.getEmail())
                .build();
    }

    private Header createHeader(MonthlyReckoning monthlyReckoning, boolean isCorrection) {
        FormCode formCode = new FormCode("JPK_VAT (3)", "1-1", "JPK_VAT");
        LocalDateTime dateOfCreation = LocalDateTime.now();

        String[] reckoningData = monthlyReckoning.getName().split("/");
        int reckoningMonth = Integer.parseInt(reckoningData[0]);
        int reckoningYear = Integer.parseInt(reckoningData[1]);
        LocalDate startDate = LocalDate.of(reckoningYear, reckoningMonth, 1);
        LocalDate endDate = startDate.with(TemporalAdjusters.lastDayOfMonth());

        return Header.builder()
                .formCode(formCode)
                .formVariant(3)
                .aimOfSubmitting(isCorrection ? findAimOfSubmittingForCorrection(monthlyReckoning.getAuditFileCorrections()) : 0)
                .dateOfCreation(dateOfCreation.toString())
                .startDate(startDate.toString())
                .finishDate(endDate.toString())
                .systemName("AR_SAF-T")
                .build();
    }

    private Integer findAimOfSubmittingForCorrection(Set<AuditFileCorrection> auditFileCorrections) {
        Integer lastNumberOfCorrection = auditFileCorrections.stream()
                .map(AuditFileCorrection::getNumberOfCorrection)
                .max(Integer::compareTo).orElse(0);
        return lastNumberOfCorrection + 1;
    }
}
