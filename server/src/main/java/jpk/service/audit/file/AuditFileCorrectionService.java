package jpk.service.audit.file;

import jpk.controller.audit.file.mapper.AuditFileCorrectionMapper;
import jpk.controller.audit.file.mapper.AuditFileMapper;
import jpk.controller.audit.file.response.AuditFileResponse;
import jpk.exception.exceptions.AuditFileNotFoundException;
import jpk.exception.exceptions.RecordNotFoundException;
import jpk.message.MessageFailed;
import jpk.message.MessageSuccess;
import jpk.model.audit.file.AuditFileCorrection;
import jpk.model.audit.file.xml.StandardAuditFileForTax;
import jpk.model.reckoining.MonthlyReckoning;
import jpk.repository.audit.file.AuditFileCorrectionRepository;
import jpk.repository.reckoning.MonthlyReckoningRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;

@Slf4j
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AuditFileCorrectionService {

    private AuditFileService auditFileService;

    private AuditFileMapper auditFileMapper;
    private AuditFileCorrectionMapper auditFileCorrectionMapper;

    private MonthlyReckoningRepository monthlyReckoningRepository;
    private AuditFileCorrectionRepository auditFileCorrectionRepository;

    public AuditFileResponse createAuditFileCorrection(long reckoningId) {
        MonthlyReckoning monthlyReckoning = monthlyReckoningRepository.findByMonthlyReckoningId(reckoningId)
                .orElseThrow(RecordNotFoundException::new);

        StandardAuditFileForTax auditFileForTax = auditFileService.createStandardAuditFileForTax(monthlyReckoning, true);
        String fileName = auditFileService.createNameForAuditFile(monthlyReckoning, auditFileForTax);
        try {
            String xmlResult = auditFileService.createStringAuditFileContent(auditFileForTax);
            log.info(xmlResult);

            AuditFileCorrection auditFileCorrection = auditFileCorrectionMapper.mapNewFileToAuditFileCorrection(xmlResult.getBytes(), fileName, monthlyReckoning);
            monthlyReckoning.getAuditFileCorrections().add(auditFileCorrection);
            auditFileCorrection = auditFileCorrectionRepository.saveAndFlush(auditFileCorrection);
            return auditFileCorrectionMapper.mapAuditFileCorrectionToResponse(auditFileCorrection, monthlyReckoning, MessageSuccess.CORRECTION_AUDIT_FILE_CREATED.text(), HttpStatus.CREATED);
        } catch (JAXBException e) {
            log.error("Error by creating audit xml file", e);
            return auditFileMapper.createFailAuditFileResponse(fileName, monthlyReckoning, MessageFailed.ERROR_BY_AUDIT_FILE_CREATING.text(), HttpStatus.BAD_GATEWAY);
        }
    }

    public ResponseEntity<Resource> getAuditFileCorrection(long fileId) {
        AuditFileCorrection auditFileCorrection = auditFileCorrectionRepository.findByAuditFileCorrectionId(fileId)
                .orElseThrow(AuditFileNotFoundException::new);

        return auditFileService.createResponseWithFile(auditFileCorrection.getData(), auditFileCorrection.getFileName());
    }

    public AuditFileResponse updateAuditFileCorrection(long fileId) {
        AuditFileCorrection oldAuditFileCorrection = auditFileCorrectionRepository.findByAuditFileCorrectionId(fileId)
                .orElseThrow(AuditFileNotFoundException::new);

        MonthlyReckoning monthlyReckoning = oldAuditFileCorrection.getMonthlyReckoning();
        StandardAuditFileForTax standardAuditFileForTax = auditFileService.createStandardAuditFileForTax(monthlyReckoning, true);
        standardAuditFileForTax.getHeader().setAimOfSubmitting(oldAuditFileCorrection.getNumberOfCorrection());

        try {
            String xmlResult = auditFileService.createStringAuditFileContent(standardAuditFileForTax);
            log.info(xmlResult);

            AuditFileCorrection auditFileCorrection = auditFileCorrectionMapper.mapUpdateFileToAuditFileCorrection(xmlResult.getBytes(), oldAuditFileCorrection, monthlyReckoning);
            auditFileCorrection = auditFileCorrectionRepository.saveAndFlush(auditFileCorrection);
            return auditFileCorrectionMapper.mapAuditFileCorrectionToResponse(auditFileCorrection, monthlyReckoning, MessageSuccess.CORRECTION_AUDIT_FILE_UPDATED.text(), HttpStatus.OK);
        } catch (JAXBException e) {
            log.error("Error by creating audit xml file", e);
            return auditFileMapper.createFailAuditFileResponse(oldAuditFileCorrection.getFileName(), monthlyReckoning, MessageFailed.ERROR_BY_AUDIT_FILE_CREATING.text(), HttpStatus.BAD_GATEWAY);
        }

    }

    public void deleteAuditFileCorrection(long fileId) {
        AuditFileCorrection auditFileCorrection = auditFileCorrectionRepository.findByAuditFileCorrectionId(fileId)
                .orElseThrow(AuditFileNotFoundException::new);
        auditFileCorrection.getMonthlyReckoning().getAuditFileCorrections().remove(auditFileCorrection);
        auditFileCorrectionRepository.delete(auditFileCorrection);
    }
}
