package jpk.service.user;

import jpk.controller.user.request.account.ChangePasswordRequest;
import jpk.controller.user.request.admin.AdminUpdateRequest;
import jpk.controller.user.request.register.NewAdminRequest;
import jpk.controller.user.request.register.NewTaxpayerRequestByAdmin;
import jpk.controller.user.request.register.NewTaxpayerRequestByTaxpayer;
import jpk.controller.user.request.taxpayer.TaxpayerUpdateRequest;
import jpk.exception.exceptions.NotUniqueLoginException;
import jpk.exception.exceptions.UserNotFoundException;
import jpk.exception.exceptions.ValidationException;
import jpk.message.MessageFailed;
import jpk.model.user.User;
import jpk.repository.user.UserRepository;
import jpk.service.user.parameter.PasswordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserValidationService {

    private static final String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
    private static final int NIP_LENGTH = 10;
    private static final String ZIP_CODE_REGEX = "^[0-9]{2}-[0-9]{3}$";

    private UserRepository userRepository;
    private PasswordService passwordService;

    @Autowired
    public UserValidationService(UserRepository userRepository, PasswordService passwordService) {
        this.userRepository = userRepository;
        this.passwordService = passwordService;
    }

    public void validateNewAdmin(NewAdminRequest newAdminRequest) {
        if (!validateEmail(newAdminRequest.getEmail())) {
            throw new ValidationException(MessageFailed.NOT_VALID_EMAIL.text());
        }
    }

    public void validateNewTaxpayerCreatedByAdmin(NewTaxpayerRequestByAdmin newTaxpayerRequestByAdmin) {
        if (!validateEmail(newTaxpayerRequestByAdmin.getEmail())) {
            throw new ValidationException(MessageFailed.NOT_VALID_EMAIL.text());
        }
        if (!validateNIP(newTaxpayerRequestByAdmin.getNIP())) {
            throw new ValidationException(MessageFailed.NOT_VALID_NIP.text());
        }
        if (!validateZipCode(newTaxpayerRequestByAdmin.getAddress().getZipCode())) {
            throw new ValidationException(MessageFailed.INVALID_FORMAT_OF_ZIP_CODE.text());
        }
    }

    public void validateNewTaxpayerCreatedByTaxpayer(NewTaxpayerRequestByTaxpayer newTaxpayerRequestByTaxpayer) {
        if (!validateEmail(newTaxpayerRequestByTaxpayer.getEmail())) {
            throw new ValidationException(MessageFailed.NOT_VALID_EMAIL.text());
        }
        if (!validateNIP(newTaxpayerRequestByTaxpayer.getNIP())) {
            throw new ValidationException(MessageFailed.NOT_VALID_NIP.text());
        }
        if (!validateLogin(newTaxpayerRequestByTaxpayer.getLogin())) {
            throw new NotUniqueLoginException();
        }
        if (!confirmPasswords(newTaxpayerRequestByTaxpayer.getPassword(), newTaxpayerRequestByTaxpayer.getPasswordConfirmation())) {
            throw new ValidationException(MessageFailed.NOT_CONFIRM_PASSWORDS.text());
        }
        if (!passwordService.isStrongPassword(newTaxpayerRequestByTaxpayer.getPassword())) {
            throw new ValidationException(MessageFailed.NOT_STRONG_PASSWORD.text());
        }
        if (!validateZipCode(newTaxpayerRequestByTaxpayer.getAddress().getZipCode())) {
            throw new ValidationException(MessageFailed.INVALID_FORMAT_OF_ZIP_CODE.text());
        }
    }

    public void validateTaxpayerUpdateRequest(TaxpayerUpdateRequest taxpayerUpdateRequest, long userId) {
        checkUniqueEmail(userId, taxpayerUpdateRequest.getLogin());
        if (!validateEmail(taxpayerUpdateRequest.getEmail())) {
            throw new ValidationException(MessageFailed.NOT_VALID_EMAIL.text());
        }
        if (!validateNIP(taxpayerUpdateRequest.getNIP())) {
            throw new ValidationException(MessageFailed.NOT_VALID_NIP.text());
        }
        if (!validateZipCode(taxpayerUpdateRequest.getAddress().getZipCode())) {
            throw new ValidationException(MessageFailed.INVALID_FORMAT_OF_ZIP_CODE.text());
        }
    }

    public void validateChangePasswordRequest(ChangePasswordRequest changePasswordRequest, String userName) {
        User user = userRepository.findByLogin(userName)
                .orElseThrow(UserNotFoundException::new);

        //check current password
        if (!validateCurrentPassword(changePasswordRequest.getOldPassword(), user)) {
            throw new ValidationException(MessageFailed.NOT_VALID_CURRENT_PASSWORD.text());
        }

        if (!confirmPasswords(changePasswordRequest.getNewPassword(), changePasswordRequest.getNewPasswordConfirmation())) {
            throw new ValidationException(MessageFailed.NOT_CONFIRM_PASSWORDS.text());
        }

        if (!passwordService.isStrongPassword(changePasswordRequest.getNewPassword())) {
            throw new ValidationException(MessageFailed.NOT_STRONG_PASSWORD.text());
        }
    }

    public void validateUpdateAdminRequest(AdminUpdateRequest adminUpdateRequest, long userId) {
        if (!validateEmail(adminUpdateRequest.getEmail())) {
            throw new ValidationException(MessageFailed.NOT_VALID_EMAIL.text());
        }
        checkUniqueEmail(userId, adminUpdateRequest.getLogin());
    }

    public boolean validateNIP(Long NIP) {
        return NIP.toString().length() == NIP_LENGTH && isValidNipSchema(NIP);
    }

    private boolean isValidNipSchema(Long nip) {
        int[] weights = {6, 5, 7, 2, 3, 4, 5, 6, 7};

        // multiple by weighs and count sum
        char[] digits = nip.toString().toCharArray();
        int sum = 0;
        for (int i = 0; i < 9; i++) {
            sum += Integer.parseInt(String.valueOf(digits[i])) * weights[i];
        }
        log.debug("Sum in control NIP: {}", sum);
        return sum % 11 == Integer.parseInt(String.valueOf(nip.toString().charAt(9)));
    }

    public boolean validateEmail(String email) {
        return email.matches(EMAIL_REGEX);
    }

    public boolean validateZipCode(String zipCode) {
        return zipCode.matches(ZIP_CODE_REGEX);
    }


    private boolean validateCurrentPassword(String currentPassword, User user) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(currentPassword, user.getPassword());
    }

    private boolean confirmPasswords(String password, String passwordConfirmation) {
        return password.equals(passwordConfirmation);
    }

    private boolean validateLogin(String login) {
        return !userRepository.findByLogin(login).isPresent();
    }

    private void checkUniqueEmail(long userId, String login) {
        User user = userRepository.findByUserId(userId).orElseThrow(UserNotFoundException::new);
        if (!user.getLogin().equals(login)) {
            if (!validateLogin(login)) {
                throw new NotUniqueLoginException();
            }
        }
    }
}
