package jpk.service.user.parameter;

import org.springframework.stereotype.Service;

@Service
public class LoginService {

    private static final String ADMIN_LOGIN_PREFIX = "admin_";

    private static final String TAXPAYER_LOGIN_PREFIX = "taxp_";

    public String generateLoginForAdmin(String email) {
        return ADMIN_LOGIN_PREFIX.concat(email);
    }

    public String generateLoginForTaxpayer(String email) {
        return TAXPAYER_LOGIN_PREFIX.concat(email);
    }
}
