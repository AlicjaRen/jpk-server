package jpk.service.user.parameter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class PasswordService {
    private static final int PASSWORD_LENGTH = 8;
    private static Random generator = new Random();

    private static final Pattern strongPasswordPattern =
            Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!*(){}\\[\\]\\-_|<>?])(?=\\S+$).{8,}$");
    /*Strong password: 8 signs: min: 1 small letter, 1 capital letter, 1 digit, 1 special sign
     */


    private PasswordEncoder passwordEncoder;

    @Autowired
    public PasswordService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public String generatePassword() {
        ArrayList<Integer> groupsNumbers = new ArrayList<>(4);
        int a = generator.nextInt(PASSWORD_LENGTH - 3) + 1;
        groupsNumbers.add(a);

        int b = generator.nextInt(PASSWORD_LENGTH - a - 2) + 1;
        groupsNumbers.add(b);

        int c = generator.nextInt(PASSWORD_LENGTH - a - b - 1) + 1;
        groupsNumbers.add(c);

        int d = PASSWORD_LENGTH - a - b - c;
        groupsNumbers.add(d);

        ArrayList<Integer> indexesForGroups = createRandomIndexes(4); //{0,1,2,3} in order without duplicates

        ArrayList<Integer> singsInAscii = new ArrayList<>();

        //small letters
        int number = groupsNumbers.get(indexesForGroups.get(0));
        int range = 122 - 97 + 1; //ascii: <97, 122>
        for (int i = 0; i < number; i++) {
            singsInAscii.add(generator.nextInt(range) + 97);
        }

        //capital letters
        number = groupsNumbers.get(indexesForGroups.get(1));
        range = 90 - 65 + 1; //ascii: <65, 90>
        for (int i = 0; i < number; i++) {
            singsInAscii.add(generator.nextInt(range) + 65);
        }

        //digits
        number = groupsNumbers.get(indexesForGroups.get(2));
        range = 57 - 48 + 1; // ascii: <48,57>
        for (int i = 0; i < number; i++) {
            singsInAscii.add(generator.nextInt(range) + 48);
        }

        //special signs
        //char[] specialSigns = {'!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '{', '}', '[', ']','-','_', '+', '=', '|', '<', '>', '?'};
        int[] specialSignsInAscii = {33, 35, 36, 37, 38, 40, 41, 42, 43, 45, 60, 61, 62, 63, 64, 91, 93, 94, 95, 123, 124, 125};
        int maxSpecialSigns = specialSignsInAscii.length - 1;
        number = groupsNumbers.get(indexesForGroups.get(3));
        for (int i = 0; i < number; i++) {
            singsInAscii.add(specialSignsInAscii[generator.nextInt(maxSpecialSigns)]);
        }

        ArrayList<Integer> newIndexes = createRandomIndexes(PASSWORD_LENGTH);
        int[] mixedAsciiCode = new int[PASSWORD_LENGTH];

        for (int i = 0; i < PASSWORD_LENGTH; i++) {
            mixedAsciiCode[newIndexes.get(i)] = singsInAscii.get(i);
        }

        StringBuilder password = new StringBuilder();

        for (int asciiCode : mixedAsciiCode) {
            char sing = (char) asciiCode;
            password.append(sing);
        }

        log.debug("Generated password: {}", password.toString());
        return password.toString();
    }

    private static ArrayList<Integer> createRandomIndexes(int number) {
        Set<Integer> indexesForGroupsSet = new LinkedHashSet<>();
        while (indexesForGroupsSet.size() < number) {
            Integer next = generator.nextInt(number);
            indexesForGroupsSet.add(next);
        }
        return new ArrayList<>(indexesForGroupsSet);
    }

    public boolean isStrongPassword(String password) {
        Matcher matcher = strongPasswordPattern.matcher(password);
        return matcher.matches();
    }

    public String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }
}
