package jpk.service.user;

import jpk.controller.user.mapper.AdminMapper;
import jpk.controller.user.request.admin.AdminUpdateRequest;
import jpk.controller.user.response.admin.AdminResponse;
import jpk.exception.exceptions.UserNotFoundException;
import jpk.message.MessageSuccess;
import jpk.model.user.Admin;
import jpk.repository.user.AdminRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AdminService {

    private AdminRepository adminRepository;
    private AdminMapper adminMapper;

    public AdminResponse getAdmin(long userId) {
        Admin admin = adminRepository.findByAdminId(userId).orElseThrow(UserNotFoundException::new);
        return adminMapper.mapAdminToResponse(admin, HttpStatus.OK, "");
    }

    public List<AdminResponse> getAllAdmins() {
        List<Admin> adminsList = adminRepository.findAll();
        return adminMapper.mapAdminsListToResponse(adminsList);
    }

    public AdminResponse updateAdmin(long userId, AdminUpdateRequest adminUpdateRequest) {
        Admin oldAdmin = adminRepository.findByAdminId(userId).orElseThrow(UserNotFoundException::new);
        Admin updatedAdmin = adminMapper.mapAdminUpdateRequestToAdmin(adminUpdateRequest, oldAdmin);
        updatedAdmin = adminRepository.saveAndFlush(updatedAdmin);
        return adminMapper.mapAdminToResponse(updatedAdmin, HttpStatus.OK, MessageSuccess.ADMIN_UPDATED.text());
    }

    public void deleteAdmin(long userId) {
        Admin admin = adminRepository.findByAdminId(userId).orElseThrow(UserNotFoundException::new);
        adminRepository.delete(admin);
    }
}
