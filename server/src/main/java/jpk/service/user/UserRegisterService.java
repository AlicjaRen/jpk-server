package jpk.service.user;

import jpk.controller.address.response.AddressResponse;
import jpk.controller.user.mapper.AdminMapper;
import jpk.controller.user.mapper.TaxpayerMapper;
import jpk.controller.user.mapper.UserMapper;
import jpk.controller.user.request.register.NewAdminRequest;
import jpk.controller.user.request.register.NewTaxpayerRequestByAdmin;
import jpk.controller.user.request.register.NewTaxpayerRequestByTaxpayer;
import jpk.controller.user.response.register.NewAdminResponse;
import jpk.controller.user.response.register.NewTaxpayerResponseByAdmin;
import jpk.controller.user.response.register.NewTaxpayerResponseByTaxpayer;
import jpk.exception.exceptions.ValidationException;
import jpk.message.MessageFailed;
import jpk.message.MessageSuccess;
import jpk.model.address.Address;
import jpk.model.user.Admin;
import jpk.model.user.Taxpayer;
import jpk.model.user.User;
import jpk.repository.user.AdminRepository;
import jpk.repository.user.TaxpayerRepository;
import jpk.repository.user.UserRepository;
import jpk.service.address.AddressService;
import jpk.service.user.parameter.LoginService;
import jpk.service.user.parameter.PasswordService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserRegisterService {

    private UserMapper userMapper;
    private AdminMapper adminMapper;
    private TaxpayerMapper taxpayerMapper;

    private UserRepository userRepository;
    private AdminRepository adminRepository;
    private TaxpayerRepository taxpayerRepository;

    private AddressService addressService;
    private LoginService loginService;
    private PasswordService passwordService;

    public NewAdminResponse registerNewAdmin(NewAdminRequest newAdminRequest) {
        String plainTextPassword = passwordService.generatePassword();
        User user = registerNewUserAdminByAdmin(newAdminRequest.getEmail(), plainTextPassword);
        Admin admin = adminMapper.mapNewAdminRequestToAdmin(newAdminRequest, user);
        admin = adminRepository.saveAndFlush(admin);
        return adminMapper.mapAdminToNewAdminResponse(admin, plainTextPassword, HttpStatus.CREATED, MessageSuccess.NEW_ADMIN_SAVED.text());
    }

    private User registerNewUserAdminByAdmin(String email, String plainTextPassword) {
        String login = loginService.generateLoginForAdmin(email);
        String encodedPassword = passwordService.encodePassword(plainTextPassword);
        User user = userMapper.mapNewAdminRequestToUser(login, encodedPassword);
        try {
            user = userRepository.saveAndFlush(user);
        } catch (DataIntegrityViolationException ex) {
            throw new ValidationException(MessageFailed.NOT_UNIQUE_EMAIL.text());
        }
        return user;
    }

    public NewTaxpayerResponseByAdmin registerNewTaxpayerByAdmin(NewTaxpayerRequestByAdmin newTaxpayerRequestByAdmin) {
        String plainTextPassword = passwordService.generatePassword();
        User user = registerNewUserTaxpayerByAdmin(newTaxpayerRequestByAdmin.getEmail(), plainTextPassword);

        Pair<Address, AddressResponse> addressCreateResult = addressService.createAddress(newTaxpayerRequestByAdmin.getAddress());
        Address address = addressCreateResult.getLeft();
        AddressResponse addressResponse = addressCreateResult.getRight();

        Taxpayer taxpayer = taxpayerMapper.mapNewTaxpayerRequestToTaxpayer(newTaxpayerRequestByAdmin, user, address);
        taxpayer = taxpayerRepository.saveAndFlush(taxpayer);
        return taxpayerMapper.mapTaxpayerByAdminToTaxpayerResponse(taxpayer, addressResponse, plainTextPassword, HttpStatus.CREATED, MessageSuccess.TAXPAYER_CREATED.text());
    }

    private User registerNewUserTaxpayerByAdmin(String email, String plainTextPassword) {
        String login = loginService.generateLoginForTaxpayer(email);
        String encodedPassword = passwordService.encodePassword(plainTextPassword);
        User user = userMapper.mapNewTaxpayerRequestToUser(login, encodedPassword);
        try {
            user = userRepository.saveAndFlush(user);
        } catch (DataIntegrityViolationException ex) {
            throw new ValidationException(MessageFailed.NOT_UNIQUE_EMAIL.text());
        }
        return user;
    }

    public NewTaxpayerResponseByTaxpayer registerNewTaxpayerByTaxpayer(NewTaxpayerRequestByTaxpayer newTaxpayerRequestByTaxpayer) {
        String encodedPassword = passwordService.encodePassword(newTaxpayerRequestByTaxpayer.getPassword());
        User user = userMapper.mapNewTaxpayerRequestToUser(newTaxpayerRequestByTaxpayer.getLogin(), encodedPassword);
        try {
            user = userRepository.saveAndFlush(user);
        } catch (DataIntegrityViolationException ex) {
            throw new ValidationException(MessageFailed.NOT_UNIQUE_EMAIL.text());
        }

        Pair<Address, AddressResponse> addressCreateResult = addressService.createAddress(newTaxpayerRequestByTaxpayer.getAddress());
        Address address = addressCreateResult.getLeft();
        AddressResponse addressResponse = addressCreateResult.getRight();

        Taxpayer taxpayer = taxpayerMapper.mapNewTaxpayerRequestByTaxpayerToTaxpayer(newTaxpayerRequestByTaxpayer, user, address);
        taxpayer = taxpayerRepository.saveAndFlush(taxpayer);
        return taxpayerMapper.mapTaxpayerByTaxpayerToTaxpayerResponse(taxpayer, addressResponse, HttpStatus.CREATED, MessageSuccess.TAXPAYER_CREATED.text());
    }
}
