package jpk.service.user;

import jpk.controller.address.mapper.AddressMapper;
import jpk.controller.address.response.AddressResponse;
import jpk.controller.invoice.mapper.CounterpartyMapper;
import jpk.controller.reckoning.mapper.MonthlyReckoningMapper;
import jpk.controller.user.mapper.TaxpayerMapper;
import jpk.controller.user.request.taxpayer.TaxpayerUpdateRequest;
import jpk.controller.user.response.taxpayer.TaxpayerResponse;
import jpk.exception.exceptions.UserNotFoundException;
import jpk.message.MessageSuccess;
import jpk.model.address.Address;
import jpk.model.user.Taxpayer;
import jpk.model.user.User;
import jpk.repository.user.TaxpayerRepository;
import jpk.repository.user.UserRepository;
import jpk.service.address.AddressService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TaxpayerService {

    private UserRepository userRepository;
    private TaxpayerRepository taxpayerRepository;

    private TaxpayerMapper taxpayerMapper;
    private AddressMapper addressMapper;
    private CounterpartyMapper counterpartyMapper;
    private MonthlyReckoningMapper monthlyReckoningMapper;

    private UserService userService;
    private AddressService addressService;

    public Taxpayer findTaxpayerFromUsername(String username) {
        User user = userRepository.findByLogin(username)
                .orElseThrow(UserNotFoundException::new);
        return taxpayerRepository.findByTaxpayerId(user.getUserId())
                .orElseThrow(UserNotFoundException::new);
    }

    public TaxpayerResponse getTaxpayer(long userId) {
        Taxpayer taxpayer = taxpayerRepository.findByTaxpayerId(userId)
                .orElseThrow(UserNotFoundException::new);
        AddressResponse addressResponse = addressMapper.mapAddressToAddressResponse(taxpayer.getAddress());
        Set<Long> reckoningsIds = monthlyReckoningMapper.mapMonthlyReckoningsToIdsResponse(taxpayer.getMonthlyReckonings());
        Set<Long> counterpartiesIds = counterpartyMapper.mapCounterpartiesToIdsResponse(taxpayer.getCounterparties());

        return taxpayerMapper.mapTaxpayerToTaxpayerResponse(taxpayer, addressResponse, reckoningsIds, counterpartiesIds, HttpStatus.OK, "");
    }

    public TaxpayerResponse updateTaxpayer(TaxpayerUpdateRequest taxpayerUpdateRequest, long userId) {
        Taxpayer oldTaxpayer = taxpayerRepository.findByTaxpayerId(userId)
                .orElseThrow(UserNotFoundException::new);

        User oldUser = oldTaxpayer.getUser();
        User updatedUser = userService.updateUser(taxpayerUpdateRequest.getLogin(), oldUser);

        Pair<Address, AddressResponse> updatedAddressResponse = addressService.updateAddress(taxpayerUpdateRequest.getAddress());
        Address updatedAddress = updatedAddressResponse.getLeft();

        Taxpayer updatedTaxpayer = taxpayerMapper.mapUpdateTaxpayerRequestToTaxpayer(taxpayerUpdateRequest, updatedUser, updatedAddress, oldTaxpayer);
        updatedTaxpayer = taxpayerRepository.saveAndFlush(updatedTaxpayer);

        Set<Long> reckoningsIds = monthlyReckoningMapper.mapMonthlyReckoningsToIdsResponse(updatedTaxpayer.getMonthlyReckonings());
        Set<Long> counterpartiesIds = counterpartyMapper.mapCounterpartiesToIdsResponse(updatedTaxpayer.getCounterparties());

        return taxpayerMapper.mapTaxpayerToTaxpayerResponse(updatedTaxpayer, updatedAddressResponse.getRight(),
                reckoningsIds, counterpartiesIds, HttpStatus.OK, MessageSuccess.TAXPAYER_UPDATED.text());
    }

    public List<TaxpayerResponse> getAllTaxpayers() {
        List<Taxpayer> taxpayers = taxpayerRepository.findAll();
        return taxpayers.stream()
                .map(taxpayer -> getTaxpayer(taxpayer.getTaxpayerId()))
                .collect(Collectors.toList());
    }

    public void deleteTaxpayer(long userId) {
        Taxpayer taxpayer = taxpayerRepository.findByTaxpayerId(userId)
                .orElseThrow(UserNotFoundException::new);
        taxpayerRepository.delete(taxpayer);
    }
}
