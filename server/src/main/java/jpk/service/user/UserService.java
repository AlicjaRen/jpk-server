package jpk.service.user;

import jpk.controller.user.mapper.UserMapper;
import jpk.controller.user.request.account.ChangePasswordRequest;
import jpk.controller.user.response.account.ResetPasswordResponse;
import jpk.exception.exceptions.UserNotFoundException;
import jpk.model.user.User;
import jpk.repository.user.UserRepository;
import jpk.service.user.parameter.PasswordService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {

    private UserMapper userMapper;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private PasswordService passwordService;

    public User getUserByLogin(String login) {
        return userRepository.findByLogin(login)
                .orElseThrow(UserNotFoundException::new);
    }

    public User updateUser(String updatedLogin, User oldUser) {
        User updatedUser = userMapper.mapTaxpayerUpdateRequestToUser(updatedLogin, oldUser);
        return userRepository.saveAndFlush(updatedUser);
    }

    public void changePassword(ChangePasswordRequest changePasswordRequest, String userName) {
        User user = userRepository.findByLogin(userName).orElseThrow(UserNotFoundException::new);
        String encodedPassword = passwordEncoder.encode(changePasswordRequest.getNewPassword());
        user.setPassword(encodedPassword);
        userRepository.save(user);
    }

    public ResetPasswordResponse resetPassword(long userId) {
        User user = userRepository.findByUserId(userId).orElseThrow(UserNotFoundException::new);
        String plainTextPassword = passwordService.generatePassword();
        user.setPassword(passwordEncoder.encode(plainTextPassword));
        userRepository.save(user);
        return userMapper.mapResetPasswordToResponse(user, plainTextPassword);
    }
}
