package jpk.service.invoice.counterparty;

import jpk.controller.address.mapper.AddressMapper;
import jpk.controller.invoice.mapper.CounterpartyMapper;
import jpk.controller.invoice.request.counterparty.CounterpartyRequest;
import jpk.model.address.Address;
import jpk.model.invoice.counterparty.Counterparty;
import jpk.repository.address.AddressRepository;
import jpk.repository.invoice.counterparty.CounterpartyRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CounterpartyService {

    private CounterpartyMapper counterpartyMapper;
    private AddressMapper addressMapper;

    private AddressRepository addressRepository;
    private CounterpartyRepository counterpartyRepository;


    public Counterparty createCounterparty(CounterpartyRequest counterpartyRequest) {

        Address address = addressMapper.mapAddressRequestToAddress(counterpartyRequest.getAddress());
        Counterparty counterparty = counterpartyMapper.mapInvoiceRequestElementsToCounterparty(address, counterpartyRequest.getNip(), counterpartyRequest.getName());

        address.getCounterparties().add(counterparty);
        addressRepository.saveAndFlush(address);

        return counterparty;
    }

    public Counterparty updateCounterparty(Counterparty oldCounterparty, CounterpartyRequest counterpartyRequest) {
        Address address = addressMapper.mapAddressRequestToAddress(counterpartyRequest.getAddress());
        address.setAddressId(counterpartyRequest.getAddress().getAddressId());

        Counterparty updatedCounterparty = counterpartyMapper.mapInvoiceRequestElementsToCounterparty(address, counterpartyRequest.getNip(), counterpartyRequest.getName());
        updatedCounterparty.setCounterpartyId(counterpartyRequest.getCounterpartyId());

        address.getCounterparties().remove(oldCounterparty);
        address.getCounterparties().add(updatedCounterparty);
        addressRepository.saveAndFlush(address);

        updatedCounterparty = counterpartyRepository.saveAndFlush(updatedCounterparty);

        return updatedCounterparty;
    }
}
