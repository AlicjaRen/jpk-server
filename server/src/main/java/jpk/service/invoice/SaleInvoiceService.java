package jpk.service.invoice;

import jpk.controller.invoice.mapper.CounterpartyMapper;
import jpk.controller.invoice.mapper.InvoiceElementMapper;
import jpk.controller.invoice.mapper.SaleInvoiceMapper;
import jpk.controller.invoice.request.SaleInvoiceRequest;
import jpk.controller.invoice.response.InvoiceElementResponse;
import jpk.controller.invoice.response.SaleInvoiceResponse;
import jpk.controller.invoice.response.counterparty.CounterpartyResponse;
import jpk.exception.exceptions.SaleInvoiceNotFound;
import jpk.message.MessageSuccess;
import jpk.model.invoice.SaleInvoice;
import jpk.model.invoice.counterparty.Counterparty;
import jpk.model.invoice.element.SaleInvoiceElement;
import jpk.model.reckoining.MonthlyReckoning;
import jpk.model.user.Taxpayer;
import jpk.repository.invoice.SaleInvoiceRepository;
import jpk.repository.invoice.element.SaleInvoiceElementRepository;
import jpk.service.record.SaleRecordService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SaleInvoiceService {

    private SaleInvoiceMapper saleInvoiceMapper;
    private InvoiceElementMapper invoiceElementMapper;
    private CounterpartyMapper counterpartyMapper;

    private InvoiceService invoiceService;
    private SaleRecordService saleRecordService;

    private SaleInvoiceRepository saleInvoiceRepository;
    private SaleInvoiceElementRepository saleInvoiceElementRepository;

    public SaleInvoiceResponse createNewSaleInvoice(SaleInvoiceRequest newSaleInvoiceRequest, String userName) {
        Pair<Taxpayer, Counterparty> taxpayerCounterpartyPair = invoiceService.createCounterpartyInTaxpayer(newSaleInvoiceRequest, userName);
        Taxpayer taxpayer = taxpayerCounterpartyPair.getKey();
        Counterparty counterparty = taxpayerCounterpartyPair.getValue();

        Set<SaleInvoiceElement> saleInvoiceElements = invoiceElementMapper.mapInvoiceElementRequestsToSaleInvoiceElements(newSaleInvoiceRequest.getInvoiceElements());

        // create and save invoice
        SaleInvoice saleInvoice = saleInvoiceMapper.mapNewSaleInvoiceRequestToSaleInvoice(newSaleInvoiceRequest, counterparty, saleInvoiceElements);
        for (SaleInvoiceElement saleInvoiceElement : saleInvoiceElements) {
            saleInvoiceElement.setSaleInvoice(saleInvoice);
        }
        saleInvoice = saleInvoiceRepository.saveAndFlush(saleInvoice);

        MonthlyReckoning monthlyReckoning = invoiceService.getOrCreateMonthlyReckoning(taxpayer, saleInvoice);
        saleRecordService.saveNewInvoiceInRecord(monthlyReckoning.getSaleRecord(), saleInvoice);

        //update saleInvoice
        saleInvoice.setSaleRecord(monthlyReckoning.getSaleRecord());
        saleInvoice = saleInvoiceRepository.saveAndFlush(saleInvoice);

        // create response and return
        CounterpartyResponse counterpartyResponse = counterpartyMapper.mapCounterpartyToResponse(counterparty);
        Set<InvoiceElementResponse> invoiceElementResponses = invoiceElementMapper.mapSaleInvoiceElementsToResponse(saleInvoice.getElements());

        return saleInvoiceMapper.mapSaleInvoiceToNewSaleInvoiceResponse(saleInvoice, monthlyReckoning, counterpartyResponse,
                invoiceElementResponses, HttpStatus.CREATED, MessageSuccess.SALE_INVOICE_CREATED.text());
    }

    public SaleInvoiceResponse updateSaleInvoice(SaleInvoiceRequest saleInvoiceRequest, String userName, long oldInvoiceId) {

        SaleInvoice oldInvoice = saleInvoiceRepository.findBySaleInvoiceId(oldInvoiceId)
                .orElseThrow(SaleInvoiceNotFound::new);

        Pair<Taxpayer, Counterparty> taxpayerCounterpartyPair = invoiceService.updateCounterpartyInTaxpayer(saleInvoiceRequest, userName, oldInvoice.getPurchaser());
        Taxpayer taxpayer = taxpayerCounterpartyPair.getKey();
        Counterparty counterparty = taxpayerCounterpartyPair.getValue();

        Set<SaleInvoiceElement> saleInvoiceElements = invoiceElementMapper.mapInvoiceElementUpdateRequestsToSaleInvoiceElements(saleInvoiceRequest.getInvoiceElements());

        //delete old elements (removed after update)
        Set<SaleInvoiceElement> elementsToDelete = findRemovedInvoiceElements(oldInvoice, saleInvoiceElements);
        deleteRemovedInvoiceElements(elementsToDelete, oldInvoice);

        //set new elements in  sale invoice
        SaleInvoice saleInvoice = saleInvoiceMapper.mapUpdateSaleInvoiceRequestToSaleInvoice(saleInvoiceRequest, oldInvoice, counterparty, saleInvoiceElements);
        for (SaleInvoiceElement saleInvoiceElement : saleInvoiceElements) {
            saleInvoiceElement.setSaleInvoice(saleInvoice);
            saleInvoiceElementRepository.saveAndFlush(saleInvoiceElement);
        }

        saleInvoice.setElements(saleInvoiceElements);
        MonthlyReckoning monthlyReckoning = invoiceService.getOrCreateMonthlyReckoning(taxpayer, saleInvoice);
        saleRecordService.updateInvoiceInRecord(monthlyReckoning.getSaleRecord(), saleInvoice, oldInvoice);
        saleInvoice = saleInvoiceRepository.saveAndFlush(saleInvoice);

        //update saleInvoice
        saleInvoice.setSaleRecord(monthlyReckoning.getSaleRecord());
        saleInvoice = saleInvoiceRepository.saveAndFlush(saleInvoice);

        // create response and return
        CounterpartyResponse counterpartyResponse = counterpartyMapper.mapCounterpartyToResponse(counterparty);
        Set<InvoiceElementResponse> invoiceElementResponses = invoiceElementMapper.mapSaleInvoiceElementsToResponse(saleInvoice.getElements());

        return saleInvoiceMapper.mapSaleInvoiceToNewSaleInvoiceResponse(saleInvoice, monthlyReckoning, counterpartyResponse,
                invoiceElementResponses, HttpStatus.OK, MessageSuccess.SALE_INVOICE_UPDATED.text());
    }

    private void deleteRemovedInvoiceElements(Set<SaleInvoiceElement> elementsToDelete, SaleInvoice oldInvoice) {
        // delete deleted element from old invoice
        oldInvoice.getElements().removeAll(elementsToDelete);
        saleInvoiceRepository.saveAndFlush(oldInvoice);

        saleInvoiceElementRepository.delete(elementsToDelete);
    }

    private Set<SaleInvoiceElement> findRemovedInvoiceElements(SaleInvoice oldInvoice, Set<SaleInvoiceElement> newSaleInvoiceElements) {
        // find deleted old elements
        Set<Long> newElementsIds = newSaleInvoiceElements.stream()
                .filter(saleInvoiceElement -> saleInvoiceElement.getInvoiceElementId() != null)
                .map(SaleInvoiceElement::getInvoiceElementId)
                .collect(Collectors.toSet());
        Set<SaleInvoiceElement> elementsToDelete = oldInvoice.getElements().stream()
                .filter(saleInvoiceElement -> saleInvoiceElement.getInvoiceElementId() != null && !newElementsIds.contains(saleInvoiceElement.getInvoiceElementId()))
                .collect(Collectors.toSet());

        elementsToDelete.forEach(saleInvoiceElement -> log.debug("Element for delete with id: {} and name: {}", saleInvoiceElement.getInvoiceElementId(), saleInvoiceElement.getName()));
        return elementsToDelete;
    }

    public void deleteSaleInvoice(long invoiceId) {
        SaleInvoice saleInvoice = saleInvoiceRepository.findBySaleInvoiceId(invoiceId)
                .orElseThrow(SaleInvoiceNotFound::new);

        // update sum in record
        saleRecordService.removeInvoiceFromRecord(saleInvoice);

        saleInvoiceRepository.delete(saleInvoice);
        log.info("Deleted sale invoice with id: {}", invoiceId);
    }
}
