package jpk.service.invoice;

import com.google.common.collect.Sets;
import jpk.controller.invoice.request.InvoiceElementRequest;
import jpk.controller.invoice.request.InvoiceRequest;
import jpk.controller.invoice.request.PurchaseInvoiceRequest;
import jpk.controller.invoice.request.SaleInvoiceRequest;
import jpk.exception.exceptions.PurchaseInvoiceNotFound;
import jpk.exception.exceptions.SaleInvoiceNotFound;
import jpk.exception.exceptions.UserNotFoundException;
import jpk.exception.exceptions.ValidationException;
import jpk.message.MessageFailed;
import jpk.model.invoice.enums.TaxRate;
import jpk.model.user.Taxpayer;
import jpk.model.user.User;
import jpk.repository.invoice.PurchaseInvoiceRepository;
import jpk.repository.invoice.SaleInvoiceRepository;
import jpk.repository.user.TaxpayerRepository;
import jpk.repository.user.UserRepository;
import jpk.service.user.UserValidationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Set;

@Service
@Slf4j
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class InvoiceValidationService {

    private UserValidationService userValidationService;
    private UserRepository userRepository;
    private TaxpayerRepository taxpayerRepository;
    private SaleInvoiceRepository saleInvoiceRepository;
    private PurchaseInvoiceRepository purchaseInvoiceRepository;

    private static final Double PRECISION = 0.001;

    public void validateNewInvoiceRequest(InvoiceRequest invoiceRequest, String username) {

        User user = userRepository.findByLogin(username)
                .orElseThrow(UserNotFoundException::new);
        Taxpayer taxpayer = taxpayerRepository.findByTaxpayerId(user.getUserId())
                .orElseThrow(UserNotFoundException::new);
        if (taxpayer.getNIP().equals(invoiceRequest.getCounterparty().getNip())) {
            throw new ValidationException(MessageFailed.NIP_THE_SAME_AS_REQUESTER.text());
        }

        if (!isValidNIP(invoiceRequest.getCounterparty().getNip())) {
            throw new ValidationException(MessageFailed.NOT_VALID_NIP.text());
        }

        if (isDateOfIssueFromFuture(invoiceRequest.getDateOfIssue())) {
            throw new ValidationException(MessageFailed.DATE_FROM_FUTURE.text());
        }

        if (isZeroSumOnInvoice(invoiceRequest)) {
            throw new ValidationException(MessageFailed.ZERO_SUM_ON_INVOICE.text());
        }

        if (!grossIsEqualToVatNetSum(invoiceRequest) ||
                !sumsNetHighAreValid(invoiceRequest.getInvoiceElements(), invoiceRequest.getNetHigh()) ||
                !sumsNetMediumAreValid(invoiceRequest.getInvoiceElements(), invoiceRequest.getNetMedium()) ||
                !sumsNetLowAreValid(invoiceRequest.getInvoiceElements(), invoiceRequest.getNetLow()) ||
                !sumsVatHighAreValid(invoiceRequest.getInvoiceElements(), invoiceRequest.getVatHigh()) ||
                !sumsVatMediumAreValid(invoiceRequest.getInvoiceElements(), invoiceRequest.getVatMedium()) ||
                !sumsVatLowAreValid(invoiceRequest.getInvoiceElements(), invoiceRequest.getVatLow()) ||
                !sumsGrossAreValid(invoiceRequest.getInvoiceElements(), invoiceRequest.getGross())) {
            throw new ValidationException(MessageFailed.NOT_VALID_SUM_ON_INVOICE.text());
        }
    }

    public void validateNewPurchaseInvoiceRequest(PurchaseInvoiceRequest newPurchaseInvoiceRequest, String username) {
        validateNewInvoiceRequest(newPurchaseInvoiceRequest, username);
        Set<String> possibleReckoningNames = createPossibleReckoningNames(newPurchaseInvoiceRequest.getDateOfIssue());
        if (!possibleReckoningNames.contains(newPurchaseInvoiceRequest.getReckoningName())) {
            throw new ValidationException(MessageFailed.NOT_VALID_NAME_FOR_RECKONING.text());
        }
    }

    public void validateUpdateSaleInvoiceRequest(SaleInvoiceRequest saleInvoiceRequest, String userName, long invoiceId) {
        saleInvoiceRepository.findBySaleInvoiceId(invoiceId).orElseThrow(SaleInvoiceNotFound::new);
        validateNewInvoiceRequest(saleInvoiceRequest, userName);
    }

    public void validateUpdatePurchaseInvoiceRequest(PurchaseInvoiceRequest purchaseInvoiceRequest, String userName, long invoiceId) {
        purchaseInvoiceRepository.findByPurchaseInvoiceId(invoiceId).orElseThrow(PurchaseInvoiceNotFound::new);
        validateNewPurchaseInvoiceRequest(purchaseInvoiceRequest, userName);
    }

    private boolean isZeroSumOnInvoice(InvoiceRequest invoice) {
        return invoice.getNetHigh() == 0 && invoice.getNetMedium() == 0 && invoice.getNetLow() == 0
                && invoice.getVatHigh() == 0 && invoice.getVatMedium() == 0 && invoice.getVatLow() == 0
                && invoice.getGross() == 0;
    }

    private boolean isValidNIP(Long tradePartnerNIP) {
        return userValidationService.validateNIP(tradePartnerNIP);
    }

    private boolean isDateOfIssueFromFuture(Date dateOfIssue) {
        GregorianCalendar calendar = new GregorianCalendar();
        java.util.Date dateNow = calendar.getTime();
        return dateNow.before(dateOfIssue);
    }

    private boolean grossIsEqualToVatNetSum(InvoiceRequest invoice) {
        return Math.abs(invoice.getGross() - (invoice.getVatLow() + invoice.getVatMedium() + invoice.getVatHigh() +
                invoice.getNetLow() + invoice.getNetMedium() + invoice.getNetHigh())) < PRECISION;
    }


    private boolean sumsNetHighAreValid(Set<InvoiceElementRequest> elements, double netHigh) {
        double netHighSum = elements.stream()
                .filter(invoiceElementRequest -> invoiceElementRequest.getTaxRate().equals(TaxRate.HIGH))
                .mapToDouble(InvoiceElementRequest::getNet)
                .sum();
        return Math.abs(netHighSum - netHigh) < PRECISION;
    }


    private boolean sumsNetMediumAreValid(Set<InvoiceElementRequest> elements, double netMedium) {
        double netMediumSum = elements.stream()
                .filter(invoiceElementRequest -> invoiceElementRequest.getTaxRate().equals(TaxRate.MEDIUM))
                .mapToDouble(InvoiceElementRequest::getNet)
                .sum();
        return Math.abs(netMediumSum - netMedium) < PRECISION;
    }

    private boolean sumsNetLowAreValid(Set<InvoiceElementRequest> elements, double netLow) {
        double netLowSum = elements.stream()
                .filter(invoiceElementRequest -> invoiceElementRequest.getTaxRate().equals(TaxRate.LOW))
                .mapToDouble(InvoiceElementRequest::getNet)
                .sum();
        return Math.abs(netLowSum - netLow) < PRECISION;
    }

    private boolean sumsVatHighAreValid(Set<InvoiceElementRequest> elements, double vatHigh) {
        double vatHighSum = elements.stream()
                .filter(invoiceElementRequest -> invoiceElementRequest.getTaxRate().equals(TaxRate.HIGH))
                .mapToDouble(InvoiceElementRequest::getVat)
                .sum();
        return Math.abs(vatHighSum - vatHigh) < PRECISION;
    }

    private boolean sumsVatMediumAreValid(Set<InvoiceElementRequest> elements, double vatMedium) {
        double vatMediumSum = elements.stream()
                .filter(invoiceElementRequest -> invoiceElementRequest.getTaxRate().equals(TaxRate.MEDIUM))
                .mapToDouble(InvoiceElementRequest::getVat)
                .sum();
        return Math.abs(vatMediumSum - vatMedium) < PRECISION;
    }

    private boolean sumsVatLowAreValid(Set<InvoiceElementRequest> elements, double vatLow) {
        double vatLowSum = elements.stream()
                .filter(invoiceElementRequest -> invoiceElementRequest.getTaxRate().equals(TaxRate.LOW))
                .mapToDouble(InvoiceElementRequest::getVat)
                .sum();
        return Math.abs(vatLowSum - vatLow) < PRECISION;
    }

    private boolean sumsGrossAreValid(Set<InvoiceElementRequest> invoiceElements, double gross) {
        double grossSum = invoiceElements.stream()
                .mapToDouble(InvoiceElementRequest::getGross)
                .sum();
        return Math.abs(grossSum - gross) < PRECISION;
    }

    private Set<String> createPossibleReckoningNames(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String dateAsString = sdf.format(date);
        String[] splitDate = dateAsString.split("-");

        String firstNameMonth = splitDate[1];
        String firstNameYear = splitDate[2];

        // normal case
        String secondNameMonth = String.valueOf(Integer.valueOf(firstNameMonth) + 1);
        String thirdNameMonth = String.valueOf(Integer.valueOf(firstNameMonth) + 2);
        String secondNameYear = firstNameYear;
        String thirdNameYear = firstNameYear;

        // boundary cases
        if (Integer.valueOf(secondNameMonth) < 10) {
            secondNameMonth = "0" + secondNameMonth;
        }
        if (Integer.valueOf(firstNameMonth) == 12) {
            secondNameMonth = "01";
            secondNameYear = String.valueOf(Integer.valueOf(firstNameYear) + 1);
            thirdNameMonth = "02";
            thirdNameYear = secondNameYear;
        }
        if (Integer.valueOf(firstNameMonth) == 11) {
            thirdNameMonth = "01";
            thirdNameYear = String.valueOf(Integer.valueOf(firstNameYear) + 1);
        }

        String firstName = firstNameMonth + "/" + firstNameYear;
        String secondName = secondNameMonth + "/" + secondNameYear;
        String thirdName = thirdNameMonth + "/" + thirdNameYear;

        log.info("Possible names for reckoning: {}, {} or {}", firstName, secondName, thirdName);
        return Sets.newHashSet(firstName, secondName, thirdName);
    }
}
