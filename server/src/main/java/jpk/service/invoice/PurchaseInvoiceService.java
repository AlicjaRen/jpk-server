package jpk.service.invoice;

import jpk.controller.invoice.mapper.CounterpartyMapper;
import jpk.controller.invoice.mapper.InvoiceElementMapper;
import jpk.controller.invoice.mapper.PurchaseInvoiceMapper;
import jpk.controller.invoice.request.PurchaseInvoiceRequest;
import jpk.controller.invoice.response.InvoiceElementResponse;
import jpk.controller.invoice.response.PurchaseInvoiceResponse;
import jpk.controller.invoice.response.counterparty.CounterpartyResponse;
import jpk.exception.exceptions.PurchaseInvoiceNotFound;
import jpk.message.MessageSuccess;
import jpk.model.invoice.PurchaseInvoice;
import jpk.model.invoice.counterparty.Counterparty;
import jpk.model.invoice.element.PurchaseInvoiceElement;
import jpk.model.reckoining.MonthlyReckoning;
import jpk.model.user.Taxpayer;
import jpk.repository.invoice.PurchaseInvoiceRepository;
import jpk.repository.invoice.element.PurchaseInvoiceElementRepository;
import jpk.repository.reckoning.MonthlyReckoningRepository;
import jpk.service.reckoning.MonthlyReckoningService;
import jpk.service.record.PurchaseRecordService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PurchaseInvoiceService {

    private PurchaseInvoiceMapper purchaseInvoiceMapper;
    private InvoiceElementMapper invoiceElementMapper;
    private CounterpartyMapper counterpartyMapper;

    private MonthlyReckoningService monthlyReckoningService;
    private PurchaseRecordService purchaseRecordService;
    private InvoiceService invoiceService;

    private PurchaseInvoiceRepository purchaseInvoiceRepository;
    private MonthlyReckoningRepository monthlyReckoningRepository;
    private PurchaseInvoiceElementRepository purchaseInvoiceElementRepository;

    public PurchaseInvoiceResponse createNewPurchaseInvoice(PurchaseInvoiceRequest newPurchaseInvoiceRequest, String userName) {

        Pair<Taxpayer, Counterparty> taxpayerCounterpartyPair = invoiceService.createCounterpartyInTaxpayer(newPurchaseInvoiceRequest, userName);
        Taxpayer taxpayer = taxpayerCounterpartyPair.getKey();
        Counterparty counterparty = taxpayerCounterpartyPair.getValue();

        Set<PurchaseInvoiceElement> purchaseInvoiceElements = invoiceElementMapper.mapInvoiceElementRequestsToPurchaseInvoiceElements(newPurchaseInvoiceRequest.getInvoiceElements());

        // create and save invoice
        PurchaseInvoice purchaseInvoice = purchaseInvoiceMapper.mapNewPurchaseInvoiceRequestToPurchaseInvoice(newPurchaseInvoiceRequest, counterparty, purchaseInvoiceElements);
        for (PurchaseInvoiceElement purchaseInvoiceElement : purchaseInvoiceElements) {
            purchaseInvoiceElement.setPurchaseInvoice(purchaseInvoice);
        }
        purchaseInvoice = purchaseInvoiceRepository.saveAndFlush(purchaseInvoice);

        // (create if not exists monthly reckoning) and save invoice in Record - get Record from Monthly Reckoning
        String reckoningName = newPurchaseInvoiceRequest.getReckoningName();
        MonthlyReckoning monthlyReckoning = monthlyReckoningRepository.findByNameAndTaxpayer(reckoningName, taxpayer)
                .orElseGet(() -> monthlyReckoningService.createMonthlyReckoning(reckoningName, taxpayer));
        purchaseRecordService.saveNewInvoiceInRecord(monthlyReckoning.getPurchaseRecord(), purchaseInvoice);

        //update purchaseInvoice
        purchaseInvoice.setPurchaseRecord(monthlyReckoning.getPurchaseRecord());
        purchaseInvoice = purchaseInvoiceRepository.saveAndFlush(purchaseInvoice);

        // create response and return
        CounterpartyResponse counterpartyResponse = counterpartyMapper.mapCounterpartyToResponse(counterparty);
        Set<InvoiceElementResponse> invoiceElementResponses = invoiceElementMapper.mapPurchaseInvoiceElementsToResponse(purchaseInvoice.getElements());

        return purchaseInvoiceMapper.mapPurchaseInvoiceToNewPurchaseInvoiceResponse(purchaseInvoice, monthlyReckoning, counterpartyResponse,
                invoiceElementResponses, HttpStatus.CREATED, MessageSuccess.PURCHASE_INVOICE_CREATED.text());
    }

    public PurchaseInvoiceResponse updatePurchaseInvoice(PurchaseInvoiceRequest purchaseInvoiceRequest, String userName, long oldInvoiceId) {
        PurchaseInvoice oldInvoice = purchaseInvoiceRepository.findByPurchaseInvoiceId(oldInvoiceId)
                .orElseThrow(PurchaseInvoiceNotFound::new);

        Pair<Taxpayer, Counterparty> taxpayerCounterpartyPair = invoiceService.updateCounterpartyInTaxpayer(purchaseInvoiceRequest, userName, oldInvoice.getSeller());
        Taxpayer taxpayer = taxpayerCounterpartyPair.getKey();
        Counterparty counterparty = taxpayerCounterpartyPair.getValue();

        Set<PurchaseInvoiceElement> purchaseInvoiceElements = invoiceElementMapper.mapInvoiceElementUpdateRequestsToPurchaseInvoiceElements(purchaseInvoiceRequest.getInvoiceElements());

        //delete old elements (removed after update)
        Set<PurchaseInvoiceElement> elementsToDelete = findRemovedInvoiceElements(oldInvoice, purchaseInvoiceElements);
        deleteRemovedInvoiceElements(elementsToDelete, oldInvoice);

        //set new elements in  purchase invoice
        PurchaseInvoice purchaseInvoice = purchaseInvoiceMapper.mapUpdateSaleInvoiceRequestToPurchaseInvoice(purchaseInvoiceRequest, oldInvoice, counterparty, purchaseInvoiceElements);
        for (PurchaseInvoiceElement purchaseInvoiceElement : purchaseInvoiceElements) {
            purchaseInvoiceElement.setPurchaseInvoice(purchaseInvoice);
            purchaseInvoiceElementRepository.saveAndFlush(purchaseInvoiceElement);
        }

        purchaseInvoice.setElements(purchaseInvoiceElements);
        String reckoningName = purchaseInvoiceRequest.getReckoningName();
        MonthlyReckoning monthlyReckoning = monthlyReckoningRepository.findByNameAndTaxpayer(reckoningName, taxpayer)
                .orElseGet(() -> monthlyReckoningService.createMonthlyReckoning(reckoningName, taxpayer));
        purchaseRecordService.updateInvoiceInRecord(monthlyReckoning.getPurchaseRecord(), purchaseInvoice, oldInvoice);
        purchaseInvoice = purchaseInvoiceRepository.saveAndFlush(purchaseInvoice);

        //update purchaseInvoice
        purchaseInvoice.setPurchaseRecord(monthlyReckoning.getPurchaseRecord());
        purchaseInvoice = purchaseInvoiceRepository.saveAndFlush(purchaseInvoice);

        // create response and return
        CounterpartyResponse counterpartyResponse = counterpartyMapper.mapCounterpartyToResponse(counterparty);
        Set<InvoiceElementResponse> invoiceElementResponses = invoiceElementMapper.mapPurchaseInvoiceElementsToResponse(purchaseInvoice.getElements());

        return purchaseInvoiceMapper.mapPurchaseInvoiceToNewPurchaseInvoiceResponse(purchaseInvoice, monthlyReckoning, counterpartyResponse,
                invoiceElementResponses, HttpStatus.OK, MessageSuccess.PURCHASE_INVOICE_UPDATED.text());
    }

    private void deleteRemovedInvoiceElements(Set<PurchaseInvoiceElement> elementsToDelete, PurchaseInvoice oldInvoice) {
        // delete deleted element from old invoice
        oldInvoice.getElements().removeAll(elementsToDelete);
        purchaseInvoiceRepository.saveAndFlush(oldInvoice);

        purchaseInvoiceElementRepository.delete(elementsToDelete);
    }

    private Set<PurchaseInvoiceElement> findRemovedInvoiceElements(PurchaseInvoice oldInvoice, Set<PurchaseInvoiceElement> newPurchaseInvoiceElements) {
        //find deleted old elements
        Set<Long> newElementsIds = newPurchaseInvoiceElements.stream()
                .filter(saleInvoiceElement -> saleInvoiceElement.getInvoiceElementId() != null)
                .map(PurchaseInvoiceElement::getInvoiceElementId)
                .collect(Collectors.toSet());
        Set<PurchaseInvoiceElement> elementsToDelete = oldInvoice.getElements().stream()
                .filter(saleInvoiceElement -> saleInvoiceElement.getInvoiceElementId() != null && !newElementsIds.contains(saleInvoiceElement.getInvoiceElementId()))
                .collect(Collectors.toSet());

        elementsToDelete.forEach(saleInvoiceElement -> log.debug("Element for delete with id: {} and name: {}", saleInvoiceElement.getInvoiceElementId(), saleInvoiceElement.getName()));
        return elementsToDelete;
    }

    public void deletePurchaseInvoice(long invoiceId) {
        PurchaseInvoice purchaseInvoice = purchaseInvoiceRepository.findByPurchaseInvoiceId(invoiceId)
                .orElseThrow(PurchaseInvoiceNotFound::new);

        // update sum in record
        purchaseRecordService.removeInvoiceFromRecord(purchaseInvoice);

        purchaseInvoiceRepository.delete(purchaseInvoice);
        log.info("Deleted purchase invoice with id: {}", invoiceId);
    }
}
