package jpk.service.invoice;

import jpk.controller.invoice.request.InvoiceRequest;
import jpk.model.invoice.Invoice;
import jpk.model.invoice.counterparty.Counterparty;
import jpk.model.reckoining.MonthlyReckoning;
import jpk.model.user.Taxpayer;
import jpk.repository.invoice.counterparty.CounterpartyRepository;
import jpk.repository.reckoning.MonthlyReckoningRepository;
import jpk.service.invoice.counterparty.CounterpartyService;
import jpk.service.reckoning.MonthlyReckoningService;
import jpk.service.user.TaxpayerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class InvoiceService {

    private CounterpartyRepository counterpartyRepository;
    private MonthlyReckoningRepository monthlyReckoningRepository;

    private TaxpayerService taxpayerService;
    private CounterpartyService counterpartyService;
    private MonthlyReckoningService monthlyReckoningService;

    public <T extends InvoiceRequest> Pair<Taxpayer, Counterparty> createCounterpartyInTaxpayer(T newInvoiceRequest, String username) {

        Taxpayer taxpayer = taxpayerService.findTaxpayerFromUsername(username);

        Counterparty counterparty = counterpartyRepository.findByNip(newInvoiceRequest.getCounterparty().getNip())
                .orElseGet(() -> counterpartyService.createCounterparty(newInvoiceRequest.getCounterparty()));

        taxpayer.getCounterparties().add(counterparty);
        return Pair.of(taxpayer, counterparty);
    }

    public <T extends Invoice> MonthlyReckoning getOrCreateMonthlyReckoning(Taxpayer taxpayer, T invoice) {
        String reckoningName = monthlyReckoningService.createMonthlyReckoningNameFromDate(invoice.getDateOfIssue());
        return monthlyReckoningRepository.findByNameAndTaxpayer(reckoningName, taxpayer)
                .orElseGet(() -> monthlyReckoningService.createMonthlyReckoning(reckoningName, taxpayer));
    }

    public <T extends InvoiceRequest> Pair<Taxpayer, Counterparty> updateCounterpartyInTaxpayer(T saleInvoiceRequest,
                                                                                                String userName, Counterparty oldCounterparty) {

        //if NIP changed -> create new Counterparty or get from Taxpayer's counterparties
        if (!oldCounterparty.getNip().equals(saleInvoiceRequest.getCounterparty().getNip())) {
            log.debug("Counterparty NIP changed");
            return createCounterpartyInTaxpayer(saleInvoiceRequest, userName);
        }

        // if NIP not changed -> update this Counterparty data
        else {
            log.debug("Counterparty NIP not changed");
            Taxpayer taxpayer = taxpayerService.findTaxpayerFromUsername(userName);
            Counterparty updatedCounterparty = counterpartyService.updateCounterparty(oldCounterparty, saleInvoiceRequest.getCounterparty());

            taxpayer.getCounterparties().remove(oldCounterparty);
            taxpayer.getCounterparties().add(updatedCounterparty);

            return Pair.of(taxpayer, updatedCounterparty);
        }
    }
}
