package jpk.service.address;

import jpk.controller.address.mapper.AddressMapper;
import jpk.controller.address.request.AddressRequest;
import jpk.controller.address.response.AddressResponse;
import jpk.exception.exceptions.AddressNotFoundException;
import jpk.model.address.Address;
import jpk.repository.address.AddressRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AddressService {

    private AddressRepository addressRepository;
    private AddressMapper addressMapper;

    public Pair<Address, AddressResponse> createAddress(AddressRequest addressRequest) {
        Address address = addressMapper.mapAddressRequestToAddress(addressRequest);
        address = addressRepository.saveAndFlush(address);
        AddressResponse addressResponse = addressMapper.mapAddressToAddressResponse(address);
        return Pair.of(address, addressResponse);
    }

    public Pair<Address, AddressResponse> updateAddress(AddressRequest updateAddressRequest) {
        Address oldAddress = addressRepository.findByAddressId(updateAddressRequest.getAddressId())
                .orElseThrow(AddressNotFoundException::new);
        Address address = addressMapper.mapUpdateAddressRequestToAddress(updateAddressRequest, oldAddress);
        address = addressRepository.saveAndFlush(address);
        AddressResponse addressResponse = addressMapper.mapAddressToAddressResponse(address);
        return Pair.of(address, addressResponse);
    }
}
