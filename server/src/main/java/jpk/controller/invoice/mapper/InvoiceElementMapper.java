package jpk.controller.invoice.mapper;

import jpk.controller.invoice.request.InvoiceElementRequest;
import jpk.controller.invoice.response.InvoiceElementResponse;
import jpk.model.invoice.element.PurchaseInvoiceElement;
import jpk.model.invoice.element.SaleInvoiceElement;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class InvoiceElementMapper {

    public Set<InvoiceElementResponse> mapSaleInvoiceElementsToResponse(Set<SaleInvoiceElement> saleInvoiceElements) {
        return saleInvoiceElements.stream()
                .map(this::mapSaleInvoiceElementToInvoiceElementResponse)
                .collect(Collectors.toSet());
    }

    public Set<SaleInvoiceElement> mapInvoiceElementRequestsToSaleInvoiceElements(Set<InvoiceElementRequest> invoiceElementRequests) {
        return invoiceElementRequests.stream()
                .map(this::mapInvoiceElementRequestToSaleInvoiceElement)
                .collect(Collectors.toSet());
    }

    public Set<PurchaseInvoiceElement> mapInvoiceElementRequestsToPurchaseInvoiceElements(Set<InvoiceElementRequest> invoiceElementRequests) {
        return invoiceElementRequests.stream()
                .map(this::mapInvoiceElementRequestToPurchaseInvoiceElement)
                .collect(Collectors.toSet());
    }

    public Set<InvoiceElementResponse> mapPurchaseInvoiceElementsToResponse(Set<PurchaseInvoiceElement> purchaseInvoiceElements) {
        return purchaseInvoiceElements.stream()
                .map(this::mapPurchaseInvoiceElementToInvoiceElementResponse)
                .collect(Collectors.toSet());
    }

    public Set<SaleInvoiceElement> mapInvoiceElementUpdateRequestsToSaleInvoiceElements(Set<InvoiceElementRequest> invoiceElements) {
        return invoiceElements.stream()
                .map(this::mapInvoiceElementUpdateRequestToSaleInvoiceElement)
                .collect(Collectors.toSet());
    }

    public Set<PurchaseInvoiceElement> mapInvoiceElementUpdateRequestsToPurchaseInvoiceElements(Set<InvoiceElementRequest> invoiceElements) {
        return invoiceElements.stream()
                .map(this::mapInvoiceElementUpdateRequestsToPurchaseInvoiceElement)
                .collect(Collectors.toSet());
    }

    private InvoiceElementResponse mapSaleInvoiceElementToInvoiceElementResponse(SaleInvoiceElement saleInvoiceElement) {
        return InvoiceElementResponse.builder()
                .invoiceElementId(saleInvoiceElement.getInvoiceElementId())
                .name(saleInvoiceElement.getName())
                .net(saleInvoiceElement.getNet())
                .vat(saleInvoiceElement.getVat())
                .gross(saleInvoiceElement.getGross())
                .taxRate(saleInvoiceElement.getTaxRate())
                .build();
    }

    private SaleInvoiceElement mapInvoiceElementRequestToSaleInvoiceElement(InvoiceElementRequest invoiceElementRequest) {
        return SaleInvoiceElement.builder()
                .name(invoiceElementRequest.getName())
                .net(invoiceElementRequest.getNet())
                .vat(invoiceElementRequest.getVat())
                .gross(invoiceElementRequest.getGross())
                .taxRate(invoiceElementRequest.getTaxRate())
                .build();
    }

    private PurchaseInvoiceElement mapInvoiceElementRequestToPurchaseInvoiceElement(InvoiceElementRequest invoiceElementRequest) {
        return PurchaseInvoiceElement.builder()
                .name(invoiceElementRequest.getName())
                .net(invoiceElementRequest.getNet())
                .vat(invoiceElementRequest.getVat())
                .gross(invoiceElementRequest.getGross())
                .taxRate(invoiceElementRequest.getTaxRate())
                .build();
    }

    private InvoiceElementResponse mapPurchaseInvoiceElementToInvoiceElementResponse(PurchaseInvoiceElement purchaseInvoiceElement) {
        return InvoiceElementResponse.builder()
                .invoiceElementId(purchaseInvoiceElement.getInvoiceElementId())
                .name(purchaseInvoiceElement.getName())
                .net(purchaseInvoiceElement.getNet())
                .vat(purchaseInvoiceElement.getVat())
                .gross(purchaseInvoiceElement.getGross())
                .taxRate(purchaseInvoiceElement.getTaxRate())
                .build();
    }

    private SaleInvoiceElement mapInvoiceElementUpdateRequestToSaleInvoiceElement(InvoiceElementRequest invoiceElementRequest) {
        SaleInvoiceElement saleInvoiceElement = mapInvoiceElementRequestToSaleInvoiceElement(invoiceElementRequest);
        saleInvoiceElement.setInvoiceElementId(invoiceElementRequest.getInvoiceElementId());
        return saleInvoiceElement;
    }

    private PurchaseInvoiceElement mapInvoiceElementUpdateRequestsToPurchaseInvoiceElement(InvoiceElementRequest invoiceElementRequest) {
        PurchaseInvoiceElement purchaseInvoiceElement = mapInvoiceElementRequestToPurchaseInvoiceElement(invoiceElementRequest);
        purchaseInvoiceElement.setInvoiceElementId(invoiceElementRequest.getInvoiceElementId());
        return purchaseInvoiceElement;
    }
}
