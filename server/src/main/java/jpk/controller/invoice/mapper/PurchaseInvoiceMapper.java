package jpk.controller.invoice.mapper;

import jpk.controller.invoice.request.PurchaseInvoiceRequest;
import jpk.controller.invoice.response.InvoiceElementResponse;
import jpk.controller.invoice.response.PurchaseInvoiceResponse;
import jpk.controller.invoice.response.counterparty.CounterpartyResponse;
import jpk.model.invoice.PurchaseInvoice;
import jpk.model.invoice.counterparty.Counterparty;
import jpk.model.invoice.element.PurchaseInvoiceElement;
import jpk.model.reckoining.MonthlyReckoning;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PurchaseInvoiceMapper {
    public PurchaseInvoice mapNewPurchaseInvoiceRequestToPurchaseInvoice(PurchaseInvoiceRequest newPurchaseInvoiceRequest,
                                                                         Counterparty seller, Set<PurchaseInvoiceElement> purchaseInvoiceElements) {
        return PurchaseInvoice.builder()
                .elements(purchaseInvoiceElements)
                .seller(seller)
                .number(newPurchaseInvoiceRequest.getNumber())
                .dateOfIssue(newPurchaseInvoiceRequest.getDateOfIssue())
                .deducted(newPurchaseInvoiceRequest.isDeducted())
                .fixedAsset(newPurchaseInvoiceRequest.isFixedAsset())
                .netHigh(newPurchaseInvoiceRequest.getNetHigh())
                .netMedium(newPurchaseInvoiceRequest.getNetMedium())
                .netLow(newPurchaseInvoiceRequest.getNetLow())
                .vatHigh(newPurchaseInvoiceRequest.getVatHigh())
                .vatMedium(newPurchaseInvoiceRequest.getVatMedium())
                .vatLow(newPurchaseInvoiceRequest.getVatLow())
                .gross(newPurchaseInvoiceRequest.getGross())
                .build();
    }

    public PurchaseInvoiceResponse mapPurchaseInvoiceToNewPurchaseInvoiceResponse(PurchaseInvoice purchaseInvoice, MonthlyReckoning monthlyReckoning, CounterpartyResponse counterpartyResponse, Set<InvoiceElementResponse> invoiceElementResponses, HttpStatus httpStatus, String message) {
        return PurchaseInvoiceResponse.builder()
                .invoiceId(purchaseInvoice.getPurchaseInvoiceId())
                .number(purchaseInvoice.getNumber())
                .dateOfIssue(purchaseInvoice.getDateOfIssue())
                .deducted(purchaseInvoice.getDeducted())
                .fixedAsset(purchaseInvoice.getFixedAsset())
                .counterparty(counterpartyResponse)
                .netHigh(purchaseInvoice.getNetHigh())
                .netMedium(purchaseInvoice.getNetMedium())
                .netLow(purchaseInvoice.getNetLow())
                .vatHigh(purchaseInvoice.getVatHigh())
                .vatMedium(purchaseInvoice.getVatMedium())
                .vatLow(purchaseInvoice.getVatLow())
                .gross(purchaseInvoice.getGross())
                .invoiceElements(invoiceElementResponses)
                .recordId(monthlyReckoning.getSaleRecord().getSaleRecordId())
                .taxpayerId(monthlyReckoning.getTaxpayer().getTaxpayerId())
                .httpStatus(httpStatus)
                .message(message)
                .build();
    }

    public PurchaseInvoice mapUpdateSaleInvoiceRequestToPurchaseInvoice(PurchaseInvoiceRequest purchaseInvoiceRequest, PurchaseInvoice oldInvoice, Counterparty counterparty, Set<PurchaseInvoiceElement> purchaseInvoiceElements) {
        PurchaseInvoice purchaseInvoice = mapNewPurchaseInvoiceRequestToPurchaseInvoice(purchaseInvoiceRequest, counterparty, purchaseInvoiceElements);
        purchaseInvoice.setPurchaseInvoiceId(oldInvoice.getPurchaseInvoiceId());
        return purchaseInvoice;
    }
}
