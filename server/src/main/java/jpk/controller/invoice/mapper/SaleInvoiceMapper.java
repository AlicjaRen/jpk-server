package jpk.controller.invoice.mapper;

import jpk.controller.invoice.request.SaleInvoiceRequest;
import jpk.controller.invoice.response.InvoiceElementResponse;
import jpk.controller.invoice.response.SaleInvoiceResponse;
import jpk.controller.invoice.response.counterparty.CounterpartyResponse;
import jpk.model.invoice.SaleInvoice;
import jpk.model.invoice.counterparty.Counterparty;
import jpk.model.invoice.element.SaleInvoiceElement;
import jpk.model.reckoining.MonthlyReckoning;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class SaleInvoiceMapper {
    public SaleInvoice mapNewSaleInvoiceRequestToSaleInvoice(SaleInvoiceRequest newSaleInvoiceRequest, Counterparty purchaser,
                                                             Set<SaleInvoiceElement> saleInvoiceElements) {
        return SaleInvoice.builder()
                .elements(saleInvoiceElements)
                .purchaser(purchaser)
                .number(newSaleInvoiceRequest.getNumber())
                .dateOfIssue(newSaleInvoiceRequest.getDateOfIssue())
                .netHigh(newSaleInvoiceRequest.getNetHigh())
                .netMedium(newSaleInvoiceRequest.getNetMedium())
                .netLow(newSaleInvoiceRequest.getNetLow())
                .vatHigh(newSaleInvoiceRequest.getVatHigh())
                .vatMedium(newSaleInvoiceRequest.getVatMedium())
                .vatLow(newSaleInvoiceRequest.getVatLow())
                .gross(newSaleInvoiceRequest.getGross())
                .build();
    }

    public SaleInvoiceResponse mapSaleInvoiceToNewSaleInvoiceResponse(SaleInvoice saleInvoice, MonthlyReckoning monthlyReckoning,
                                                                      CounterpartyResponse counterpartyResponse,
                                                                      Set<InvoiceElementResponse> invoiceElementResponses,
                                                                      HttpStatus httpStatus, String message) {
        return SaleInvoiceResponse.builder()
                .invoiceId(saleInvoice.getSaleInvoiceId())
                .number(saleInvoice.getNumber())
                .dateOfIssue(saleInvoice.getDateOfIssue())
                .counterparty(counterpartyResponse)
                .netHigh(saleInvoice.getNetHigh())
                .netMedium(saleInvoice.getNetMedium())
                .netLow(saleInvoice.getNetLow())
                .vatHigh(saleInvoice.getVatHigh())
                .vatMedium(saleInvoice.getVatMedium())
                .vatLow(saleInvoice.getVatLow())
                .gross(saleInvoice.getGross())
                .invoiceElements(invoiceElementResponses)
                .recordId(monthlyReckoning.getSaleRecord().getSaleRecordId())
                .taxpayerId(monthlyReckoning.getTaxpayer().getTaxpayerId())
                .httpStatus(httpStatus)
                .message(message)
                .build();
    }

    public SaleInvoice mapUpdateSaleInvoiceRequestToSaleInvoice(SaleInvoiceRequest saleInvoiceRequest, SaleInvoice oldInvoice,
                                                                Counterparty counterparty, Set<SaleInvoiceElement> saleInvoiceElements) {
        SaleInvoice saleInvoice = mapNewSaleInvoiceRequestToSaleInvoice(saleInvoiceRequest, counterparty, saleInvoiceElements);
        saleInvoice.setSaleInvoiceId(oldInvoice.getSaleInvoiceId());
        return saleInvoice;
    }
}
