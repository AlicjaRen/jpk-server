package jpk.controller.invoice.mapper;

import jpk.controller.address.mapper.AddressMapper;
import jpk.controller.invoice.response.counterparty.CounterpartyResponse;
import jpk.model.address.Address;
import jpk.model.invoice.counterparty.Counterparty;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CounterpartyMapper {

    private AddressMapper addressMapper;

    public Counterparty mapInvoiceRequestElementsToCounterparty(Address address, long tradePartnerNIP, String tradePartnerName) {
        return Counterparty.builder()
                .address(address)
                .name(tradePartnerName)
                .nip(tradePartnerNIP)
                .purchaseInvoices(new HashSet<>())
                .saleInvoices(new HashSet<>())
                .taxpayers(new HashSet<>())
                .build();
    }

    public CounterpartyResponse mapCounterpartyToResponse(Counterparty counterparty) {
        return CounterpartyResponse.builder()
                .address(addressMapper.mapAddressToAddressResponse(counterparty.getAddress()))
                .counterpartyId(counterparty.getCounterpartyId())
                .name(counterparty.getName())
                .NIP(counterparty.getNip())
                .build();
    }

    public Set<Long> mapCounterpartiesToIdsResponse(Set<Counterparty> counterparties) {
        return counterparties.stream()
                .map(Counterparty::getCounterpartyId)
                .collect(Collectors.toSet());
    }
}
