package jpk.controller.invoice;

import jpk.controller.invoice.request.PurchaseInvoiceRequest;
import jpk.controller.invoice.response.PurchaseInvoiceResponse;
import jpk.service.invoice.InvoiceValidationService;
import jpk.service.invoice.PurchaseInvoiceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/invoice/purchase")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PurchaseInvoiceController {

    private PurchaseInvoiceService purchaseInvoiceService;
    private InvoiceValidationService invoiceValidationService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public PurchaseInvoiceResponse createNewPurchaseInvoice(@RequestBody PurchaseInvoiceRequest newPurchaseInvoiceRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        log.info("POST request for create new purchase invoice with number: {} from user: {}", newPurchaseInvoiceRequest.getNumber(), userName);
        invoiceValidationService.validateNewPurchaseInvoiceRequest(newPurchaseInvoiceRequest, userName);
        return purchaseInvoiceService.createNewPurchaseInvoice(newPurchaseInvoiceRequest, userName);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@invoicePermission.canUpdateOrDeletePurchaseInvoice(authentication, #invoiceId)")
    public PurchaseInvoiceResponse updatePurchaseInvoice(@PathVariable(name = "id") long invoiceId,
                                                         @RequestBody PurchaseInvoiceRequest purchaseInvoiceRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        log.info("PUT request for purchase invoice with id: {} from user: {}", invoiceId, userName);
        invoiceValidationService.validateUpdatePurchaseInvoiceRequest(purchaseInvoiceRequest, userName, invoiceId);
        return purchaseInvoiceService.updatePurchaseInvoice(purchaseInvoiceRequest, userName, invoiceId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@invoicePermission.canUpdateOrDeletePurchaseInvoice(authentication, #invoiceId)")
    public void deletePurchaseInvoice(@PathVariable(value = "id") long invoiceId) {
        log.info("DELETE request for purchase invoice with id: {}", invoiceId);
        purchaseInvoiceService.deletePurchaseInvoice(invoiceId);
    }
}
