package jpk.controller.invoice.request;

import jpk.model.invoice.enums.TaxRate;
import lombok.Value;

@Value
public class InvoiceElementRequest {

    private Long invoiceElementId;
    private String name;
    private double net;
    private double vat;
    private double gross;
    private TaxRate taxRate;
}
