package jpk.controller.invoice.request;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value
@AllArgsConstructor
public class PurchaseInvoiceRequest extends InvoiceRequest {

    private boolean deducted;

    private boolean fixedAsset;

    private String reckoningName;
}
