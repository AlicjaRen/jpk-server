package jpk.controller.invoice.request;

import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value
public class SaleInvoiceRequest extends InvoiceRequest {

}
