package jpk.controller.invoice.request.counterparty;

import jpk.controller.address.request.AddressRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CounterpartyRequest {

    private long counterpartyId;

    private AddressRequest address;

    private String name;

    private long nip;
}
