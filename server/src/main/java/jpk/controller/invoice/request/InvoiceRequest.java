package jpk.controller.invoice.request;

import jpk.controller.invoice.request.counterparty.CounterpartyRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceRequest {

    private String number;
    private Date dateOfIssue;
    private CounterpartyRequest counterparty;
    private double netHigh;
    private double netMedium;
    private double netLow;
    private double vatHigh;
    private double vatMedium;
    private double vatLow;
    private double gross;

    private Set<InvoiceElementRequest> invoiceElements;
}
