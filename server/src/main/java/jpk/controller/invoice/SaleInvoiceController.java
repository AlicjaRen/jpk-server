package jpk.controller.invoice;

import jpk.controller.invoice.request.SaleInvoiceRequest;
import jpk.controller.invoice.response.SaleInvoiceResponse;
import jpk.service.invoice.InvoiceValidationService;
import jpk.service.invoice.SaleInvoiceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/invoice/sale")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SaleInvoiceController {

    private SaleInvoiceService saleInvoiceService;
    private InvoiceValidationService invoiceValidationService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public SaleInvoiceResponse createNewSaleInvoice(@RequestBody SaleInvoiceRequest newSaleInvoiceRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        log.info("POST request for create new sale invoice with number: {} from user: {}", newSaleInvoiceRequest.getNumber(), userName);
        invoiceValidationService.validateNewInvoiceRequest(newSaleInvoiceRequest, userName);
        return saleInvoiceService.createNewSaleInvoice(newSaleInvoiceRequest, userName);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@invoicePermission.canUpdateOrDeleteSaleInvoice(authentication, #invoiceId)")
    public SaleInvoiceResponse updateSaleInvoice(@PathVariable(name = "id") long invoiceId,
                                                 @RequestBody SaleInvoiceRequest saleInvoiceRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        log.info("PUT request for sale invoice with id: {} from user: {}", invoiceId, userName);
        invoiceValidationService.validateUpdateSaleInvoiceRequest(saleInvoiceRequest, userName, invoiceId);
        return saleInvoiceService.updateSaleInvoice(saleInvoiceRequest, userName, invoiceId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@invoicePermission.canUpdateOrDeleteSaleInvoice(authentication, #invoiceId)")
    public void deleteSaleInvoice(@PathVariable(value = "id") long invoiceId) {
        log.info("DELETE request for sale invoice with id: {}", invoiceId);
        saleInvoiceService.deleteSaleInvoice(invoiceId);
    }
}
