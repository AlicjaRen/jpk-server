package jpk.controller.invoice.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import jpk.controller.common.DatePattern;
import jpk.controller.common.reponse.HttpResponse;
import jpk.controller.invoice.response.counterparty.CounterpartyResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceResponse extends HttpResponse {

    public InvoiceResponse(HttpStatus httpStatus, String message, long invoiceId, String number, Date dateOfIssue,
                           CounterpartyResponse counterparty, double netHigh, double netMedium, double netLow,
                           double vatHigh, double vatMedium, double vatLow, double gross, Set<InvoiceElementResponse> invoiceElements,
                           long recordId, long taxpayerId) {
        super(httpStatus, message);
        this.invoiceId = invoiceId;
        this.number = number;
        this.dateOfIssue = dateOfIssue;
        this.counterparty = counterparty;
        this.netHigh = netHigh;
        this.netMedium = netMedium;
        this.netLow = netLow;
        this.vatHigh = vatHigh;
        this.vatMedium = vatMedium;
        this.vatLow = vatLow;
        this.gross = gross;
        this.invoiceElements = invoiceElements;
        this.recordId = recordId;
        this.taxpayerId = taxpayerId;
    }

    private long invoiceId;
    private String number;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DatePattern.DATE_INVOICE)
    private Date dateOfIssue;

    private CounterpartyResponse counterparty;
    private double netHigh;
    private double netMedium;
    private double netLow;
    private double vatHigh;
    private double vatMedium;
    private double vatLow;
    private double gross;
    private Set<InvoiceElementResponse> invoiceElements;
    private long recordId;
    private long taxpayerId;

}
