package jpk.controller.invoice.response;

import jpk.controller.invoice.response.counterparty.CounterpartyResponse;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Value
public class PurchaseInvoiceResponse extends InvoiceResponse {

    private boolean deducted;
    private boolean fixedAsset;

    @Builder
    public PurchaseInvoiceResponse(HttpStatus httpStatus, String message, long invoiceId, String number, Date dateOfIssue,
                                   CounterpartyResponse counterparty, double netHigh, double netMedium, double netLow,
                                   double vatHigh, double vatMedium, double vatLow, double gross, Set<InvoiceElementResponse> invoiceElements,
                                   long recordId, long taxpayerId, boolean deducted, boolean fixedAsset) {
        super(httpStatus, message, invoiceId, number, dateOfIssue, counterparty, netHigh, netMedium, netLow, vatHigh,
                vatMedium, vatLow, gross, invoiceElements, recordId, taxpayerId);
        this.deducted = deducted;
        this.fixedAsset = fixedAsset;
    }
}
