package jpk.controller.invoice.response.counterparty;

import jpk.controller.address.response.AddressResponse;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CounterpartyResponse {

    private long counterpartyId;

    private AddressResponse address;

    private String name;

    private long NIP;

}
