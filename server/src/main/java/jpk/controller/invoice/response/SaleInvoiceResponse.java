package jpk.controller.invoice.response;

import jpk.controller.invoice.response.counterparty.CounterpartyResponse;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class SaleInvoiceResponse extends InvoiceResponse {

    @Builder
    public SaleInvoiceResponse(HttpStatus httpStatus, String message, long invoiceId, String number, Date dateOfIssue,
                               CounterpartyResponse counterparty, double netHigh, double netMedium, double netLow,
                               double vatHigh, double vatMedium, double vatLow, double gross, Set<InvoiceElementResponse> invoiceElements,
                               long recordId, long taxpayerId) {
        super(httpStatus, message, invoiceId, number, dateOfIssue, counterparty, netHigh, netMedium, netLow, vatHigh,
                vatMedium, vatLow, gross, invoiceElements, recordId, taxpayerId);
    }
}
