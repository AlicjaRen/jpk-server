package jpk.controller.invoice.response;

import jpk.model.invoice.enums.TaxRate;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class InvoiceElementResponse {
    private long invoiceElementId;
    private String name;
    private double net;
    private double vat;
    private double gross;
    private TaxRate taxRate;
}
