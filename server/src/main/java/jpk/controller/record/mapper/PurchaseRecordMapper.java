package jpk.controller.record.mapper;

import jpk.controller.invoice.mapper.CounterpartyMapper;
import jpk.controller.invoice.mapper.InvoiceElementMapper;
import jpk.controller.invoice.mapper.PurchaseInvoiceMapper;
import jpk.controller.invoice.response.PurchaseInvoiceResponse;
import jpk.controller.record.response.PurchaseRecordResponse;
import jpk.model.record.PurchaseRecord;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PurchaseRecordMapper {

    private PurchaseInvoiceMapper purchaseInvoiceMapper;
    private CounterpartyMapper counterpartyMapper;
    private InvoiceElementMapper invoiceElementMapper;

    public PurchaseRecordResponse mapPurchaseRecordToResponse(PurchaseRecord purchaseRecord) {
        return PurchaseRecordResponse.builder()
                .recordId(purchaseRecord.getPurchaseRecordId())
                .monthlyReckoningId(purchaseRecord.getMonthlyReckoning().getMonthlyReckoningId())
                .invoices(mapPurchaseInvoicesToResponse(purchaseRecord))
                .sumNetHigh(purchaseRecord.getSumNetHigh())
                .sumNetMedium(purchaseRecord.getSumNetMedium())
                .sumNetLow(purchaseRecord.getSumNetLow())
                .sumVatHigh(purchaseRecord.getSumVatHigh())
                .sumVatMedium(purchaseRecord.getSumVatMedium())
                .sumVatLow(purchaseRecord.getSumVatLow())
                .sumGross(purchaseRecord.getSumGross())
                .build();
    }

    private Set<PurchaseInvoiceResponse> mapPurchaseInvoicesToResponse(PurchaseRecord purchaseRecord) {
        return purchaseRecord.getPurchaseInvoices()
                .stream()
                .map(purchaseInvoice -> purchaseInvoiceMapper.mapPurchaseInvoiceToNewPurchaseInvoiceResponse(purchaseInvoice,
                        purchaseRecord.getMonthlyReckoning(), counterpartyMapper.mapCounterpartyToResponse(purchaseInvoice.getSeller()),
                        invoiceElementMapper.mapPurchaseInvoiceElementsToResponse(purchaseInvoice.getElements()),
                        HttpStatus.OK, "")
                ).collect(Collectors.toSet());
    }
}
