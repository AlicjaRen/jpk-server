package jpk.controller.record.mapper;

import jpk.controller.invoice.mapper.CounterpartyMapper;
import jpk.controller.invoice.mapper.InvoiceElementMapper;
import jpk.controller.invoice.mapper.SaleInvoiceMapper;
import jpk.controller.invoice.response.SaleInvoiceResponse;
import jpk.controller.record.response.SaleRecordResponse;
import jpk.model.record.SaleRecord;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SaleRecordMapper {

    private SaleInvoiceMapper saleInvoiceMapper;
    private CounterpartyMapper counterpartyMapper;
    private InvoiceElementMapper invoiceElementMapper;

    public SaleRecordResponse mapSaleRecordToSaleRecordResponse(SaleRecord saleRecord) {
        return SaleRecordResponse.builder()
                .recordId(saleRecord.getSaleRecordId())
                .monthlyReckoningId(saleRecord.getMonthlyReckoning().getMonthlyReckoningId())
                .invoices(mapSaleInvoicesToResponse(saleRecord))
                .sumNetHigh(saleRecord.getSumNetHigh())
                .sumNetMedium(saleRecord.getSumNetMedium())
                .sumNetLow(saleRecord.getSumNetLow())
                .sumVatHigh(saleRecord.getSumVatHigh())
                .sumVatMedium(saleRecord.getSumVatMedium())
                .sumVatLow(saleRecord.getSumVatLow())
                .sumGross(saleRecord.getSumGross())
                .build();
    }

    private Set<SaleInvoiceResponse> mapSaleInvoicesToResponse(SaleRecord saleRecord) {
        return saleRecord.getSaleInvoices()
                .stream()
                .map(saleInvoice -> saleInvoiceMapper.mapSaleInvoiceToNewSaleInvoiceResponse(saleInvoice,
                        saleRecord.getMonthlyReckoning(), counterpartyMapper.mapCounterpartyToResponse(saleInvoice.getPurchaser()),
                        invoiceElementMapper.mapSaleInvoiceElementsToResponse(saleInvoice.getElements()),
                        HttpStatus.OK, ""))
                .collect(Collectors.toSet());
    }
}
