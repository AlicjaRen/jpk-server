package jpk.controller.record;

import jpk.controller.record.response.SaleRecordResponse;
import jpk.service.record.SaleRecordService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/record/sale")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SaleRecordController {

    private SaleRecordService saleRecordService;

    @RequestMapping(value = "/{recordId}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@saleRecordPermission.canReadSaleRecord(authentication, #recordId)")
    public SaleRecordResponse getSaleRecord(@PathVariable("recordId") long recordId) {
        log.info("GET request for sale record with id: {}", recordId);
        return saleRecordService.getSaleRecord(recordId);
    }
}
