package jpk.controller.record.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RecordResponse {
    private long recordId;

    private Double sumNetHigh;
    private Double sumNetMedium;
    private Double sumNetLow;
    private Double sumVatHigh;
    private Double sumVatMedium;
    private Double sumVatLow;
    private Double sumGross;

    private long monthlyReckoningId;
}
