package jpk.controller.record.response;

import jpk.controller.invoice.response.PurchaseInvoiceResponse;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class PurchaseRecordResponse extends RecordResponse {

    private Set<PurchaseInvoiceResponse> invoices;

    @Builder
    public PurchaseRecordResponse(long recordId, Double sumNetHigh, Double sumNetMedium, Double sumNetLow, Double sumVatHigh,
                                  Double sumVatMedium, Double sumVatLow, Double sumGross, long monthlyReckoningId,
                                  Set<PurchaseInvoiceResponse> invoices) {
        super(recordId, sumNetHigh, sumNetMedium, sumNetLow, sumVatHigh, sumVatMedium, sumVatLow, sumGross,
                monthlyReckoningId);
        this.invoices = invoices;
    }
}
