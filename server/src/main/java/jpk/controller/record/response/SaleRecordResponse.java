package jpk.controller.record.response;

import jpk.controller.invoice.response.SaleInvoiceResponse;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class SaleRecordResponse extends RecordResponse {

    private Set<SaleInvoiceResponse> invoices;

    @Builder
    public SaleRecordResponse(long recordId, Double sumNetHigh, Double sumNetMedium, Double sumNetLow, Double sumVatHigh,
                              Double sumVatMedium, Double sumVatLow, Double sumGross, long monthlyReckoningId,
                              Set<SaleInvoiceResponse> invoices) {
        super(recordId, sumNetHigh, sumNetMedium, sumNetLow, sumVatHigh, sumVatMedium, sumVatLow, sumGross,
                monthlyReckoningId);
        this.invoices = invoices;
    }
}
