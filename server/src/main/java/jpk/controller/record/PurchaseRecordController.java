package jpk.controller.record;

import jpk.controller.record.response.PurchaseRecordResponse;
import jpk.service.record.PurchaseRecordService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/record/purchase")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PurchaseRecordController {

    private PurchaseRecordService purchaseRecordService;

    @RequestMapping(value = "/{recordId}", method = RequestMethod.GET)
    @PreAuthorize("@purchaseRecordPermission.canReadPurchaseRecord(authentication, #recordId)")
    public PurchaseRecordResponse getSaleRecord(@PathVariable("recordId") long recordId) {
        log.info("GET request for sale record with id: {}", recordId);
        return purchaseRecordService.getPurchaseRecord(recordId);
    }
}
