package jpk.controller.address.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressRequest {

    private long addressId;

    private String voivodeship;

    private String county;

    private String zipCode;

    private String borough;

    private String town;

    private String street;

    private String buildingNumber;

}
