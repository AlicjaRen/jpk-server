package jpk.controller.address.mapper;

import jpk.controller.address.request.AddressRequest;
import jpk.controller.address.response.AddressResponse;
import jpk.model.address.Address;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class AddressMapper {
    public Address mapAddressRequestToAddress(AddressRequest addressRequest) {
        return Address.builder()
                .voivodeship(addressRequest.getVoivodeship())
                .county(addressRequest.getCounty())
                .zipCode(addressRequest.getZipCode())
                .borough(addressRequest.getBorough())
                .town(addressRequest.getTown())
                .street(addressRequest.getStreet())
                .buildingNumber(addressRequest.getBuildingNumber())
                .counterparties(new HashSet<>())
                .taxpayers(new HashSet<>())
                .build();
    }

    public AddressResponse mapAddressToAddressResponse(Address address) {
        return AddressResponse.builder()
                .addressId(address.getAddressId())
                .voivodeship(address.getVoivodeship())
                .county(address.getCounty())
                .borough(address.getBorough())
                .zipCode(address.getZipCode())
                .town(address.getTown())
                .street(address.getStreet())
                .buildingNumber(address.getBuildingNumber())
                .build();
    }

    public Address mapUpdateAddressRequestToAddress(AddressRequest updateAddressRequest, Address oldAddress) {
        Address updateAddress = mapAddressRequestToAddress(updateAddressRequest);
        updateAddress.setAddressId(oldAddress.getAddressId());
        updateAddress.setCounterparties(oldAddress.getCounterparties());
        updateAddress.setTaxpayers(oldAddress.getTaxpayers());
        return updateAddress;
    }
}
