package jpk.controller.address.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AddressResponse {

    private long addressId;

    private String voivodeship;

    private String county;

    private String zipCode;

    private String borough;

    private String town;

    private String street;

    private String buildingNumber;

}
