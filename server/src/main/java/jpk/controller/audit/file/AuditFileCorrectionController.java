package jpk.controller.audit.file;

import jpk.controller.audit.file.response.AuditFileResponse;
import jpk.service.audit.file.AuditFileCorrectionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/audit/file/correction")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AuditFileCorrectionController {

    private AuditFileCorrectionService auditFileCorrectionService;

    @RequestMapping(value = "/{reckoningId}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@auditFilePermission.canCreateAuditFileOrAuditFileCorrection(authentication, #reckoningId)")
    public AuditFileResponse createAuditFileCorrection(@PathVariable(value = "reckoningId") long reckoningId) {
        log.info("Create request for audit file correction for reckoning with id: {}", reckoningId);
        return auditFileCorrectionService.createAuditFileCorrection(reckoningId);
    }

    @RequestMapping(value = "/{fileId}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@auditFilePermission.canReadUpdateOrDeleteAuditFileCorrection(authentication, #fileId)")
    public ResponseEntity<Resource> downloadAuditFileCorrection(@PathVariable(value = "fileId") long fileId) {
        log.info("GET request for audit file correction with id: {}", fileId);
        return auditFileCorrectionService.getAuditFileCorrection(fileId);
    }

    @RequestMapping(value = "/{fileId}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@auditFilePermission.canReadUpdateOrDeleteAuditFileCorrection(authentication, #fileId)")
    public AuditFileResponse updateAuditFileCorrection(@PathVariable(value = "fileId") long fileId) {
        log.info("PUT request for audit file correction with id: {}", fileId);
        return auditFileCorrectionService.updateAuditFileCorrection(fileId);
    }

    @RequestMapping(value = "/{fileId}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@auditFilePermission.canReadUpdateOrDeleteAuditFileCorrection(authentication, #fileId)")
    public void deleteAuditFileCorrection(@PathVariable(value = "fileId") long fileId) {
        log.info("DELETE request for audit file correction with id: {}", fileId);
        auditFileCorrectionService.deleteAuditFileCorrection(fileId);
        log.info("Audit file correction with id: {} deleted", fileId);
    }

}
