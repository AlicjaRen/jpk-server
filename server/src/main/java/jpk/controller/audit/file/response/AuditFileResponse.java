package jpk.controller.audit.file.response;

import jpk.controller.common.reponse.HttpResponse;
import lombok.Builder;
import lombok.Value;
import org.springframework.http.HttpStatus;

@Value
public class AuditFileResponse extends HttpResponse {

    private long auditFileId;
    private long monthlyReckoningId;
    private String fileName;

    @Builder
    public AuditFileResponse(HttpStatus httpStatus, String message, long auditFileId, long monthlyReckoningId, String fileName) {
        super(httpStatus, message);
        this.auditFileId = auditFileId;
        this.monthlyReckoningId = monthlyReckoningId;
        this.fileName = fileName;
    }
}
