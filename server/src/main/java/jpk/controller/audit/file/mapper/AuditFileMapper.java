package jpk.controller.audit.file.mapper;

import jpk.controller.audit.file.response.AuditFileResponse;
import jpk.model.audit.file.AuditFile;
import jpk.model.reckoining.MonthlyReckoning;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class AuditFileMapper {
    public AuditFile mapNewFileToAuditFile(byte[] data, String fileName, MonthlyReckoning monthlyReckoning) {
        return AuditFile.builder()
                .data(data)
                .fileName(fileName)
                .monthlyReckoning(monthlyReckoning)
                .build();
    }

    public AuditFileResponse mapAuditFileToResponse(AuditFile auditFile, MonthlyReckoning monthlyReckoning, String message, HttpStatus httpStatus) {
        return AuditFileResponse.builder()
                .auditFileId(auditFile.getAuditFileId())
                .monthlyReckoningId(monthlyReckoning.getMonthlyReckoningId())
                .fileName(auditFile.getFileName())
                .message(message)
                .httpStatus(httpStatus)
                .build();
    }

    public AuditFileResponse createFailAuditFileResponse(String fileName, MonthlyReckoning monthlyReckoning, String message, HttpStatus httpStatus) {
        return AuditFileResponse.builder()
                .monthlyReckoningId(monthlyReckoning.getMonthlyReckoningId())
                .fileName(fileName)
                .message(message)
                .httpStatus(httpStatus)
                .build();
    }
}
