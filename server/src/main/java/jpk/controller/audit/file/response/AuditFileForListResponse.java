package jpk.controller.audit.file.response;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuditFileForListResponse {
    private Long auditFileId;
    private String fileName;
}
