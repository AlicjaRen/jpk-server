package jpk.controller.audit.file;

import jpk.controller.audit.file.response.AuditFileResponse;
import jpk.service.audit.file.AuditFileService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/audit/file")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AuditFileController {

    private AuditFileService auditFileService;

    @RequestMapping(value = "/{reckoningId}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@auditFilePermission.canCreateAuditFileOrAuditFileCorrection(authentication, #reckoningId)")
    public AuditFileResponse createAuditFile(@PathVariable(value = "reckoningId") long reckoningId) {
        log.info("Create request for audit file for reckoning with id: {}", reckoningId);
        return auditFileService.createAuditFile(reckoningId);
    }

    @RequestMapping(value = "/{fileId}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@auditFilePermission.canReadUpdateOrDeleteAuditFile(authentication, #fileId)")
    public ResponseEntity<Resource> downloadAuditFile(@PathVariable(value = "fileId") long fileId) {
        log.info("GET request for audit file with id: {}", fileId);
        return auditFileService.getAuditFile(fileId);
    }

    @RequestMapping(value = "/{fileId}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@auditFilePermission.canReadUpdateOrDeleteAuditFile(authentication, #fileId)")
    public AuditFileResponse updateAuditFile(@PathVariable(value = "fileId") long fileId) {
        log.info("PUT request for audit file with id: {}", fileId);
        return auditFileService.updateAuditFile(fileId);
    }

    @RequestMapping(value = "/{fileId}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@auditFilePermission.canReadUpdateOrDeleteAuditFile(authentication, #fileId)")
    public void deleteAuditFile(@PathVariable(value = "fileId") long fileId) {
        log.info("DELETE request for audit file with id: {}", fileId);
        auditFileService.deleteAuditFile(fileId);
        log.info("Audit file with id: {} deleted", fileId);
    }
}
