package jpk.controller.audit.file.mapper;

import jpk.controller.audit.file.response.AuditFileResponse;
import jpk.model.audit.file.AuditFileCorrection;
import jpk.model.reckoining.MonthlyReckoning;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class AuditFileCorrectionMapper {

    public AuditFileCorrection mapNewFileToAuditFileCorrection(byte[] data, String fileName, MonthlyReckoning monthlyReckoning) {
        return AuditFileCorrection.builder()
                .data(data)
                .fileName(fileName)
                .monthlyReckoning(monthlyReckoning)
                .numberOfCorrection(monthlyReckoning.getAuditFileCorrections().size() + 1)
                .build();
    }

    public AuditFileResponse mapAuditFileCorrectionToResponse(AuditFileCorrection auditFileCorrection, MonthlyReckoning monthlyReckoning, String message, HttpStatus httpStatus) {
        return AuditFileResponse.builder()
                .auditFileId(auditFileCorrection.getAuditFileCorrectionId())
                .monthlyReckoningId(monthlyReckoning.getMonthlyReckoningId())
                .fileName(auditFileCorrection.getFileName())
                .message(message)
                .httpStatus(httpStatus)
                .build();
    }

    public AuditFileCorrection mapUpdateFileToAuditFileCorrection(byte[] data, AuditFileCorrection oldAuditFileCorrection, MonthlyReckoning monthlyReckoning) {
        return AuditFileCorrection.builder()
                .auditFileCorrectionId(oldAuditFileCorrection.getAuditFileCorrectionId())
                .data(data)
                .fileName(oldAuditFileCorrection.getFileName())
                .monthlyReckoning(monthlyReckoning)
                .numberOfCorrection(oldAuditFileCorrection.getNumberOfCorrection())
                .build();
    }
}
