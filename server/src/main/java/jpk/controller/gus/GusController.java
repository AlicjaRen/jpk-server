package jpk.controller.gus;

import jpk.exception.exceptions.DownloadingDataFromGusException;
import jpk.exception.exceptions.NIPNotFoundException;
import jpk.gus.api.model.GusDataResponse;
import jpk.gus.api.service.GusDataService;
import jpk.message.MessageFailed;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBException;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping(value = "/gus")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class GusController {

    private GusDataService gusDataService;

    @RequestMapping(value = "/nip/{nip}", method = RequestMethod.GET)
    public GusDataResponse getInformationByNIP(@PathVariable String nip) {
        log.info("Request to GUS for NIP: {}", nip);
        Optional<GusDataResponse> gusResponse;
        try {
            gusResponse = gusDataService.downloadReport(nip);
        } catch (JAXBException e) {
            throw new NIPNotFoundException();
        }
        gusResponse.ifPresent(response -> response.setNIP(nip));
        return gusResponse.orElseThrow(() -> new DownloadingDataFromGusException(MessageFailed.CONNECTING_TO_GUS_FAILED.text()));
    }
}
