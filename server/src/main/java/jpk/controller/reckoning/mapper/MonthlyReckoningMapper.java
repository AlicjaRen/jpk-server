package jpk.controller.reckoning.mapper;

import jpk.controller.audit.file.response.AuditFileForListResponse;
import jpk.controller.reckoning.response.ReckoningResponse;
import jpk.model.audit.file.AuditFileCorrection;
import jpk.model.reckoining.MonthlyReckoning;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MonthlyReckoningMapper {

    public ReckoningResponse mapMonthlyReckoningToReckoningNameResponse(MonthlyReckoning reckoning) {
        return ReckoningResponse.builder()
                .id(reckoning.getMonthlyReckoningId())
                .name(reckoning.getName())
                .saleRecordId(reckoning.getSaleRecord().getSaleRecordId())
                .purchaseRecordId(reckoning.getPurchaseRecord().getPurchaseRecordId())
                .auditFileId(reckoning.getAuditFile() != null ? reckoning.getAuditFile().getAuditFileId() : null)
                .auditFileName(reckoning.getAuditFile() != null ? reckoning.getAuditFile().getFileName() : null)
                .auditFileCorrections(mapAuditFileCorrectionsToResponseForList(reckoning.getAuditFileCorrections()))
                .build();
    }

    private Set<AuditFileForListResponse> mapAuditFileCorrectionsToResponseForList(Set<AuditFileCorrection> auditFileCorrections) {
        return auditFileCorrections.stream()
                .map(this::mapAuditFileCorrectionToResponseForList)
                .collect(Collectors.toSet());
    }

    private AuditFileForListResponse mapAuditFileCorrectionToResponseForList(AuditFileCorrection auditFileCorrection) {
        return AuditFileForListResponse.builder()
                .auditFileId(auditFileCorrection.getAuditFileCorrectionId())
                .fileName(auditFileCorrection.getFileName())
                .build();
    }

    public Set<Long> mapMonthlyReckoningsToIdsResponse(Set<MonthlyReckoning> reckonings) {
        return reckonings.stream()
                .map(MonthlyReckoning::getMonthlyReckoningId)
                .collect(Collectors.toSet());
    }
}
