package jpk.controller.reckoning;

import jpk.controller.reckoning.response.MonthlyReckoningsResponse;
import jpk.service.reckoning.MonthlyReckoningService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/reckoning")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ReckoningController {

    private MonthlyReckoningService monthlyReckoningService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public MonthlyReckoningsResponse getReckoningsNames() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        log.info("GET request for monthly reckonings for user: {}", userName);
        return monthlyReckoningService.getMonthlyReckoningNamesForTaxpayer(userName);
    }

}
