package jpk.controller.reckoning.response;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MonthlyReckoningsResponse {

    private List<ReckoningResponse> reckonings;
    private ReckoningResponse currentSuggestedReckoning;
}
