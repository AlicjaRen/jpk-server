package jpk.controller.reckoning.response;

import jpk.controller.audit.file.response.AuditFileForListResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class ReckoningResponse {

    private Long id;
    private String name;
    private Long saleRecordId;
    private Long purchaseRecordId;
    private Long auditFileId;
    private String auditFileName;
    private Set<AuditFileForListResponse> auditFileCorrections;
}
