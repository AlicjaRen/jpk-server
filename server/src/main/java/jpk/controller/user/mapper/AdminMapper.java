package jpk.controller.user.mapper;

import jpk.controller.user.request.admin.AdminUpdateRequest;
import jpk.controller.user.request.register.NewAdminRequest;
import jpk.controller.user.response.admin.AdminResponse;
import jpk.controller.user.response.register.NewAdminResponse;
import jpk.model.user.Admin;
import jpk.model.user.User;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminMapper {
    public Admin mapNewAdminRequestToAdmin(NewAdminRequest newAdminRequest, User user) {
        return Admin.builder()
                .adminId(user.getUserId())
                .email(newAdminRequest.getEmail())
                .user(user)
                .build();
    }

    public NewAdminResponse mapAdminToNewAdminResponse(Admin admin, String plainTextPassword, HttpStatus httpStatus, String message) {
        return NewAdminResponse.builder()
                .userId(admin.getAdminId())
                .email(admin.getEmail())
                .login(admin.getUser().getLogin())
                .password(plainTextPassword)
                .userRole(admin.getUser().getUserRole())
                .httpStatus(httpStatus)
                .message(message)
                .build();
    }

    public AdminResponse mapAdminToResponse(Admin admin, HttpStatus httpStatus, String message) {
        return AdminResponse.builder()
                .userId(admin.getAdminId())
                .email(admin.getEmail())
                .login(admin.getUser().getLogin())
                .httpStatus(httpStatus)
                .message(message)
                .build();
    }

    public List<AdminResponse> mapAdminsListToResponse(List<Admin> adminsList) {
        return adminsList.stream()
                .map(admin -> mapAdminToResponse(admin, HttpStatus.OK, ""))
                .collect(Collectors.toList());
    }

    public Admin mapAdminUpdateRequestToAdmin(AdminUpdateRequest adminUpdateRequest, Admin oldAdmin) {
        oldAdmin.setEmail(adminUpdateRequest.getEmail());
        oldAdmin.getUser().setLogin(adminUpdateRequest.getLogin());
        return oldAdmin;
    }
}
