package jpk.controller.user.mapper;

import jpk.controller.user.response.account.ResetPasswordResponse;
import jpk.model.user.User;
import jpk.model.user.enums.UserRole;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class UserMapper {
    public User mapNewAdminRequestToUser(String login, String password) {
        return User.builder()
                .login(login)
                .password(password)
                .userRole(UserRole.ADMIN)
                .build();
    }

    public User mapNewTaxpayerRequestToUser(String login, String encodedPassword) {
        return User.builder()
                .login(login)
                .password(encodedPassword)
                .userRole(UserRole.TAXPAYER)
                .build();
    }

    public User mapTaxpayerUpdateRequestToUser(String updatedLogin, User oldUser) {
        oldUser.setLogin(updatedLogin);
        return oldUser;
    }

    public ResetPasswordResponse mapResetPasswordToResponse(User user, String plainTextPassword) {
        return ResetPasswordResponse.builder()
                .login(user.getLogin())
                .userId(user.getUserId())
                .newPassword(plainTextPassword)
                .httpStatus(HttpStatus.OK)
                .message("")
                .build();
    }
}
