package jpk.controller.user.mapper;

import jpk.controller.address.response.AddressResponse;
import jpk.controller.user.request.register.NewTaxpayerRequestByAdmin;
import jpk.controller.user.request.register.NewTaxpayerRequestByTaxpayer;
import jpk.controller.user.request.taxpayer.TaxpayerUpdateRequest;
import jpk.controller.user.response.register.NewTaxpayerResponseByAdmin;
import jpk.controller.user.response.register.NewTaxpayerResponseByTaxpayer;
import jpk.controller.user.response.taxpayer.TaxpayerResponse;
import jpk.model.address.Address;
import jpk.model.user.Taxpayer;
import jpk.model.user.User;
import jpk.model.user.enums.UserRole;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Set;

@Service
public class TaxpayerMapper {
    public Taxpayer mapNewTaxpayerRequestToTaxpayer(NewTaxpayerRequestByAdmin newTaxpayerRequestByAdmin, User user, Address address) {
        return Taxpayer.builder()
                .taxpayerId(user.getUserId())
                .email(newTaxpayerRequestByAdmin.getEmail())
                .name(newTaxpayerRequestByAdmin.getName())
                .NIP(newTaxpayerRequestByAdmin.getNIP())
                .user(user)
                .address(address)
                .monthlyReckonings(Collections.emptySet())
                .build();
    }

    public NewTaxpayerResponseByAdmin mapTaxpayerByAdminToTaxpayerResponse(Taxpayer taxpayer, AddressResponse addressResponse,
                                                                           String plainTextPassword, HttpStatus httpStatus,
                                                                           String message) {
        return NewTaxpayerResponseByAdmin.builder()
                .login(taxpayer.getUser().getLogin())
                .plainTextPassword(plainTextPassword)
                .email(taxpayer.getEmail())
                .name(taxpayer.getName())
                .NIP(taxpayer.getNIP())
                .userId(taxpayer.getTaxpayerId())
                .userRole(taxpayer.getUser().getUserRole())
                .addressResponse(addressResponse)
                .httpStatus(httpStatus)
                .message(message)
                .build();
    }

    public Taxpayer mapNewTaxpayerRequestByTaxpayerToTaxpayer(NewTaxpayerRequestByTaxpayer newTaxpayerRequestByTaxpayer, User user, Address address) {
        return Taxpayer.builder()
                .taxpayerId(user.getUserId())
                .email(newTaxpayerRequestByTaxpayer.getEmail())
                .name(newTaxpayerRequestByTaxpayer.getName())
                .NIP(newTaxpayerRequestByTaxpayer.getNIP())
                .user(user)
                .address(address)
                .monthlyReckonings(Collections.emptySet())
                .build();
    }

    public NewTaxpayerResponseByTaxpayer mapTaxpayerByTaxpayerToTaxpayerResponse(Taxpayer taxpayer, AddressResponse addressResponse,
                                                                                 HttpStatus httpStatus, String message) {
        return NewTaxpayerResponseByTaxpayer.builder()
                .email(taxpayer.getEmail())
                .login(taxpayer.getUser().getLogin())
                .password("SECRET")
                .name(taxpayer.getName())
                .NIP(taxpayer.getNIP())
                .userId(taxpayer.getTaxpayerId())
                .userRole(UserRole.TAXPAYER)
                .addressResponse(addressResponse)
                .httpStatus(httpStatus)
                .message(message)
                .build();
    }

    public TaxpayerResponse mapTaxpayerToTaxpayerResponse(Taxpayer taxpayer, AddressResponse addressResponse, Set<Long> reckoningsIds,
                                                          Set<Long> counterpartiesIds, HttpStatus httpStatus, String message) {
        return TaxpayerResponse.builder()
                .userId(taxpayer.getTaxpayerId())
                .login(taxpayer.getUser().getLogin())
                .userRole(UserRole.TAXPAYER)
                .name(taxpayer.getName())
                .nip(taxpayer.getNIP())
                .email(taxpayer.getEmail())
                .address(addressResponse)
                .monthlyReckoningsIds(reckoningsIds)
                .counterpartiesIds(counterpartiesIds)
                .httpStatus(httpStatus)
                .message(message)
                .build();
    }

    public Taxpayer mapUpdateTaxpayerRequestToTaxpayer(TaxpayerUpdateRequest taxpayerUpdateRequest, User updatedUser,
                                                       Address updatedAddress, Taxpayer oldTaxpayer) {
        return Taxpayer.builder()
                .taxpayerId(oldTaxpayer.getTaxpayerId())
                .email(taxpayerUpdateRequest.getEmail())
                .name(taxpayerUpdateRequest.getName())
                .NIP(taxpayerUpdateRequest.getNIP())
                .user(updatedUser)
                .address(updatedAddress)
                .counterparties(oldTaxpayer.getCounterparties())
                .monthlyReckonings(oldTaxpayer.getMonthlyReckonings())
                .build();
    }

}
