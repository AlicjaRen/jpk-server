package jpk.controller.user.response.register;

import jpk.controller.common.reponse.HttpResponse;
import jpk.model.user.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewUserResponse extends HttpResponse {

    private Long userId;
    private String login;
    private String password;
    private UserRole userRole;

    public NewUserResponse(HttpStatus httpStatus, String message, Long userId, String login, String password, UserRole userRole) {
        super(httpStatus, message);
        this.userId = userId;
        this.login = login;
        this.password = password;
        this.userRole = userRole;
    }
}
