package jpk.controller.user.response.account;

import jpk.model.user.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class LoginResponse {

    private String login;
    private String token;
    private UserRole userRole;
    private Long userId;
}
