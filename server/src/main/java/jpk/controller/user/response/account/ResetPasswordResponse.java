package jpk.controller.user.response.account;

import jpk.controller.common.reponse.HttpResponse;
import lombok.Builder;
import lombok.Value;
import org.springframework.http.HttpStatus;

@Value
public class ResetPasswordResponse extends HttpResponse {

    private long userId;
    private String login;
    private String newPassword;

    @Builder
    public ResetPasswordResponse(HttpStatus httpStatus, String message, long userId, String login, String newPassword) {
        super(httpStatus, message);
        this.userId = userId;
        this.login = login;
        this.newPassword = newPassword;
    }
}
