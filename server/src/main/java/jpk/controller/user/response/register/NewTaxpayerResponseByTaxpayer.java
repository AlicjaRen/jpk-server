package jpk.controller.user.response.register;

import jpk.controller.address.response.AddressResponse;
import jpk.model.user.enums.UserRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class NewTaxpayerResponseByTaxpayer extends NewUserResponse {

    private String name;

    private Long NIP;

    private String email;

    private AddressResponse address;

    @Builder
    public NewTaxpayerResponseByTaxpayer(Long userId, String login, String password, UserRole userRole,
                                         HttpStatus httpStatus, String message, String name, Long NIP, String email,
                                         AddressResponse addressResponse) {
        super(httpStatus, message, userId, login, password, userRole);
        this.name = name;
        this.NIP = NIP;
        this.email = email;
        this.address = addressResponse;
    }
}
