package jpk.controller.user.response.register;

import jpk.model.user.enums.UserRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class NewAdminResponse extends NewUserResponse {

    private String email;

    @Builder
    public NewAdminResponse(Long userId, String login, String password, UserRole userRole, HttpStatus httpStatus,
                            String message, String email) {
        super(httpStatus, message, userId, login, password, userRole);
        this.email = email;
    }
}