package jpk.controller.user.response.taxpayer;

import jpk.controller.address.response.AddressResponse;
import jpk.controller.common.reponse.HttpResponse;
import jpk.model.user.enums.UserRole;
import lombok.Builder;
import lombok.Value;
import org.springframework.http.HttpStatus;

import java.util.Set;

@Value
public class TaxpayerResponse extends HttpResponse {

    private Long userId;
    private String login;
    private UserRole userRole;
    private String name;
    private Long nip;
    private String email;
    private AddressResponse address;
    private Set<Long> monthlyReckoningsIds;
    private Set<Long> counterpartiesIds;

    @Builder
    public TaxpayerResponse(HttpStatus httpStatus, String message, Long userId, String login, UserRole userRole,
                            String name, Long nip, String email, AddressResponse address, Set<Long> monthlyReckoningsIds,
                            Set<Long> counterpartiesIds) {
        super(httpStatus, message);
        this.userId = userId;
        this.login = login;
        this.userRole = userRole;
        this.name = name;
        this.nip = nip;
        this.email = email;
        this.address = address;
        this.monthlyReckoningsIds = monthlyReckoningsIds;
        this.counterpartiesIds = counterpartiesIds;
    }
}
