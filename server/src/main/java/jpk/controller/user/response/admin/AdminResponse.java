package jpk.controller.user.response.admin;

import jpk.controller.common.reponse.HttpResponse;
import lombok.Builder;
import lombok.Value;
import org.springframework.http.HttpStatus;

@Value
public class AdminResponse extends HttpResponse {
    private long userId;
    private String email;
    private String login;

    @Builder
    public AdminResponse(HttpStatus httpStatus, String message, long userId, String email, String login) {
        super(httpStatus, message);
        this.userId = userId;
        this.email = email;
        this.login = login;
    }
}
