package jpk.controller.user.response.register;

import jpk.controller.address.response.AddressResponse;
import jpk.model.user.enums.UserRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class NewTaxpayerResponseByAdmin extends NewUserResponse {

    private String name;

    private Long NIP;

    private String email;

    private AddressResponse address;

    @Builder
    public NewTaxpayerResponseByAdmin(Long userId, String login, UserRole userRole, HttpStatus httpStatus,
                                      String message, String name, Long NIP, String email, String plainTextPassword,
                                      AddressResponse addressResponse) {
        super(httpStatus, message, userId, login, plainTextPassword, userRole);
        this.name = name;
        this.NIP = NIP;
        this.email = email;
        this.address = addressResponse;
    }
}
