package jpk.controller.user;

import jpk.controller.user.request.taxpayer.TaxpayerUpdateRequest;
import jpk.controller.user.response.taxpayer.TaxpayerResponse;
import jpk.service.user.TaxpayerService;
import jpk.service.user.UserValidationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/user/taxpayer")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TaxpayerController {

    private TaxpayerService taxpayerService;
    private UserValidationService userValidationService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@taxpayerPermission.canReadOrUpdateTaxpayer(authentication, #userId)")
    public TaxpayerResponse getTaxpayer(@PathVariable(value = "id") long userId) {
        log.info("GET request for taxpayer with id: {}", userId);
        return taxpayerService.getTaxpayer(userId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@taxpayerPermission.canReadOrUpdateTaxpayer(authentication, #userId)")
    public TaxpayerResponse updateTaxpayer(@PathVariable(name = "id") long userId,
                                           @RequestBody TaxpayerUpdateRequest taxpayerUpdateRequest) {
        log.info("PUT request for taxpayer with id: {}", userId);
        userValidationService.validateTaxpayerUpdateRequest(taxpayerUpdateRequest, userId);
        TaxpayerResponse taxpayerResponse = taxpayerService.updateTaxpayer(taxpayerUpdateRequest, userId);
        log.info("Taxpayer with id: {} updated", userId);
        return taxpayerResponse;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@taxpayerPermission.canDeleteOrReadAllTaxpayers(authentication)")
    public List<TaxpayerResponse> getAllTaxpayers() {
        log.info("GET request for all taxpayers");
        return taxpayerService.getAllTaxpayers();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@taxpayerPermission.canDeleteOrReadAllTaxpayers(authentication)")
    public void deleteTaxpayer(@PathVariable(name = "id") long userId) {
        log.info("DELETE request for taxpayer with id: {}", userId);
        taxpayerService.deleteTaxpayer(userId);
        log.info("Taxpayer with id {} deleted", userId);
    }
}
