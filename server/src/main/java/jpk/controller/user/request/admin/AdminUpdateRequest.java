package jpk.controller.user.request.admin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class AdminUpdateRequest {

    private String email;
    private String login;
}
