package jpk.controller.user.request.register;

import jpk.model.user.enums.UserRole;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewUserRequest {

    private String login;
    private String password;
    private UserRole userRole;
}
