package jpk.controller.user.request.register;

import jpk.controller.address.request.AddressRequest;
import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value
public class NewTaxpayerRequestByTaxpayer extends NewUserRequest {

    private String passwordConfirmation;
    private Long NIP;
    private String email;
    private String name;
    private AddressRequest address;
}
