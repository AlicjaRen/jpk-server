package jpk.controller.user.request.register;

import jpk.controller.address.request.AddressRequest;
import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value
public class NewTaxpayerRequestByAdmin extends NewUserRequest {

    private String name;
    private Long NIP;
    private String email;
    private AddressRequest address;
}
