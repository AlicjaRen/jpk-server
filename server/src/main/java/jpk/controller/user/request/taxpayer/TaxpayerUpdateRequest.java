package jpk.controller.user.request.taxpayer;

import jpk.controller.address.request.AddressRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaxpayerUpdateRequest {

    private String login;
    private Long NIP;
    private String email;
    private String name;
    private AddressRequest address;
}
