package jpk.controller.user.request.register;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class NewAdminRequest {

    private String email;
}
