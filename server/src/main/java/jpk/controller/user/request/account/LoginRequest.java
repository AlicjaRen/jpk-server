package jpk.controller.user.request.account;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class LoginRequest {

    private String login;
    private String password;
}
