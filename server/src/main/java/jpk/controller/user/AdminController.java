package jpk.controller.user;

import jpk.controller.user.request.admin.AdminUpdateRequest;
import jpk.controller.user.response.admin.AdminResponse;
import jpk.service.user.AdminService;
import jpk.service.user.UserValidationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/user/admin")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AdminController {

    private AdminService adminService;
    private UserValidationService userValidationService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@adminPermission.canUseAdmin(authentication)")
    public AdminResponse getAdmin(@PathVariable(value = "id") long userId) {
        log.info("GET request for admin with id: {}", userId);
        return adminService.getAdmin(userId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@adminPermission.canUseAdmin(authentication)")
    public AdminResponse updateAdmin(@PathVariable(value = "id") long userId,
                                     @RequestBody AdminUpdateRequest adminUpdateRequest) {
        log.info("PUT request for admin with id: {}", userId);
        userValidationService.validateUpdateAdminRequest(adminUpdateRequest, userId);
        return adminService.updateAdmin(userId, adminUpdateRequest);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@adminPermission.canUseAdmin(authentication)")
    public void deleteAdmin(@PathVariable(value = "id") long userId) {
        log.info("DELETE request for admin with id: {}", userId);
        adminService.deleteAdmin(userId);
        log.info("Admin with id: {} deleted", userId);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@adminPermission.canUseAdmin(authentication)")
    public List<AdminResponse> getAllAdmins() {
        log.info("GET request for admin list");
        return adminService.getAllAdmins();
    }
}
