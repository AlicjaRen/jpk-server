package jpk.controller.user;

import jpk.controller.user.request.register.NewAdminRequest;
import jpk.controller.user.request.register.NewTaxpayerRequestByAdmin;
import jpk.controller.user.request.register.NewTaxpayerRequestByTaxpayer;
import jpk.controller.user.response.register.NewAdminResponse;
import jpk.controller.user.response.register.NewTaxpayerResponseByAdmin;
import jpk.controller.user.response.register.NewTaxpayerResponseByTaxpayer;
import jpk.service.user.UserRegisterService;
import jpk.service.user.UserValidationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/user")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserRegisterController {

    private UserRegisterService userRegisterService;
    private UserValidationService userValidationService;

    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    public NewAdminResponse registerAdmin(@RequestBody NewAdminRequest newAdminRequest) {
        log.info("POST request for register new admin with email: {}", newAdminRequest.getEmail());
        userValidationService.validateNewAdmin(newAdminRequest);
        return userRegisterService.registerNewAdmin(newAdminRequest);
    }

    @RequestMapping(value = "/taxpayer/admin", method = RequestMethod.POST)
    public NewTaxpayerResponseByAdmin registerNewTaxpayerByAdmin(@RequestBody NewTaxpayerRequestByAdmin newTaxpayerRequestByAdmin) {
        log.info("POST request for register new taxpayer with email: {} by admin", newTaxpayerRequestByAdmin.getEmail());
        userValidationService.validateNewTaxpayerCreatedByAdmin(newTaxpayerRequestByAdmin);
        return userRegisterService.registerNewTaxpayerByAdmin(newTaxpayerRequestByAdmin);
    }

    @RequestMapping(value = "/taxpayer/user", method = RequestMethod.POST)
    public NewTaxpayerResponseByTaxpayer registerNewTaxpayerByTaxpayer(@RequestBody NewTaxpayerRequestByTaxpayer newTaxpayerRequestByTaxpayer) {
        log.info("POST request for register new taxpayer with email: {} by admin", newTaxpayerRequestByTaxpayer.getEmail());
        userValidationService.validateNewTaxpayerCreatedByTaxpayer(newTaxpayerRequestByTaxpayer);
        return userRegisterService.registerNewTaxpayerByTaxpayer(newTaxpayerRequestByTaxpayer);
    }
}
