package jpk.controller.user;

import jpk.controller.user.request.account.ChangePasswordRequest;
import jpk.controller.user.response.account.ResetPasswordResponse;
import jpk.service.user.UserService;
import jpk.service.user.UserValidationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/user/password")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserAccountController {

    private UserService userService;
    private UserValidationService userValidationService;

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePassword(@RequestBody ChangePasswordRequest changePasswordRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        log.info("Change password request for user with username: {}", userName);
        userValidationService.validateChangePasswordRequest(changePasswordRequest, userName);
        userService.changePassword(changePasswordRequest, userName);
        log.info("Password for user with username: {} changed", userName);
    }

    @RequestMapping(value = "/reset/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@userPermission.canResetPassword(authentication)")
    public ResetPasswordResponse resetPassword(@PathVariable(name = "id") long userId) {
        log.info("Reset password request for user with id: {}", userId);
        return userService.resetPassword(userId);
    }
}
