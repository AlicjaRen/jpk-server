package jpk.controller.user;

import jpk.config.security.TokenService;
import jpk.controller.user.request.account.LoginRequest;
import jpk.controller.user.response.account.LoginResponse;
import jpk.exception.exceptions.AuthorizationException;
import jpk.exception.exceptions.UserNotFoundException;
import jpk.model.user.User;
import jpk.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
public class UserLoginController {
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final TokenService tokenService;

    @Autowired
    public UserLoginController(UserService userService, PasswordEncoder passwordEncoder, TokenService tokenService) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.tokenService = tokenService;
    }

    @RequestMapping(value = "/user/login", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public LoginResponse login(@RequestBody LoginRequest loginRequest) {
        User user;
        try {
            user = userService.getUserByLogin(loginRequest.getLogin());
        } catch (UserNotFoundException e) {
            throw new AuthorizationException();
        }
        if (!passwordEncoder.matches(loginRequest.getPassword(), user.getPassword()))
            throw new AuthorizationException();
        log.info("Login for user with login: {}", loginRequest.getLogin());
        return LoginResponse.builder()
                .login(user.getLogin())
                .token(tokenService.generateToken(user))
                .userId(user.getUserId())
                .userRole(user.getUserRole())
                .build();
    }
}