package jpk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpkServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(JpkServerApplication.class, args);
    }
}
