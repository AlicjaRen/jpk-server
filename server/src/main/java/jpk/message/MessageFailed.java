package jpk.message;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum MessageFailed {

    USER_NOT_FOUND("Such user doesn't exist"),

    NOT_VALID_EMAIL("Email address doesn't match email pattern"),
    NOT_UNIQUE_EMAIL("Email must be unique for role"),

    NOT_VALID_NIP("Invalid nip"),

    NOT_UNIQUE_LOGIN("Login must be unique"),

    NOT_CONFIRM_PASSWORDS("Password and password confirmation must be equals"),
    NOT_STRONG_PASSWORD("Too leak password - must contain at least 8 signs: 1 digit, 1 small letter, 1 upper letter and 1 special sign (from: @#$%^&+=!*(){}[]_|<>?) "),

    INVALID_FORMAT_OF_ZIP_CODE("Invalid format of zip code. Required: NN-NNN"),

    INFORMATION_IN_GUS_NOT_FOUND("Information about such taxpayer is not found in GUS"),

    CONNECTING_TO_GUS_FAILED("Error by connecting to GUS"),

    INVALID_CREDENTIALS("Invalid credentials"),
    NOT_VALID_CURRENT_PASSWORD("Current password is invalid"),


    NIP_NOT_FOUND("nip not found in GUS"),

    NIP_THE_SAME_AS_REQUESTER("Counterparty nip can't be the same as nip of requester"),
    DATE_FROM_FUTURE("Date of issue can't be from the future"),
    ZERO_SUM_ON_INVOICE("Sum on invoice can't be equal to zero"),
    NOT_VALID_SUM_ON_INVOICE("Sum on invoice and sum from elements must be equal"),
    NOT_VALID_NAME_FOR_RECKONING("This purchase invoice can't be accounted in that settling period"),

    RECORD_NOT_FOUND("Such record doesn't exist"),
    SALE_INVOICE_NOT_FOUND("Such sale invoice doesn't exist"),
    PURCHASE_INVOICE_NOT_FOUND("Such purchase invoice doesn't exist"),
    ADDRESS_NOT_FOUND("Such address doesn't exist"),

    AUDIT_FILE_NOT_FOUND("Such audit file doesn't exist"),
    ERROR_BY_AUDIT_FILE_CREATING("Error by creating audit file for tax"),
    AUDIT_FILE_EXISTS("Audit file for this monthly reckoning already exists");

    private String text;

    public String text() {
        return text;
    }
}
