package jpk.message;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum MessageSuccess {

    NEW_ADMIN_SAVED("New admin saved successfully"),
    TAXPAYER_CREATED("New taxpayer created"),

    SALE_INVOICE_CREATED("New sale invoice created"),
    PURCHASE_INVOICE_CREATED("New purchase invoice created"),

    AUDIT_FILE_CREATED("Audit file created"),

    SALE_INVOICE_UPDATED("Sale invoice updated"),
    PURCHASE_INVOICE_UPDATED("Purchase invoice updated"),

    TAXPAYER_UPDATED("Taxpayer updated"),
    ADMIN_UPDATED("Admin updated successfully"),

    AUDIT_FILE_UPDATED("Audit file updated"),
    CORRECTION_AUDIT_FILE_CREATED("Audit file correction created"),
    CORRECTION_AUDIT_FILE_UPDATED("Audit file correction updated");

    private String text;

    public String text() {
        return text;
    }
}
