package jpk.exception.exceptions;

import jpk.message.MessageFailed;

public class AuthorizationException extends RuntimeException {
    public AuthorizationException() {
        super(MessageFailed.INVALID_CREDENTIALS.text());
    }
}
