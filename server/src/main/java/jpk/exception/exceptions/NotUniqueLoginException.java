package jpk.exception.exceptions;

import jpk.message.MessageFailed;

public class NotUniqueLoginException extends RuntimeException {
    public NotUniqueLoginException() {
        super(MessageFailed.NOT_UNIQUE_LOGIN.text());
    }
}
