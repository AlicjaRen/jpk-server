package jpk.exception.exceptions;

import jpk.message.MessageFailed;

public class PurchaseInvoiceNotFound extends RuntimeException {
    public PurchaseInvoiceNotFound() {
        super(MessageFailed.PURCHASE_INVOICE_NOT_FOUND.text());
    }

}
