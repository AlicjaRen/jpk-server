package jpk.exception.exceptions;

import jpk.message.MessageFailed;

public class AddressNotFoundException extends RuntimeException {
    public AddressNotFoundException() {
        super(MessageFailed.ADDRESS_NOT_FOUND.text());
    }
}
