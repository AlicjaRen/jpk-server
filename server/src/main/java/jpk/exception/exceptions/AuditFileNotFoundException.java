package jpk.exception.exceptions;

import jpk.message.MessageFailed;

public class AuditFileNotFoundException extends RuntimeException {
    public AuditFileNotFoundException() {
        super(MessageFailed.AUDIT_FILE_NOT_FOUND.text());
    }
}
