package jpk.exception.exceptions;

import jpk.message.MessageFailed;

public class NIPNotFoundException extends RuntimeException {
    public NIPNotFoundException() {
        super(MessageFailed.NIP_NOT_FOUND.text());
    }
}
