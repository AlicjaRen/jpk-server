package jpk.exception.exceptions;

import jpk.message.MessageFailed;

public class RecordNotFoundException extends RuntimeException {
    public RecordNotFoundException() {
        super(MessageFailed.RECORD_NOT_FOUND.text());
    }
}
