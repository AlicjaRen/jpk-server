package jpk.exception.exceptions;

import jpk.message.MessageFailed;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException() {
        super(MessageFailed.USER_NOT_FOUND.text());
    }
}
