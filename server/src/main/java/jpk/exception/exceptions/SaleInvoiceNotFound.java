package jpk.exception.exceptions;

import jpk.message.MessageFailed;

public class SaleInvoiceNotFound extends RuntimeException {
    public SaleInvoiceNotFound() {
        super(MessageFailed.SALE_INVOICE_NOT_FOUND.text());
    }
}
