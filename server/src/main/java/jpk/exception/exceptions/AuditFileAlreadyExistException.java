package jpk.exception.exceptions;

import jpk.message.MessageFailed;

public class AuditFileAlreadyExistException extends RuntimeException {
    public AuditFileAlreadyExistException() {
        super(MessageFailed.AUDIT_FILE_EXISTS.text());
    }
}
