package jpk.exception.exceptions;

public class DownloadingDataFromGusException extends RuntimeException {
    public DownloadingDataFromGusException(String message) {
        super(message);
    }
}