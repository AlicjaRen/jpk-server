package jpk.exception;

import jpk.exception.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionMessage handleUserNotFoundException(UserNotFoundException ex) {
        return new ExceptionMessage(ex);
    }

    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionMessage handleValidationException(ValidationException ex) {
        return new ExceptionMessage(ex);
    }

    @ExceptionHandler(NotUniqueLoginException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionMessage handleNotUniqueLoginException(NotUniqueLoginException ex) {
        return new ExceptionMessage(ex);
    }

    @ExceptionHandler(SaleInvoiceNotFound.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionMessage handleSaleInvoiceNotFound(SaleInvoiceNotFound ex) {
        return new ExceptionMessage(ex);
    }

    @ExceptionHandler(PurchaseInvoiceNotFound.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionMessage handlePurchaseInvoiceNotFound(PurchaseInvoiceNotFound ex) {
        return new ExceptionMessage(ex);
    }

    @ExceptionHandler(AuthorizationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ExceptionMessage handleAuthorizationException(AuthorizationException ex) {
        return new ExceptionMessage(ex);
    }

    @ExceptionHandler(NIPNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionMessage handleNIPNotFoundException(NIPNotFoundException ex) {
        return new ExceptionMessage(ex);
    }

    @ExceptionHandler(AuditFileAlreadyExistException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionMessage handleAuditFileAlreadyExistException(AuditFileAlreadyExistException ex) {
        return new ExceptionMessage(ex);
    }

    @ExceptionHandler(AuditFileNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionMessage handleAuditFileNotFoundException(AuditFileNotFoundException ex) {
        return new ExceptionMessage(ex);
    }
}
