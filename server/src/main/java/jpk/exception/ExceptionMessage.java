package jpk.exception;

import lombok.Value;
import lombok.experimental.Tolerate;

import java.util.Date;

@Value
public class ExceptionMessage {
    String message;
    String exception;
    long timestamp;

    @Tolerate
    public ExceptionMessage(Exception ex) {
        this.message = ex.getMessage();
        this.exception = ex.getClass().toString();
        this.timestamp = new Date().getTime();
    }

}