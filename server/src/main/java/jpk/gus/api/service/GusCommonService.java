package jpk.gus.api.service;

import com.google.common.collect.ImmutableMap;
import jpk.gus.api.model.GusRootResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.io.StringReader;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class GusCommonService {

    private static final String GUS_SESSION_HTTP_HEADER = "sid";

    public <T> void addSessionHeader(String sessionId, T port) {

        final Map<String, List<String>> header = ImmutableMap.<String, List<String>>builder().put(GUS_SESSION_HTTP_HEADER, Collections.singletonList(sessionId)).build();
        BindingProvider bindingProvider = (BindingProvider) port;

        bindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, header);
    }

    public Optional<GusRootResponse> parseDataResponseToGusResponse(String dataResponse) throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(GusRootResponse.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(dataResponse);
        GusRootResponse gusRootResponse = (GusRootResponse) unmarshaller.unmarshal(reader);
        return Optional.of(gusRootResponse);
    }
}
