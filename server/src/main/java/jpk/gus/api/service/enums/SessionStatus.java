package jpk.gus.api.service.enums;

import lombok.Getter;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Getter
public enum SessionStatus {

    EXISTS("1"),
    NOT_EXISTS("0"),
    ERROR("");

    public final static String PARAMETER_NAME = "StatusSesji";
    private String returnCode;

    SessionStatus(String code) {
        this.returnCode = code;
    }

    public static SessionStatus ofCode(String code) {
        Optional<SessionStatus> result =
                Stream.of(SessionStatus.values())
                        .filter(s -> Objects.equals(s.getReturnCode(), code)).findFirst();
        return result.orElse(ERROR);
    }
}