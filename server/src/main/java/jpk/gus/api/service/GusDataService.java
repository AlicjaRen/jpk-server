package jpk.gus.api.service;

import jpk.exception.exceptions.DownloadingDataFromGusException;
import jpk.gus.api.client.IUslugaBIRzewnPubl;
import jpk.gus.api.client.ObjectFactory;
import jpk.gus.api.client.ParametryWyszukiwania;
import jpk.gus.api.client.UslugaBIRzewnPubl;
import jpk.gus.api.model.GusDataResponse;
import jpk.gus.api.model.GusRootResponse;
import jpk.gus.api.properties.GusProperties;
import jpk.gus.api.service.enums.SessionStatus;
import jpk.message.MessageFailed;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.AddressingFeature;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

@Component
@Slf4j
public class GusDataService {

    private IUslugaBIRzewnPubl iUslugaBIRzewnPubl;
    private GusCommonService gusCommonService;
    private GusProperties gusProperties;

    private Instant sessionBeginTime;
    private String sessionId;

    private final static ObjectFactory objectsCreator = new ObjectFactory();

    @Autowired
    public GusDataService(GusCommonService gusCommonService, GusProperties gusProperties) {
        this.gusProperties = gusProperties;
        this.iUslugaBIRzewnPubl = createIUslugaBIRzewnPubl();
        this.gusCommonService = gusCommonService;
        this.sessionId = "";
    }

    public Optional<GusDataResponse> downloadReport(String nip) throws JAXBException {
        ParametryWyszukiwania searchingParameters = createSearchingParametersWithNip(nip);
        return downloadReport(searchingParameters);
    }

    private ParametryWyszukiwania createSearchingParametersWithNip(String nip) {
        ParametryWyszukiwania searchingParameters = objectsCreator.createParametryWyszukiwania();
        searchingParameters.setNip(objectsCreator.createParametryWyszukiwaniaNip(nip));
        return searchingParameters;
    }

    private Optional<GusDataResponse> downloadReport(ParametryWyszukiwania searchingParameters) throws JAXBException {

        if (!checkSessionValid()) {
            logIn();
        }

        Optional<GusDataResponse> result = Optional.empty();

        if (checkSessionValid() && checkSessionTime()) {
            gusCommonService.addSessionHeader(sessionId, iUslugaBIRzewnPubl);
            GusRootResponse gusRootResponse = downloadData(searchingParameters);
            result = Optional.of(gusRootResponse.getGusDataResponse());
        }
        logOut();
        return result;
    }

    private boolean checkSessionValid() {
        String returnCode = iUslugaBIRzewnPubl.getValue(SessionStatus.PARAMETER_NAME);
        switch (SessionStatus.ofCode(returnCode)) {
            case EXISTS:
                return true;
            default:
                return false;
        }
    }

    // false session timeout
    // true session time is valid
    private boolean checkSessionTime() {
        return sessionBeginTime != null
                && !gusProperties.getGusSessionDurationInMinutes().minus(Duration.between(Instant.now(), sessionBeginTime)).isNegative();
    }

    private void logIn() {
        if (!checkSessionValid()) {
            sessionId = iUslugaBIRzewnPubl.zaloguj(gusProperties.getGusKey());
            if (StringUtils.isNotBlank(sessionId)) {
                sessionBeginTime = Instant.now();
                gusCommonService.addSessionHeader(sessionId, iUslugaBIRzewnPubl);
                log.info("Logged on GUS");
            } else {
                log.info("Logging on GUS attempt failed");
            }
        }
    }

    public void logOut() {
        if (!sessionId.isEmpty()) {
            iUslugaBIRzewnPubl.wyloguj(sessionId);
            sessionBeginTime = null;
        }
        log.info("Logged out from GUS");
    }

    private GusRootResponse downloadData(ParametryWyszukiwania searchingParameters) throws JAXBException {

        if (!checkSessionValid()) {
            logIn();
        }
        gusCommonService.addSessionHeader(sessionId, iUslugaBIRzewnPubl);
        String dataResponse = iUslugaBIRzewnPubl.daneSzukaj(searchingParameters);
        log.debug("Response before unmarshalling: {}", dataResponse);
        Optional<GusRootResponse> gusResponse = gusCommonService.parseDataResponseToGusResponse(dataResponse);

        return gusResponse.orElseThrow(() -> new DownloadingDataFromGusException(MessageFailed.INFORMATION_IN_GUS_NOT_FOUND.text()));
    }

    @Bean
    public IUslugaBIRzewnPubl createIUslugaBIRzewnPubl() {
        UslugaBIRzewnPubl service = new UslugaBIRzewnPubl();

        IUslugaBIRzewnPubl port = service.getE3(new AddressingFeature(true, true));
        BindingProvider bindingProvider = (BindingProvider) port;
        bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, gusProperties.getGusAddress());

        return port;
    }
}