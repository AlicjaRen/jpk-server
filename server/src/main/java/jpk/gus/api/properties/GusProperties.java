package jpk.gus.api.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.time.Duration;

@Getter
@Configuration
@PropertySource("classpath:gus.connection.properties")
public class GusProperties {

    @Value("${gus.address}")
    private String gusAddress;

    @Value("${gus.key}")
    private String gusKey;

    @Value("${gus.session.duration}")
    private int gusSessionDuration;

    public Duration getGusSessionDurationInMinutes() {
        return Duration.ofMinutes(this.gusSessionDuration);
    }
}
