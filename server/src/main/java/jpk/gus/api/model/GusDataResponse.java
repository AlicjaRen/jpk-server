package jpk.gus.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "dane")
public class GusDataResponse {

    private String NIP;

    @XmlElement(name = "Regon")
    private String regonNumber;

    @XmlElement(name = "RegonLink")
    private String regonLink;

    @XmlElement(name = "Nazwa")
    private String name;

    @XmlElement(name = "Wojewodztwo")
    private String province;

    @XmlElement(name = "Powiat")
    private String county;

    @XmlElement(name = "Gmina")
    private String borough;

    @XmlElement(name = "Miejscowosc")
    private String town;

    @XmlElement(name = "Ulica")
    private String street;

    @XmlElement(name = "KodPocztowy")
    private String zipCode;
}
