package jpk.model.record;

import jpk.model.invoice.PurchaseInvoice;
import jpk.model.reckoining.MonthlyReckoning;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "purchase_records")
public class PurchaseRecord extends Record {

    @Builder
    public PurchaseRecord(Double sumNetHigh, Double sumNetMedium, Double sumNetLow, Double sumVatHigh, Double sumVatMedium,
                          Double sumVatLow, Double sumGross, Set<PurchaseInvoice> purchaseInvoices, MonthlyReckoning monthlyReckoning) {
        super(sumNetHigh, sumNetMedium, sumNetLow, sumVatHigh, sumVatMedium, sumVatLow, sumGross);
        this.purchaseInvoices = purchaseInvoices;
        this.monthlyReckoning = monthlyReckoning;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "purchase_record_id")
    private Long purchaseRecordId;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "purchaseRecord")
    private Set<PurchaseInvoice> purchaseInvoices;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "purchase_record_id",
            referencedColumnName = "monthly_reckoning_id")
    private MonthlyReckoning monthlyReckoning;
}
