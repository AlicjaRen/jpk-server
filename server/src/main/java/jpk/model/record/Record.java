package jpk.model.record;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public class Record {

    @Column(name = "sum_net_high")
    private Double sumNetHigh;

    @Column(name = "sum_net_medium")
    private Double sumNetMedium;

    @Column(name = "sum_net_low")
    private Double sumNetLow;

    @Column(name = "sum_vat_high")
    private Double sumVatHigh;

    @Column(name = "sum_vat_medium")
    private Double sumVatMedium;

    @Column(name = "sum_vat_low")
    private Double sumVatLow;

    @Column(name = "sum_gross")
    private Double sumGross;
}
