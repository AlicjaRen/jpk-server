package jpk.model.record;

import jpk.model.invoice.SaleInvoice;
import jpk.model.reckoining.MonthlyReckoning;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "sale_records")
public class SaleRecord extends Record {

    @Builder
    public SaleRecord(Double sumNetHigh, Double sumNetMedium, Double sumNetLow, Double sumVatHigh, Double sumVatMedium,
                      Double sumVatLow, Double sumGross, Set<SaleInvoice> saleInvoices, MonthlyReckoning monthlyReckoning) {
        super(sumNetHigh, sumNetMedium, sumNetLow, sumVatHigh, sumVatMedium, sumVatLow, sumGross);
        this.saleInvoices = saleInvoices;
        this.monthlyReckoning = monthlyReckoning;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "sale_record_id")
    private Long saleRecordId;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "saleRecord")
    private Set<SaleInvoice> saleInvoices;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "sale_record_id",
            referencedColumnName = "monthly_reckoning_id")
    private MonthlyReckoning monthlyReckoning;
}
