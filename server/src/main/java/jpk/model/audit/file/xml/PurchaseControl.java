package jpk.model.audit.file.xml;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ZakupCtrl")
public class PurchaseControl {

    @XmlElement(name = "LiczbaWierszyZakupow")
    private Integer numberOfPurchaseRows;

    @XmlElement(name = "PodatekNaliczony")
    private Double inputTax;
}
