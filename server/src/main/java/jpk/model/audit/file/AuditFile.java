package jpk.model.audit.file;

import jpk.model.reckoining.MonthlyReckoning;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "audit_files")
public class AuditFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "audit_file_id")
    private Long auditFileId;

    @Column(name = "file_name")
    private String fileName;

    @Lob
    @Column(name = "xml_file")
    private byte[] data;

    @Column(name = "create_date", updatable = false)
    @CreationTimestamp
    private Date createDate;

    @Column(name = "update_date")
    @UpdateTimestamp
    private Date updateDate;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "monthly_reckoning_id", nullable = false)
    private MonthlyReckoning monthlyReckoning;
}
