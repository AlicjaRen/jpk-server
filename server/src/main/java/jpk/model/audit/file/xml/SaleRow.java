package jpk.model.audit.file.xml;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SprzedazWiersz")
public class SaleRow {

    @XmlElement(name = "LpSprzedazy")
    private Integer saleIndex;

    @XmlElement(name = "NrKontrahenta")
    private String counterpartyNIP;

    @XmlElement(name = "NazwaKontrahenta")
    private String counterpartyName;

    @XmlElement(name = "AdresKontrahenta")
    private String counterpartyAddress;

    @XmlElement(name = "DowodSprzedazy")
    private String invoiceNumber;

    @XmlElement(name = "DataWystawienia")
    private String dateOfIssue;

    @XmlElement(name = "DataSprzedazy")
    private String dateOfSale;

    @XmlElement(name = "K_15")
    private Double sumNetLow;

    @XmlElement(name = "K_16")
    private Double sumVatLow;

    @XmlElement(name = "K_17")
    private Double sumNetMedium;

    @XmlElement(name = "K_18")
    private Double sumVatMedium;

    @XmlElement(name = "K_19")
    private Double sumNetHigh;

    @XmlElement(name = "K_20")
    private Double sumVatHigh;
}
