package jpk.model.audit.file.xml;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "JPK")
public class StandardAuditFileForTax {

    @XmlElement(name = "Naglowek")
    private Header header;

    @XmlElement(name = "Podmiot1")
    private Person person;

    @XmlElement(name = "SprzedazWiersz")
    private List<SaleRow> saleRows;

    @XmlElement(name = "SprzedazCtrl")
    private SaleControl saleControl;

    @XmlElement(name = "ZakupWiersz")
    private List<PurchaseRow> purchaseRows;

    @XmlElement(name = "ZakupCtrl")
    private PurchaseControl purchaseControl;
}
