package jpk.model.audit.file.xml;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SprzedazCtrl")
public class SaleControl {

    @XmlElement(name = "LiczbaWierszySprzedazy")
    private Integer numberOfSaleRows;

    @XmlElement(name = "PodatekNalezny")
    private Double taxDue;
}
