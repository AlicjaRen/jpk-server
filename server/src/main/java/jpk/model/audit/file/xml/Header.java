package jpk.model.audit.file.xml;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Naglowek")
public class Header {

    @XmlElement(name = "KodFormularza")
    private FormCode formCode;

    @XmlElement(name = "WariantFormularza")
    private Integer formVariant;

    @XmlElement(name = "CelZlozenia")
    private Integer aimOfSubmitting;

    @XmlElement(name = "DataWytworzeniaJPK")
    private String dateOfCreation;

    @XmlElement(name = "DataOd")
    private String startDate;

    @XmlElement(name = "DataDo")
    private String finishDate;

    @XmlElement(name = "NazwaSystemu")
    private String systemName;
}
