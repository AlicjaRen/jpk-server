package jpk.model.audit.file.xml;


import com.sun.xml.internal.txw2.annotation.XmlElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlElement
public class FormCode {

    @XmlAttribute(name = "kodSystemowy")
    private String systemCode;

    @XmlAttribute(name = "wersjaSchemy")
    private String schemaVersion;

    @XmlValue
    private String value;
}
