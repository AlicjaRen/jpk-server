package jpk.model.audit.file;

import jpk.model.reckoining.MonthlyReckoning;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "audit_file_corrections")
public class AuditFileCorrection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "audit_file_correction_id")
    private Long auditFileCorrectionId;

    @Column(name = "file_name")
    private String fileName;

    @Lob
    @Column(name = "xml_file")
    private byte[] data;

    @Column(name = "correction_number")
    private Integer numberOfCorrection;

    @Column(name = "create_date", updatable = false)
    @CreationTimestamp
    private Date createDate;

    @Column(name = "update_date")
    @UpdateTimestamp
    private Date updateDate;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "monthly_reckoning_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MonthlyReckoning monthlyReckoning;
}
