package jpk.model.audit.file.xml;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ZakupWiersz")
public class PurchaseRow {

    @XmlElement(name = "LpZakupu")
    private Integer purchaseIndex;

    @XmlElement(name = "NrDostawcy")
    private String supplierNIP;

    @XmlElement(name = "NazwaDostawcy")
    private String supplierName;

    @XmlElement(name = "AdresDostawcy")
    private String supplierAddress;

    @XmlElement(name = "DowodZakupu")
    private String invoiceNumber;

    @XmlElement(name = "DataZakupu")
    private String dateOfPurchase;

    @XmlElement(name = "DataWplywu")
    private String dateOfReceive;

    @XmlElement(name = "K_43")
    private Double sumNetFixedAsset;

    @XmlElement(name = "K_44")
    private Double sumInputTaxFromFixedAsset;

    @XmlElement(name = "K_45")
    private Double sumNetOther;

    @XmlElement(name = "K_46")
    private Double sumInputTaxFromOther;

}
