package jpk.model.audit.file.xml;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Podmiot1")
public class Person {

    @XmlElement(name = "NIP")
    private String NIP;

    @XmlElement(name = "PelnaNazwa")
    private String name;

    @XmlElement(name = "Email")
    private String email;
}
