@XmlSchema(
        namespace = "http://jpk.mf.gov.pl/wzor/2017/11/13/1113/",
        elementFormDefault = XmlNsForm.QUALIFIED,
        xmlns = {
                @XmlNs(prefix = "etd", namespaceURI = "http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"),
                @XmlNs(prefix = "tns", namespaceURI = "http://jpk.mf.gov.pl/wzor/2017/11/13/1113/"),
                @XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance")
        }
)
package jpk.model.audit.file.xml;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;