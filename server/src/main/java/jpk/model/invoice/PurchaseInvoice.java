package jpk.model.invoice;

import jpk.model.invoice.counterparty.Counterparty;
import jpk.model.invoice.element.PurchaseInvoiceElement;
import jpk.model.record.PurchaseRecord;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "purchase_invoices")
public class PurchaseInvoice extends Invoice {

    @Builder
    public PurchaseInvoice(String number, Date dateOfIssue, Date createDate, Date updateDate, Double netHigh,
                           Double netMedium, Double netLow, Double vatHigh, Double vatMedium, Double vatLow,
                           Double gross, Boolean deducted, Boolean fixedAsset, Counterparty seller, PurchaseRecord purchaseRecord,
                           Set<PurchaseInvoiceElement> elements) {
        super(number, dateOfIssue, createDate, updateDate, netHigh, netMedium,
                netLow, vatHigh, vatMedium, vatLow, gross);
        this.deducted = deducted;
        this.fixedAsset = fixedAsset;
        this.seller = seller;
        this.purchaseRecord = purchaseRecord;
        this.elements = elements;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "purchase_invoice_id")
    private Long purchaseInvoiceId;

    @NotNull
    @Column(name = "deducted")
    private Boolean deducted;

    @NotNull
    @Column(name = "fixed_asset")
    private Boolean fixedAsset;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "counterparty_id")
    private Counterparty seller;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "purchase_record_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PurchaseRecord purchaseRecord;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "purchaseInvoice")
    private Set<PurchaseInvoiceElement> elements;

}
