package jpk.model.invoice.counterparty;

import jpk.model.address.Address;
import jpk.model.invoice.PurchaseInvoice;
import jpk.model.invoice.SaleInvoice;
import jpk.model.user.Taxpayer;
import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "counterparties")
public class Counterparty {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "counterparty_id")
    private Long counterpartyId;

    @NotNull
    @NotEmpty
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "nip", unique = true)
    private Long nip;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "purchaser")
    private Set<SaleInvoice> saleInvoices;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "seller")
    private Set<PurchaseInvoice> purchaseInvoices;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "counterparties")
    private Set<Taxpayer> taxpayers;
}
