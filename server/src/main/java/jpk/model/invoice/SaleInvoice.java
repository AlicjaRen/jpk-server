package jpk.model.invoice;

import jpk.model.invoice.counterparty.Counterparty;
import jpk.model.invoice.element.SaleInvoiceElement;
import jpk.model.record.SaleRecord;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "sale_invoices")
public class SaleInvoice extends Invoice {

    @Builder
    public SaleInvoice(String number, Date dateOfIssue, Date createDate, Date updateDate, Double netHigh, Double netMedium,
                       Double netLow, Double vatHigh, Double vatMedium, Double vatLow, Double gross, Counterparty purchaser,
                       SaleRecord saleRecord, Set<SaleInvoiceElement> elements) {
        super(number, dateOfIssue, createDate, updateDate, netHigh, netMedium,
                netLow, vatHigh, vatMedium, vatLow, gross);
        this.purchaser = purchaser;
        this.saleRecord = saleRecord;
        this.elements = elements;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sale_invoice_id")
    private Long saleInvoiceId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "counterparty_id")
    private Counterparty purchaser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sale_record_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private SaleRecord saleRecord;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "saleInvoice")
    private Set<SaleInvoiceElement> elements;

}
