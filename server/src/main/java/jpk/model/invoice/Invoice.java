package jpk.model.invoice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class Invoice {

    @NotNull
    @Column(name = "invoice_number")
    private String number;

    @NotNull
    @Column(name = "date_of_issue")
    private Date dateOfIssue;

    @Column(name = "create_date", updatable = false)
    @CreationTimestamp
    private Date createDate;

    @Column(name = "update_date")
    @UpdateTimestamp
    private Date updateDate;

    @Column(name = "net_high")
    private Double netHigh;

    @Column(name = "net_medium")
    private Double netMedium;

    @Column(name = "net_low")
    private Double netLow;

    @Column(name = "vat_high")
    private Double vatHigh;

    @Column(name = "vat_medium")
    private Double vatMedium;

    @Column(name = "vat_low")
    private Double vatLow;

    @NotNull
    @Column(name = "gross")
    private Double gross;
}
