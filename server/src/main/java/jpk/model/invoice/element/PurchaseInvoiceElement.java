package jpk.model.invoice.element;

import jpk.model.invoice.PurchaseInvoice;
import jpk.model.invoice.enums.TaxRate;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "purchase_invoice_elements")
public class PurchaseInvoiceElement extends InvoiceElement {

    @Builder
    public PurchaseInvoiceElement(String name, Double net, Double vat, Double gross, TaxRate taxRate, PurchaseInvoice purchaseInvoice) {
        super(name, net, vat, gross, taxRate);
        this.purchaseInvoice = purchaseInvoice;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "purchase_invoice_element_id")
    private Long invoiceElementId;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "purchase_invoice_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PurchaseInvoice purchaseInvoice;
}
