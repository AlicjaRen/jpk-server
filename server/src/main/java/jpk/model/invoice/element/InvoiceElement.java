package jpk.model.invoice.element;

import jpk.model.invoice.enums.TaxRate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class InvoiceElement {

    @NotNull
    @Column(name = "name", length = 90)
    private String name;

    @NotNull
    @Column(name = "net")
    private Double net;

    @NotNull
    @Column(name = "vat")
    private Double vat;

    @NotNull
    @Column(name = "gross")
    private Double gross;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tax_rate")
    private TaxRate taxRate;

}
