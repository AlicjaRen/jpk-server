package jpk.model.invoice.element;

import jpk.model.invoice.SaleInvoice;
import jpk.model.invoice.enums.TaxRate;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "sale_invoice_elements")
public class SaleInvoiceElement extends InvoiceElement {

    @Builder
    public SaleInvoiceElement(String name, Double net, Double vat, Double gross, TaxRate taxRate, SaleInvoice saleInvoice) {
        super(name, net, vat, gross, taxRate);
        this.saleInvoice = saleInvoice;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sale_invoice_element_id")
    private Long invoiceElementId;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sale_invoice_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private SaleInvoice saleInvoice;
}
