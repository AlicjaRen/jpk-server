package jpk.model.invoice.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum TaxRate {

    LOW(5),
    MEDIUM(8),
    HIGH(23);

    private int value;

    public int value() {
        return this.value;
    }
}
