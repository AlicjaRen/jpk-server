package jpk.model.reckoining;

import jpk.model.audit.file.AuditFile;
import jpk.model.audit.file.AuditFileCorrection;
import jpk.model.record.PurchaseRecord;
import jpk.model.record.SaleRecord;
import jpk.model.user.Taxpayer;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "monthly_reckonings",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"name", "taxpayer_id"})
        })
public class MonthlyReckoning {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "monthly_reckoning_id")
    private Long monthlyReckoningId;

    @NotNull
    @Column(name = "name")
    private String name;

    @OneToOne(fetch = FetchType.LAZY,
            mappedBy = "monthlyReckoning",
            cascade = CascadeType.ALL)
    private PurchaseRecord purchaseRecord;

    @OneToOne(fetch = FetchType.LAZY,
            mappedBy = "monthlyReckoning",
            cascade = CascadeType.ALL)
    private SaleRecord saleRecord;

    @OneToOne(fetch = FetchType.LAZY,
            mappedBy = "monthlyReckoning",
            cascade = CascadeType.ALL)
    private AuditFile auditFile;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "monthlyReckoning",
            cascade = CascadeType.ALL)
    private Set<AuditFileCorrection> auditFileCorrections;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "taxpayer_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Taxpayer taxpayer;
}
