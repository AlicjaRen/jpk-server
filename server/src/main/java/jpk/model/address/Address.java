package jpk.model.address;

import jpk.model.invoice.counterparty.Counterparty;
import jpk.model.user.Taxpayer;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "addresses")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "address_id")
    private Long addressId;

    @NotNull
    @Column(name = "voivodeship")
    private String voivodeship;

    @NotNull
    @Column(name = "county")
    private String county;

    @NotNull
    @Column(name = "zipCode", length = 6)
    private String zipCode;

    @NotNull
    @Column(name = "borough")
    private String borough;

    @NotNull
    @Column(name = "town")
    private String town;

    @Column(name = "street")
    private String street;

    @NotNull
    @Column(name = "buildingNumber")
    private String buildingNumber;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "address")
    private Set<Taxpayer> taxpayers;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "address")
    private Set<Counterparty> counterparties;


    @Override
    public String toString() {
        if (street == null) {
            return String.format("%s %s, %s %s", borough, zipCode, town, buildingNumber);
        }
        return String.format("%s %s, ul. %s %s", borough, zipCode, street, buildingNumber);
    }
}
