package jpk.model.user.enums;

public enum UserRole {
    TAXPAYER, ADMIN
}
