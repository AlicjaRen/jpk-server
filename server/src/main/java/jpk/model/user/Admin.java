package jpk.model.user;

import lombok.*;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "admins")
public class Admin {
    @Id
    @Column(name = "admin_id")
    private Long adminId;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "admin_id", referencedColumnName = "user_id")
    private User user;

    @NotEmpty
    @NotNull
    @Email
    @Column(name = "email")
    private String email;
}
