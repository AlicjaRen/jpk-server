package jpk.model.user;

import jpk.model.address.Address;
import jpk.model.invoice.counterparty.Counterparty;
import jpk.model.reckoining.MonthlyReckoning;
import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "taxpayers")
public class Taxpayer {
    @Id
    @Column(name = "taxpayer_id")
    private Long taxpayerId;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "taxpayer_id", referencedColumnName = "user_id")
    private User user;

    @NotNull
    @NotEmpty
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "nip")
    private Long NIP;

    @NotEmpty
    @NotNull
    @Column(name = "email")
    private String email;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "taxpayer")
    private Set<MonthlyReckoning> monthlyReckonings;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "taxpayers_counterparties",
            catalog = "jpkdb",
            joinColumns = {@JoinColumn(name = "taxpayer_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "counterparty_id", nullable = false)})
    private Set<Counterparty> counterparties;
}
