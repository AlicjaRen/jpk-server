package jpk.repository.reckoning;

import jpk.model.reckoining.MonthlyReckoning;
import jpk.model.user.Taxpayer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MonthlyReckoningRepository extends JpaRepository<MonthlyReckoning, Long> {

    Optional<MonthlyReckoning> findByMonthlyReckoningId(long monthlyReckoningId);

    Optional<MonthlyReckoning> findByNameAndTaxpayer(String name, Taxpayer taxpayer);
}
