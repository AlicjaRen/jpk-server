package jpk.repository.record;

import jpk.model.record.SaleRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SaleRecordRepository extends JpaRepository<SaleRecord, Long> {

    Optional<SaleRecord> findBySaleRecordId(long saleRecordId);
}
