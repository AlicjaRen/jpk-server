package jpk.repository.record;

import jpk.model.record.PurchaseRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PurchaseRecordRepository extends JpaRepository<PurchaseRecord, Long> {

    Optional<PurchaseRecord> findByPurchaseRecordId(long purchaseRecordId);
}
