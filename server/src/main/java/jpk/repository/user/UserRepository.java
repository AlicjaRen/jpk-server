package jpk.repository.user;

import jpk.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUserId(long userId);

    Optional<User> findByLogin(String login);
}