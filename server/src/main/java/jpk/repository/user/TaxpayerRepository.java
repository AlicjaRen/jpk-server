package jpk.repository.user;

import jpk.model.user.Taxpayer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TaxpayerRepository extends JpaRepository<Taxpayer, Long> {

    Optional<Taxpayer> findByName(String username);
    Optional<Taxpayer> findByTaxpayerId(long taxpayerId);

}
