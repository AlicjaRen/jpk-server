package jpk.repository.audit.file;

import jpk.model.audit.file.AuditFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuditFileRepository extends JpaRepository<AuditFile, Long> {

    Optional<AuditFile> findByAuditFileId(long auditFileId);
}
