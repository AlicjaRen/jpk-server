package jpk.repository.audit.file;

import jpk.model.audit.file.AuditFileCorrection;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuditFileCorrectionRepository extends JpaRepository<AuditFileCorrection, Long> {

    Optional<AuditFileCorrection> findByAuditFileCorrectionId(long auditFileCorrectionId);
}
