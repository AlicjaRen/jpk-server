package jpk.repository.invoice.counterparty;

import jpk.model.invoice.counterparty.Counterparty;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CounterpartyRepository extends JpaRepository<Counterparty, Long> {

    Optional<Counterparty> findByNip(long nip);
}
