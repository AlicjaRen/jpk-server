package jpk.repository.invoice.element;

import jpk.model.invoice.element.SaleInvoiceElement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SaleInvoiceElementRepository extends JpaRepository<SaleInvoiceElement, Long> {

    Optional<SaleInvoiceElement> findByInvoiceElementId(long invoiceElementId);
}
