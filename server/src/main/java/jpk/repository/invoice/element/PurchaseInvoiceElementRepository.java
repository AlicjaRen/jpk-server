package jpk.repository.invoice.element;

import jpk.model.invoice.element.PurchaseInvoiceElement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PurchaseInvoiceElementRepository extends JpaRepository<PurchaseInvoiceElement, Long> {

    Optional<PurchaseInvoiceElement> findByInvoiceElementId(long invoiceElementId);
}
