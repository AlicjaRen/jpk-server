package jpk.repository.invoice;

import jpk.model.invoice.PurchaseInvoice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PurchaseInvoiceRepository extends JpaRepository<PurchaseInvoice, Long> {

    Optional<PurchaseInvoice> findByPurchaseInvoiceId(long purchaseInvoiceId);
}
