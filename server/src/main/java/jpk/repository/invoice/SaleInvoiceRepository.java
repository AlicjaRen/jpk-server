package jpk.repository.invoice;

import jpk.model.invoice.SaleInvoice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SaleInvoiceRepository extends JpaRepository<SaleInvoice, Long> {

    Optional<SaleInvoice> findBySaleInvoiceId(long saleInvoiceId);
}
